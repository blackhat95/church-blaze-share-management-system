<?php if (!defined('BASEPATH')) {
	exit('No direct script access allowed');
}

class SystemUsers extends CI_Controller {
	private $username;

	public function __construct() {
		parent::__construct();
		$this->load->model('user_model');
		$this->load->model('shares/shares_model', 'shares');
		$this->load->model('settings/settings_model', 'settings');

	}

	public function logout() {
		$data['success'] = ("");
		$data['error'] = ("");
		$this->load->library('session');
		$this->session->userdata('username');
		$this->session->sess_destroy();
		$this->load->helper(array ( 'form', 'url' ));
		$this->load->library('form_validation');
		$data['title'] = "Login";
		log_message('info', 'User:- ' . $this->session->userdata('name') . ' ' . $this->session->userdata('role')
				. ' logged out successfully');
		$data['success'] = "Logged Out Successfully";
		$this->load->view('login', $data);

	}

	public function accessDenied() {
		$data['success'] = ("");
		$data['error'] = ("");
		$this->load->library('session');
		$this->session->userdata('username');
		$this->session->sess_destroy();
		$this->load->helper(array ( 'form', 'url' ));
		$this->load->library('form_validation');
		$data['title'] = "Login";
		$data['error'] = "Access Denied! Please Login To Continue!";
		log_message('info', 'User:- ' . $this->session->userdata('name') . ' ' . $this->session->userdata('role')
				. ' Access Denied! Please login to Continue');
		$this->load->view('login', $data);

	}

	public function index() {
		$this->load->library('session');
		$this->session->userdata('username');
		$this->session->userdata('role');
		$this->session->userdata('Id');
		if ($this->session->userdata('logged_in') == "TRUE") {
			if ($this->session->userdata('role') == "Admin") {
				header('Location:' . base_url('admin'));
			} else {
				$this->dashboard();
			}
		} else {
			$data['success'] = ("");
			$data['error'] = ("");
			$this->load->helper(array ( 'form', 'url' ));
			$this->load->library('form_validation');
			$this->form_validation->set_rules('username', 'Username ', 'required');
			$this->form_validation->set_rules('password', 'Password  ', 'required');
			if ($this->form_validation->run() == false) {
				$data['title'] = "Login";
				$this->load->view('login', $data);
			} else {
				$status = $this->user_model->userstatus();
				$role = $this->user_model->getRole();
				$passcode = md5($this->input->post("password"));
				$name = null;
				$id = null;
				if ($status == 1) {
					if (($role == "Admin") OR ($role == "Admin2") OR ($role == "Finance")) {
						$this->load->library('session');
						$name = $this->user_model->getStaffName();
						$id = $this->user_model->getStaffId();
						$idnumber = $this->user_model->getStaffIdnumber();
						$phone = $this->user_model->getStaffphone();
						$result = $this->settings->getSystemInfo();
						$sysinfo = $result['row_count'];
						$result1 = $this->shares->getShareTypesCount();
						$sharetypes = $result1['row_count'];
						$newdata = array ( 'username' => $this->input->post("username"), 'logged_in' => true,
								'password' => $passcode, 'role' => $role, 'name' => $name, 'id' => $id,
								'idnumber' => $idnumber, 'phone' => $phone, 'sysinfo' => $sysinfo,
								'sharetypes' => $sharetypes );

						$this->session->set_userdata($newdata);
						$user = $this->session->all_userdata();
						log_message('info',
								'User:- ' . $this->session->userdata('name') . ' ' . $this->session->userdata('role')
								. ' logged in successfully');
						$this->dashboard();
					} elseif ($role == "Agent") {
						$this->load->library('session');
						$name = $this->user_model->getAgentName();
						$id = $this->user_model->getAgentId();
						$idnumber = $this->user_model->getAgentIdnumber();
						$phone = $this->user_model->getAgentphone();
						$newdata = array ( 'username' => $this->input->post("username"), 'logged_in' => true,
								'password' => $passcode, 'role' => $role, 'name' => $name, 'id' => $id,
								'idnumber' => $idnumber, 'phone' => $phone );

						$this->session->set_userdata($newdata);
						$user = $this->session->all_userdata();
						log_message('info',
								'User:- ' . $this->session->userdata('name') . ' ' . $this->session->userdata('role')
								. ' logged in successfully');
						$this->dashboard();
					} else {
						$this->load->library('session');
						$name = $this->user_model->getAgentName();
						$id = $this->user_model->getAgentId();
						$newdata = array ( 'username' => $this->input->post("username"), 'logged_in' => true,
								'password' => $passcode, 'role' => $role, 'name' => $name, 'id' => $id );

						$this->session->set_userdata($newdata);
						$user = $this->session->all_userdata();
						log_message('info',
								'User:- ' . $this->session->userdata('name') . ' ' . $this->session->userdata('role')
								. ' logged in successfully');
						$this->dashboard();
					}

				} else if ($status == 0) {
					$data['error'] = ("Account Suspended. Contact System Admin!");
					log_message('info', 'User:- ' . $this->input->post("username") . ' access to the system was declined');
				} else {
					$data['error'] = ("Invalid Login Credentials");
					log_message('info', 'User:- ' . $this->input->post("username") . ' access to the system was declined');
				}
				$data['title'] = "Login";
				$this->load->view('login', $data);
			}

		}

	}

	public function dashboard() {
		$this->load->library('session');
		$this->load->helper('url');
		$data['profile'] = $this->user_model->get_user($this->session->userdata('username'));
		if ($data['profile']['0']['role'] == "Admin") {
			header('Location:' . base_url('redirect'));
		} elseif ($data['profile']['0']['role'] == "Admin2") {
			header('location:' . base_url('redirect'));
		} elseif ($data['profile']['0']['role'] == "Agent") {
			header('location:' . base_url('redirect'));
		} else {
			header('Location:' . base_url('redirect'));
		}
	}

	public function userProfile() {
		if ($this->session->userdata('logged_in')) {
			$role = $this->session->userdata('name');
			$index['success'] = ("");
			$index['error'] = ("");
			$index['info'] = ("");
			$index['mainContent'] = 'Admin/userProfile';
			$index['title'] = "User Profile";
			$this->load->view('Admin/adminRedirect', $index);
		} else {
			redirect('systemUsers/accessDenied');
		}
	}

	public function changePassword() {
		if (($this->session->userdata('role') == "Admin") OR ($this->session->userdata('role') == "Admin2")
				OR ($this->session->userdata('role') == "Agent") OR ($this->session->userdata('role') == "Finance")
		) {
			if ($this->session->userdata('logged_in')) {
				$data['success'] = ("");
				$data['error'] = ("");
				$data['info'] = ("");
				$this->load->library('form_validation');
				$this->form_validation->set_rules('currentpass', 'Enter your current password', 'required|trim');
				$this->form_validation->set_rules('newpass', 'Enter your new password', 'required|trim');
				$this->form_validation->set_rules('confirmpass', 'Confirm the password', 'required|trim');
				$role = $this->session->userdata('role');
				$Id = $this->session->userdata('id');
				$currentpass = md5($this->input->post('currentpass'));
				$newpass = md5($this->input->post('newpass'));
				$confirmpass = md5($this->input->post('confirmpass'));
				if ($this->form_validation->run()) {
					if (isset($_POST) && $_POST['save'] == 'Change Password') {
						if (($role == "Admin") OR ($role == "Admin2") OR ($role == "Finance")) {
							$password = $this->user_model->getStaffId1($Id);
							$id = $this->user_model->getUserId($Id);
							if ($password == $currentpass) {
								if ($newpass == $confirmpass) {
									$index['id'] = $id;
									$index['password'] = $newpass;
									$this->user_model->change($index);
									$data['success'] = ("");
									$data['error'] = ("");
									$data['info'] = ("");
									$data['mainContent'] = 'Admin/userProfile';
									$data['title'] = "User Profile";
									$data['success'] = ("User Password has been updated successfully");
									log_message('info', 'User:- ' . $this->session->userdata('name') . ' '
											. $this->session->userdata('role') . ' password has been updated successfully');
									$this->load->view('Admin/adminRedirect', $data);
								} else {
									$data['success'] = ("");
									$data['error'] = ("");
									$data['info'] = ("");
									$data['mainContent'] = 'Admin/userProfile';
									$data['title'] = "User Profile";
									$data['error'] = ("User passwords do not match. Please make sure the passwords match");
									log_message('info', 'User:- ' . $this->session->userdata('name') . ' '
											. $this->session->userdata('role') . ' password provided do not match');
									$this->load->view('Admin/adminRedirect', $data);
								}
							} else {
								$data['success'] = ("");
								$data['error'] = ("");
								$data['info'] = ("");
								$data['mainContent'] = 'Admin/userProfile';
								$data['title'] = "User Profile";
								$data['error'] = ("The Current password entered does not match the password in the system");
								log_message('info',
										'User:- ' . $this->session->userdata('name') . ' ' . $this->session->userdata('role')
										. ' The enetered password does not match the password stored into the system');
								$this->load->view('Admin/adminRedirect', $data);
							}
						} elseif ($role == "Agent") {
							$password = $this->user_model->getAgentId1($Id);
							$id = $this->user_model->getUserId2($Id);
							if ($password == $currentpass) {
								if ($newpass == $confirmpass) {
									$index['id'] = $id;
									$index['password'] = $newpass;
									$this->user_model->change($index);
									$data['success'] = ("");
									$data['error'] = ("");
									$data['info'] = ("");
									$data['mainContent'] = 'Admin/userProfile';
									$data['title'] = "User Profile";
									$data['success'] = ("User Password has been updated successfully");
									log_message('info', 'User:- ' . $this->session->userdata('name') . ' '
											. $this->session->userdata('role') . ' password has been updated successfully');
									$this->load->view('Admin/adminRedirect', $data);
								} else {
									$data['success'] = ("");
									$data['error'] = ("");
									$data['info'] = ("");
									$data['mainContent'] = 'Admin/userProfile';
									$data['title'] = "User Profile";
									$data['error'] = ("User passwords do not match. Please make sure the passwords match");
									log_message('info', 'User:- ' . $this->session->userdata('name') . ' '
											. $this->session->userdata('role')
											. ' user password do not match. make sure the password match');
									$this->load->view('Admin/adminRedirect', $data);
								}
							} else {
								$data['success'] = ("");
								$data['error'] = ("");
								$data['info'] = ("");
								$data['mainContent'] = 'Admin/userProfile';
								$data['title'] = "User Profile";
								$data['error'] = ("The Current password entered does not match the password in the system");
								log_message('info',
										'User:- ' . $this->session->userdata('name') . ' ' . $this->session->userdata('role')
										. ' the entered password does not match the password stored by the system');
								$this->load->view('Admin/adminRedirect', $data);
							}
						} else {
							$password = $this->user_model->getShareholderId1($Id);
							$id = $this->user_model->getUserId3($Id);
							if ($password == $currentpass) {
								if ($newpass == $confirmpass) {
									$index['id'] = $id;
									$index['password'] = $newpass;
									$this->user_model->change($index);
									$data['success'] = ("");
									$data['error'] = ("");
									$data['info'] = ("");
									$data['mainContent'] = 'Admin/userProfile';
									$data['title'] = "User Profile";
									$data['success'] = ("User Password has been updated successfully");
									log_message('info', 'User:- ' . $this->session->userdata('name') . ' '
											. $this->session->userdata('role') . ' password has been updated successfully');
									$this->load->view('Admin/adminRedirect', $data);
								} else {
									$data['success'] = ("");
									$data['error'] = ("");
									$data['info'] = ("");
									$data['mainContent'] = 'Admin/userProfile';
									$data['title'] = "User Profile";
									$data['error'] = ("User passwords do not match. Please make sure the passwords match");
									$this->load->view('Admin/adminRedirect', $data);
								}
							} else {
								$data['success'] = ("");
								$data['error'] = ("");
								$data['info'] = ("");
								$data['mainContent'] = 'Admin/userProfile';
								$data['title'] = "User Profile";
								$data['error'] = ("The Current password entered does not match the password in the system");
								$this->load->view('Admin/adminRedirect', $data);
							}
						}
					} else {

					}
				} else {

				}
			} else {
				redirect('systemUsers/accessDenied');
			}
		} else {
			redirect('systemUsers/accessDenied');
		}

	}

	//change both avatars or one of them
	public function changeAvatars() {
		$this->load->library('session');

		$this->load->helper(array ( 'form', 'url' ));

		$config['upload_path'] = './assets/img/users/';
		$config['allowed_types'] = 'gif|jpg|png';
		$config['max_size'] = '10000';
		$config['max_width'] = '10240';
		$config['max_height'] = '7680';
		$config['overwrite'] = FALSE;
		$this->load->library('upload', $config);
		$this->upload->initialize($config);
		$useravatar = 'useravatar';

		if ($this->upload->do_upload($useravatar)) {
			$this->load->library('session');
			//   $error = $this->upload->display_errors();
			$this->admin_model->changeuseravatar();

			echo "1";
		} else {
			$error = $this->upload->display_errors();
			echo $error;
			echo "0";
		}
	}
}



