<!-- /. NAV SIDE  -->
<div id="page-wrapper">
	<div id="page-inner">
		<!--BEGIN TITLE & BREADCRUMB PAGE-->
		<div id="title-breadcrumb-option-demo" class="page-title-breadcrumb">
			<div class="page-header pull-left">
				<div class="page-title">
					Payment Management
				</div>
			</div>
			<ol class="breadcrumb page-breadcrumb pull-right">
				<li><i class="fa fa-home"></i>&nbsp;<a href="<?php echo base_url() . "home" ?>">Home</a>&nbsp;&nbsp;
				</li>
				<li><i class="fa fa-money"></i><a href='<?php echo base_url() . 'payment/paymentHome' ?>'>&nbsp;&nbsp;Payment
						Management</a></li>
				<li class="active"><i class="fa fa-edit"></i>&nbsp;&nbsp;Edit Payments</li>
			</ol>
			<div class="clearfix">
			</div>
		</div>
		<!--END TITLE & BREADCRUMB PAGE-->
		<div class="row">
			<div class="col-md-12">
				<div class="alert alert-info">
					<i class="fa fa-info-circle"></i>
					<strong>Heads up!</strong>
					This helps in buying shares for existing shareholders!
				</div>
				<br/>
				<?php if (strlen($success) > 0) {
					?>
					<div class="alert alert-success" id="success"><i class="fa fa-check"></i>&nbsp;<?php echo $success
							. ''; ?>
					</div>
					<?php
				}
				?>
				<?php if (strlen($error) > 0) {
					?>
					<div class="alert alert-danger" id="error"><i class="fa fa-ban"></i>&nbsp;<?php echo $error . ''; ?>
					</div>
					<?php
				}
				?>
			</div>
		</div>
		<!-- /. ROW  -->
		<div class="row">
			<div class="col-md-12">
				<!-- Advanced Tables -->
				<?php if (($this->session->userdata('role') == "Admin") OR ($this->session->userdata('role') == "Finance")
				OR ($this->session->userdata('role') == "Admin2")) { ?>
				<div class="panel panel-pink">
					<?php } ?>
					<?php if (($this->session->userdata('role') == "Agent")) { ?>
					<div class="panel panel-green">
						<?php } ?>
						<div class="panel-heading">
							<div class="row">
								<div class="col-sm-6">
									<h3>Edit Share Payment</h3>
								</div>
							</div>
						</div>
						<div class="panel-body">
							<?php $this->load->helper('form'); ?>
							<?php echo form_open('payment/update'); ?>
							<div class="form-body pal">
								<div class="row">
									<div class="col-md-4">
										<div class="form-group">
											<label>Payment Type</label>
											<select class="form-control" id="type" name="type">
												<option value="choose">Please Select One....</option>
												<option value="M-Pesa">M-Pesa</option>
												<option value="Cash">Cash</option>
												<option value="Cheque">Cheque</option>
												<option value="Bank Deposit">Bank Deposit</option>

											</select>
										</div>
									</div>
									<div class="col-md-4">
										<div class="form-group">
											<label>Number of Shares Paid For</label>
											<?php echo form_input(array("class" => "form-control",
												"placeholder" => "Number of shares", "name" => "shares",
												"value" => $view_data['numberofshares'], "required" => "true")) ?>
										</div>
									</div>
									<div class="col-md-4">
										<div class="form-group">
											<label>Amount Paid</label>
											<?php echo form_input(array("class" => "form-control",
												"placeholder" => "Amount Paid", "name" => "paid",
												"value" => $view_data['Amount'], "required" => "true")) ?>
										</div>
									</div>
								</div>
								<div class="row">
									<div class="col-md-4">
										<div class="form-group">
											<label>Payment Code</label>
											<?php echo form_input(array("class" => "form-control",
												"placeholder" => "Shareholder", "name" => "code",
												"value" => $view_data['Code'], "required" => "true")) ?>
										</div>
									</div>
									<div class="col-md-4">
										<div class="form-group">
											<label>Physical Receipt Number</label>
											<?php echo form_input(array("class" => "form-control",
												"placeholder" => "Physical Receipt Number", "name" => "fnumber",
												"required" => "true", "type" => "text",
												"value" => $view_data['physicalReceiptNumber'])) ?>
										</div>
									</div>
									<div class="col-md-4">
										<div class="form-group">
											<label>Receiver Name</label>
											<?php echo form_input(array("class" => "form-control",
												"placeholder" => "Receiver Name", "name" => "receiver",
												"readonly" => "true", "value" => $this->session->userdata('name'))) ?>
										</div>
									</div>
								</div>
								<div class="row">
									<div class="col-md-4">
										<label>Share Type</label>
										<?php echo form_input(array("class" => "form-control",
											"placeholder" => "Share Types", "name" => "typename", "readonly" => "true",
											"value" => $sharetypes)) ?>
									</div>
									<div class="col-md-4">
										<div class="form-group">
											<label>Shareholder's Name</label>
											<?php echo form_input(array("class" => "form-control",
												"placeholder" => "Shareholder's Name", "name" => "holdername",
												"readonly" => "true", "value" => $holders)) ?>
										</div>
									</div>
									<div class="col-md-4">
										<label>Shares Bought</label>
										<?php echo form_input(array("class" => "form-control",
											"placeholder" => "Shares Bought", "name" => "bought", "readonly" => "true",
											"value" => $sharesbought)) ?>
									</div>
									<div class="col-md-4">
									</div>
								</div>
								<div class="row">
									<div class="col-md-6">
										<?php if ($view_data != null) { ?>
											<?php echo form_hidden('buyId', $view_data['buyId'], 'class="form-control"'); ?>
										<?php } ?>
										<?php if ($view_data != null) { ?>
											<?php echo form_hidden('Id', $view_data['Id'], 'class="form-control"'); ?>
										<?php } ?>
										<?php if ($view_data != null) { ?>
											<?php echo form_hidden('idnumber', $view_data['idnumber'],
												'class="form-control"'); ?>
										<?php } ?>
										<?php if ($view_data != null) { ?>
											<?php echo form_hidden('sharetype', $view_data['shareType'],
												'class="form-control"'); ?>
										<?php } ?>
										<?php if ($view_data != null) { ?>
											<?php echo form_hidden('shareholder', $view_data['holder'],
												'class="form-control"'); ?>
										<?php } ?>
										<?php if ($shareprice != null) { ?>
											<?php echo form_hidden('price', $shareprice, 'class="form-control"'); ?>
										<?php } ?>
										<?php if ($amountreceived != null) { ?>
											<?php echo form_hidden('amountreceived', $amountreceived,
												'class="form-control"'); ?>
										<?php } ?>
									</div>
									<div class="col-md-6">
										<div class="form-group">
										</div>
									</div>
								</div>
								<div class="row">
									<div class="col-md-4">
										<div class="form-group">
											<?php if ($view_data != null) { ?>
												<?php echo form_hidden('Receiptnumber', $view_data['buyreceipt'],
													'class="form-control"'); ?>
												<?php echo form_hidden('paycode', $view_data['PaymentUUID'],
													'class="form-control"'); ?>
												<?php echo form_hidden('oldnoshare', $view_data['numberofshares'],
													'class="form-control"'); ?>
												<?php echo form_hidden('oldpaid', $view_data['Amount'],
													'class="form-control"'); ?>
											<?php } ?>
										</div>
									</div>
									<div class="col-md-4">
										<div class="form-group">
										</div>
									</div>
									<div class="col-md-4">
										<div class="form-group">
											<button type="reset" class="btn btn-danger pull-right">Cancel</button>
											<?php if ($view_data != null) { ?>
												<?php echo form_submit('update', 'Update Payment',
													'class="btn btn-success pull-right margin-right"'); ?>
											<?php } ?>
										</div>
									</div>
								</div>
							</div>
							</form>
							<!-- /.row (nested) -->
						</div>
						<!-- /.panel-body -->
					</div>
					<!-- /.panel -->
				</div>
				<!-- /.col-lg-12 -->
			</div>
