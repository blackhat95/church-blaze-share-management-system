<!-- /. NAV SIDE  -->
<div id="page-wrapper">
	<div id="page-inner">
		<!--BEGIN TITLE & BREADCRUMB PAGE-->
		<div id="title-breadcrumb-option-demo" class="page-title-breadcrumb no-print">
			<div class="page-header pull-left">
				<div class="page-title">
					General Reporting
				</div>
			</div>
			<ol class="breadcrumb page-breadcrumb pull-right">
				<li><i class="fa fa-home"></i>&nbsp;<a href="<?php echo base_url() . "home" ?>">Home</a>&nbsp;&nbsp;
				</li>
				<li><i class="fa fa-bar-chart-o"></i>&nbsp;&nbsp;<a href="<?php echo base_url() . "reports/reportsHome" ?>">General
						Reporting</a></li>
				<li><i class="fa fa-print"></i>&nbsp;&nbsp;All Shareholders</li>
			</ol>
			<div class="clearfix">
			</div>
		</div>
		<!--END TITLE & BREADCRUMB PAGE-->
		<div class="row">
			<div class="col-md-12">
				<!-- Advanced Tables -->
				<div class="panel panel-report">
					<div class="panel-heading no-print">
						<div class="row">
							<div class="col-sm-6">
								<h3>Shareholders</h3>
							</div>
						</div>
					</div>
					<div class="panel-body">
						<div class="row">
							<h2 class="center receiptheader"><?php echo $view_data1['Name'] ?></h2>

							<h3 class="center receiptheader"><?php echo $view_data1['addressone'] ?>
								,&nbsp;<?php echo $view_data1['addresstwo'] ?></h3>

							<h3 class="center receiptheader">Phonenumber
								:-&nbsp;<?php echo $view_data1['phonenumberone'] ?>
								,&nbsp;<?php echo $view_data1['phonenumbertwo'] ?></h3>
						</div>
						<hr/>
						<div class="row">
							<h3 class="center heading"><b>All Share Holders</b></h3>
							<br/>

							<div>
								<table class="table table-responsive table-striped" id="stafftable">
									<thead>
									<tr>
										<?php foreach ($fields as $field_name => $field_Column): ?>
											<th>
												<?php echo $field_Column ?>
											</th>
										<?php endforeach; ?>
									</tr>
									</thead>
									<tbody>
									<?php foreach ($view_data as $key => $data): ?>
										<tr>
											<?php foreach ($fields as $field_name => $field_Column): ?>
												<td>
													<?php echo $data->$field_name ?>
												</td>
											<?php endforeach; ?>
										</tr>
									<?php endforeach; ?>
									</tbody>
								</table>
							</div>
						</div>
						<hr/>
						<div class="row">
							<div class="col-md-12">
								<table class="table table-condensed">
									<tr>
										<td class="pull-left col-md-6"><p><b>Signature:....................................................</b>
											</p></td>
										<td class="pull-right col-md-6"><p><b>Date:.....................................................</b>
											</p></td>
									</tr>
								</table>
								<table class="table table-condensed">
									<tr>
										<td class="pull-left col-md-6"><p><b>Signature:....................................................</b>
											</p></td>
										<td class="pull-right col-md-6"><p><b>Date:.....................................................</b>
											</p></td>
									</tr>
								</table>
							</div>
						</div>
						<div class="row ">
							<div class="col-md-6">
								<p><b>Terms & Conditions</b></p>
								<ol>
									<li>Applicant must remain with a copy of the application form, pay-in-slip and
										original cash receipt
									</li>
								</ol>
							</div>
						</div>
						<div class="row">
							<div class="col-md-6">

								<p><b>NB:</b>&nbsp;All cash must be deposited in the following back account</p>
								<ul>
									<li><b><?php echo $view_data1['bankname'] ?></b></li>
									<li><b><?php echo $view_data1['bankbranch'] ?></b></li>
									<li><b>A/C Name: <?php echo $view_data1['accountname'] ?></b></li>
									<li><b>A/C NO.<?php echo $view_data1['accountnumber'] ?></b></li>
									<li><b>Paybill Number: <?php echo $view_data1['paybillnumber'] ?></b></li>
								</ul>
							</div>
						</div>
						<hr/>
						<div class="row no-print">
							<div class="col-md-4">
								<div class="form-group">
								</div>
							</div>
							<div class="col-md-4">
								<div class="form-group">
								</div>
							</div>
							<div class="col-md-4">
								<div class="form-group">
									<a class="btn btn-bitbucket pull-right" href="javascript:window.print();"><i
											class="fa fa-print"></i>&nbsp;Print
									</a>
								</div>
							</div>
						</div>

					</div>
				</div>
			</div>
		</div>
	</div>
