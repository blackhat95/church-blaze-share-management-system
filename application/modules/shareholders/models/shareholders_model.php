<?php if (!defined('BASEPATH')) {
	exit('No direct script access allowed');
}

class Shareholders_model extends CI_Model
{

	public function __construct()
	{
		parent::__construct();
	}

	public function get($limit, $offset, $sort_by, $sort_order)
	{

		$sort_order = ($sort_order == 'desc') ? 'desc' : 'asc';
		$sort_columns = array('Name', 'IdNo', 'Telephone', 'Email', 'Box', 'Village', 'Town', 'County', 'Country');
		$sort_by = (in_array($sort_by, $sort_columns)) ? $sort_by : 'Name';

		$q = $this->db->select('*')->from('shareholders')->where('Approval', 0)->where('type', 0)
			->join('users', 'users.link3 = shareholders.Id')->limit($limit, $offset)->order_by($sort_by, $sort_order);
		$query = $q->get()->result();
		$q1 = $this->db->select('COUNT(*) as count', false)->from('shareholders')->where('Approval', 0)->where('type', 0);
		$tmp = $q1->get()->result();

		$q2 = $this->db->select('*')->from('shareholders')->where('Approval', 1)->where('type', 0)
			->join('users', 'users.link3 = shareholders.Id')->limit($limit, $offset)->order_by($sort_by, $sort_order);
		$query2 = $q2->get()->result();
		$q3 = $this->db->select('COUNT(*) as count', false)->from('shareholders')->where('Approval', 1)->where('type', 0);
		$tmp2 = $q3->get()->result();

		$result['rows'] = $query;
		$result['row_count'] = $tmp[0]->count;

		$result['rows1'] = $query2;
		$result['row_count1'] = $tmp2[0]->count;

		return $result;
	}

	public function getCompany($limit, $offset, $sort_by, $sort_order)
	{

		$sort_order = ($sort_order == 'desc') ? 'desc' : 'asc';
		$sort_columns =
			array('Name', 'cin', 'krapin', 'Telephone', 'Email', 'Box', 'Village', 'Town', 'County', 'Country');
		$sort_by = (in_array($sort_by, $sort_columns)) ? $sort_by : 'Name';

		$q = $this->db->select('*')->from('shareholders')->where('Approval', 0)->where('type', 1)
			->join('users', 'users.link3 = shareholders.Id')->limit($limit, $offset)->order_by($sort_by, $sort_order);
		$query = $q->get()->result();
		$q1 = $this->db->select('COUNT(*) as count', false)->from('shareholders')->where('Approval', 0)->where('type', 1);
		$tmp = $q1->get()->result();

		$q2 = $this->db->select('*')->from('shareholders')->where('Approval', 1)->where('type', 1)
			->join('users', 'users.link3 = shareholders.Id')->limit($limit, $offset)->order_by($sort_by, $sort_order);
		$query2 = $q2->get()->result();
		$q3 = $this->db->select('COUNT(*) as count', false)->from('shareholders')->where('Approval', 1)->where('type', 1);
		$tmp2 = $q3->get()->result();

		$result['rows'] = $query;
		$result['row_count'] = $tmp[0]->count;

		$result['rows1'] = $query2;
		$result['row_count1'] = $tmp2[0]->count;

		return $result;
	}

	public function getKin($limit, $offset, $sort_by, $sort_order)
	{

		$sort_order = ($sort_order == 'desc') ? 'desc' : 'asc';
		$sort_columns = array('kinName', 'kinIdentity', 'kinPhonenumber', 'email', 'percentage', 'relation', 'KintoName');
		$sort_by = (in_array($sort_by, $sort_columns)) ? $sort_by : 'KinName';

		$q = $this->db->select('*')->from('shareholders')->join('nextofkin', 'nextofkin.kinTo = shareholders.Id')
			->limit($limit, $offset)->order_by($sort_by, $sort_order);
		$query = $q->get()->result();
		$q1 = $this->db->select('COUNT(*) as count', false)->from('nextofkin');
		$tmp = $q1->get()->result();

		$result['rows'] = $query;
		$result['row_count'] = $tmp[0]->count;

		return $result;
	}

	public function getkinsearch($index)
	{
		$q = $this->db->select('*')->from('shareholders')->join('nextofkin', 'nextofkin.kinTo = shareholders.Id')
			->where('nextofkin.kinIdentity', $index)->or_where('shareholders.IdNo', $index);
		$query = $q->get()->result();

		$result['rows'] = $query;

		return $result;
	}

	public function shareholderKinDetails($index)
	{
		$q = $this->db->select('*')->from('nextofkin')->where('kinTo', $index);
		$query = $q->get()->result();

		$result['rows'] = $query;

		return $result;
	}

	public function shareholderKinPayments($id)
	{
		$q = $this->db->select('*')->from('payment')->join('sharetypes', 'sharetypes.Id = payment.shareType')
			->join('sharesBought', 'sharesBought.buyId= payment.buyId')
			->where('payment.holder', $id);
		$query = $q->get()->result();
		$result['rows'] = $query;
		return $result;
	}

	public function shareholderkinShareSubscription($id)
	{
		$q = $this->db->select('*')->from('sharesBought')->join('sharetypes', 'sharetypes.Id = sharesBought.sharetype')
			->where('sharesBought.shareholder', $id);
		$query = $q->get()->result();
		$result['rows'] = $query;
		return $result;
	}

	public function getIndividualSearch($index = null)
	{
		$this->db->select('*')->from('shareholders');
		$where = "type = '0' AND formnumber = '$index'";
		$where2 = "type = '0' AND IdNo = '$index'";
		$this->db->where($where);
		$this->db->or_where($where2);
		$query = $this->db->get()->result();

		$result['rows'] = $query;

		return $result;
	}

	public function getCompanySearch($index)
	{
		$this->db->select('*')->from('shareholders');
		$where = "type = '1' AND formnumber = '$index'";
		$where2 = "type = '1' AND krapin = '$index'";
		$this->db->where($where);
		$this->db->or_where($where2);
		$query = $this->db->get()->result();

		$result['rows'] = $query;

		return $result;
	}

	public function remove($id)
	{
		$this->db->where('Id', $id);
		$this->db->delete('shareholders');
	}

	public function kinremove($id)
	{
		$this->db->where('kinId', $id);
		$this->db->delete('nextofkin');
	}

	public function add($index)
	{
		if (isset($index['Id'])) {
			$this->db->where('Id', $index['Id']);
			$this->db->update('shareholders', $index);
		} else {
			$this->db->insert('shareholders', $index);
		}

	}

	public function addUser($user)
	{
		if (isset($user['id'])) {
			$this->db->where('id', $user['id']);
			$this->db->update('users', $user);
		} else {
			$this->db->insert('users', $user);
		}

	}

	public function edit($id = null)
	{
		$this->db->select('*');
		$this->db->from('shareholders as sh');
		$this->db->join('users', 'users.link3 = sh.Id');

		if ($id != null) {
			$this->db->where(array('sh.Id' => $id, 'sh.type' => 0));
		}
		$query = $this->db->get();
		if ($id != null) {
			return $query->row_array();
		}
	}

	public function getShareholder($id = null)
	{
		$this->db->select('*');
		$this->db->from('shareholders as sh');
		$this->db->join('users', 'users.link3 = sh.Id');

		if ($id != null) {
			$this->db->where(array('sh.Id' => $id));
		}
		$query = $this->db->get();
		if ($id != null) {
			return $query->row_array();
		}
	}

	public function printout($id = null)
	{
		$this->db->select('*');
		$this->db->from('shareholders as sh');
		$this->db->join('users', 'users.link3 = sh.Id');

		if ($id != null) {
			$this->db->where(array('sh.Id' => $id));
		}
		$query = $this->db->get();
		if ($id != null) {
			return $query->row_array();
		}
	}

	public function editCompany($id = null)
	{
		$this->db->select('*');
		$this->db->from('shareholders as sh');
		$this->db->join('users', 'users.link3 = sh.Id');

		if ($id != null) {
			$this->db->where(array('sh.Id' => $id, 'sh.type' => 1));
		}
		$query = $this->db->get();
		if ($id != null) {
			return $query->row_array();
		}
	}

	public function kinedit($id = null)
	{
		$this->db->select('*');
		$this->db->from('nextofkin');

		if ($id != null) {
			$this->db->where(array('nextofkin.kinId' => $id));
		}
		$query = $this->db->get();
		if ($id != null) {
			return $query->row_array();
		}
	}

	function get_search($index)
	{
		$this->db->select('*');
		$this->db->from('shareholders');
		$this->db->where('IdNo', $index);
		$this->db->where('Approval', 1);
		$this->db->or_where('krapin', $index);
		$query = $this->db->get();

		return $query->row_array();
	}

	function get_search1($idnumber)
	{
		$this->db->select('*');
		$this->db->from('shareholders');
		$this->db->where('IdNo', $idnumber);
		$this->db->where('Approval', 0);
		$this->db->or_where('krapin', $idnumber);
		$query = $this->db->get();

		return $query->row_array();
	}

	function get_search2($idnumber)
	{
		$this->db->select('*');
		$this->db->from('shareholders');
		$this->db->where(array('IdNo' => $idnumber, 'type' => 0, 'Approval' => 1));
		$query = $this->db->get();

		return $query->row_array();
	}

	public function addKin($index)
	{
		if (isset($index['kinId'])) {
			$this->db->where('kinId', $index['kinId']);
			$this->db->update('nextofkin', $index);
		} else {
			$this->db->insert('nextofkin', $index);
		}

	}

	function formNumber($length = 6)
	{
		$string = substr(str_shuffle("0123456789"), 0, $length);
		$formNum = "FORM/" . $string . "/" . date("Y");

		return $formNum;
	}

	function getAgent($id = null)
	{
		$this->db->select('*');
		$this->db->from('agents');
		$this->db->join('shareholders', 'shareholders.agentID = agents.Id');
		$this->db->where('shareholders.Id', $id);
		$this->db->order_by('AgName');
		$query = $this->db->get();

		return $query->row_array();
	}

	function get_agentsdropdown()
	{
		$q = $this->db->select('*')->from('agents')->order_by('AgName');
		$query = $q->get()->result();
		$result['rows'] = $query;

		return $result;
	}

	function nextofkin($id = null)
	{
		$q = $this->db->select('*')->from('nextofkin')->where('nextofkin.kinTo', $id);
		$query = $q->get()->result();
		$request['rows'] = $query;

		return $request;
	}

	function getpayments($id = null)
	{
		$q = $this->db->select('*')->from('shareholders')->join('payment', 'payment.holder = shareholders.Id')
			->where('shareholders.Id', $id)->order_by('payment.Id');
		$query = $q->get()->result();
		$payment['rows'] = $query;

		return $payment;
	}

	function getreg($id = null)
	{
		$q = $this->db->select('*')->from('shareholders')->join('regpay', 'regpay.shareholderId = shareholders.Id')
			->where('shareholders.Id', $id)->order_by('regpay.payId');
		$query = $q->get()->result();
		$reg['rows'] = $query;

		return $reg;
	}

	function getshares($id = null)
	{
		$q = $this->db->select('*')->from('shareholders')->join('sharesBought', 'sharesBought.shareholder = shareholders.Id')
			->where('shareholders.Id', $id)->order_by('sharesBought.buyId');
		$query = $q->get()->result();
		$shares['rows'] = $query;

		return $shares;
	}

	function getusername($username = null)
	{
		$this->db->select('*')->from('users')->where('username', $username);
		$query = $this->db->get();
		foreach ($query->result() as $row) {
			return $row->username;
		}
	}

}
