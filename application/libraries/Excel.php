<?php
/**
 * Created by IntelliJ IDEA.
 * User: dubdabasoduba
 * Date: 10/11/2015
 * Time: 18:18
 */
if (!defined('BASEPATH')) exit('No direct script access allowed');

require_once APPPATH."/third_party/PHPExcel.php";

class Excel extends PHPExcel {
	public function __construct() {
		parent::__construct();
	}
}
