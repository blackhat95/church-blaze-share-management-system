<!-- /. NAV SIDE  -->
<div id="page-wrapper">
	<div id="page-inner">
		<!--BEGIN TITLE & BREADCRUMB PAGE-->
		<div id="title-breadcrumb-option-demo" class="page-title-breadcrumb">
			<div class="page-header pull-left">
				<div class="page-title">
					Shareholder Management
				</div>
			</div>
			<ol class="breadcrumb page-breadcrumb pull-right">
				<li><i class="fa fa-home"></i>&nbsp;<a href="<?php echo base_url() . "home" ?>">Home</a>&nbsp;&nbsp;
				</li>
				<li><i class="fa fa-users"></i><a href='<?php echo base_url() . 'shareholders/shareholderAdd' ?>'>&nbsp;&nbsp;Shareholder
						Management</a></li>
				<li class="active"><i class="fa fa-check-square-o"></i>&nbsp;&nbsp;Approve Shareholders</li>
			</ol>
			<div class="clearfix">
			</div>
		</div>
		<!--END TITLE & BREADCRUMB PAGE-->
		<div class="row">
			<div class="col-md-12">
				<div class="alert alert-info">
					<i class="fa fa-info-circle"></i>
					<strong>Heads up!</strong>
					The approving of Shareholders info in the system!
				</div>
				<br/>
				<?php if (strlen($success) > 0) {
					?>
					<div class="alert alert-success" id="success"><i class="fa fa-check"></i>&nbsp;<?php echo $success
							. ''; ?>
					</div>
					<?php
				}
				?>
				<?php if (strlen($error) > 0) {
					?>
					<div class="alert alert-danger" id="error"><i class="fa fa-ban"></i>&nbsp;<?php echo $error . ''; ?>
					</div>
					<?php
				}
				?>
			</div>
		</div>
		<!-- /. ROW  -->
		<div class="row">
			<div class="col-md-12">
				<!-- Advanced Tables -->
				<?php if (($this->session->userdata('role') == "Admin") OR ($this->session->userdata('role') == "Finance")
				OR ($this->session->userdata('role') == "Admin2")) { ?>
				<div class="panel panel-pink">
					<?php } ?>
					<?php if (($this->session->userdata('role') == "Agent")) { ?>
					<div class="panel panel-green">
						<?php } ?>
						<div class="panel-heading">
							<div class="row">
								<div class="col-sm-6">
									<h3>Approve Shareholders</h3>
								</div>
							</div>
						</div>
						<div class="panel-body">
							<?php $this->load->helper('form'); ?>
							<?php echo form_open('shareholders/approvesave'); ?>
							<div class="form-body pal">
								<div class="row">
									<div class="col-md-6">
										<div class="form-group">
											<label>Shareholder Name</label>
											<?php echo form_input(array("class" => "form-control",
												"placeholder" => "Enter Shareholder names", "name" => "name",
												"value" => $view_data['Name'], "readonly" => "true")) ?>
										</div>
									</div>
									<div class="col-md-6">
										<div class="form-group">
											<label>Approve</label>
											<select class="form-control" id="approval" name="approval">
												<option value="1">Approve</option>
												<option value="0">Reject</option>
											</select>
										</div>
									</div>
								</div>
								<div class="row">
									<div class="col-md-4">
										<div class="form-group">
											<label>Country</label>
											<?php echo form_input(array("class" => "form-control",
												"placeholder" => "Country", "name" => "country",
												"value" => $view_data['Country'], "readonly" => "true")) ?>
										</div>
									</div>
									<div class="col-md-4">
										<div class="form-group">
											<label>County</label>
											<?php echo form_input(array("class" => "form-control",
												"placeholder" => "County", "name" => "county",
												"value" => $view_data['County'], "readonly" => "true")) ?>
										</div>
									</div>
									<div class="col-md-4">
										<div class="form-group">
											<label>Town</label>
											<?php echo form_input(array("class" => "form-control", "placeholder" => "Town",
												"name" => "town", "value" => $view_data['Town'],
												"readonly" => "true")) ?>
										</div>
									</div>
								</div>
								<div class="row">
									<div class="col-md-4">
										<div class="form-group">
											<label>Village</label>
											<?php echo form_input(array("class" => "form-control",
												"placeholder" => "Village", "name" => "village",
												"value" => $view_data['Village'], "readonly" => "true")) ?>
										</div>
									</div>
									<div class="col-md-4">
										<div class="form-group">
											<label>Postal Address</label>
											<?php echo form_input(array("class" => "form-control",
												"placeholder" => "Postal Address", "name" => "postaladdress",
												"value" => $view_data['Box'], "readonly" => "true")) ?>
										</div>
									</div>
									<?php if ($view_data['IdNo'] != null) { ?>
										<div class="col-md-4">
											<div class="form-group">
												<label>Id Number</label>
												<?php echo form_input(array("class" => "form-control",
													"placeholder" => "Id / Passport Number", "name" => "idnumber",
													"value" => $view_data['IdNo'], "readonly" => "true")) ?>
											</div>
										</div>
									<?php } ?>
									<?php if ($view_data['cin'] != null) { ?>
										<div class="col-md-4">
											<div class="form-group">
												<label>Certificate of In Co-oporation</label>
												<?php echo form_input(array("class" => "form-control",
													"placeholder" => "Id / Passport Number", "name" => "cin",
													"value" => $view_data['cin'], "readonly" => "true")) ?>
											</div>
										</div>
									<?php } ?>
								</div>
								<div class="row">
									<?php if ($view_data['cin'] != null) { ?>
										<div class="col-md-4">
											<div class="form-group">
												<label>KRA Pin Number</label>
												<?php echo form_input(array("class" => "form-control",
													"placeholder" => "Id / Passport Number", "name" => "krapin",
													"value" => $view_data['krapin'], "readonly" => "true")) ?>
											</div>
										</div>
									<?php } ?>
									<div class="col-md-4">
										<label>Mobile Number</label>
										<?php echo form_input(array("class" => "form-control",
											"placeholder" => "Mobilenumber | Minimum length of 10 Characters",
											"name" => "mobile", "value" => $view_data['Telephone'],
											"readonly" => "true")) ?>
									</div>
									<div class="col-md-4">
										<div class="form-group">
											<label>Email</label>
											<?php echo form_input(array("class" => "form-control",
												"placeholder" => "Email address", "name" => "email",
												"value" => $view_data['Email'], "readonly" => "true")) ?>
										</div>
									</div>
								</div>
								<div class="row">
									<div class="col-md-6">
										<div class="form-group">
											<label>Added By</label>
											<?php echo form_input(array("class" => "form-control",
												"placeholder" => "Location", "name" => "addedby", "readonly" => "true",
												"value" => $view_data['AddedBy'])) ?>
										</div>
									</div>
									<div class="col-md-6">
										<div class="form-group">
											<label>Edited By</label>
											<?php echo form_input(array("class" => "form-control",
												"placeholder" => "Location", "name" => "editedby", "readonly" => "true",
												"value" => $this->session->userdata('name'))) ?>
										</div>
									</div>
								</div>
								<hr/>
								<div class="row">
									<div class="col-md-4">
										<div class="form-group">
											<div class="input-icon right">
												<?php echo form_hidden('Id', $view_data['Id'], 'class="form-control"'); ?>
												<?php echo form_hidden('type', $view_data['type'],
													'class="form-control"'); ?>
											</div>
										</div>
									</div>
									<div class="col-md-4">
									</div>
									<div class="col-md-4">
										<div class="form-group">
											<button type="reset" class="btn btn-danger pull-right">Cancel</button>
											<?php echo form_submit('approve', 'Approve Shareholder',
												'class="btn btn-success pull-right margin-right"'); ?>

										</div>
									</div>
								</div>
							</div>
							</form>
							<!-- /.row (nested) -->
						</div>
						<!-- /.panel-body -->
					</div>
					<!-- /.panel -->
				</div>
				<!-- /.col-lg-12 -->
			</div>
