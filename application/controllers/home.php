<?php if (!defined('BASEPATH')) {
	exit('No direct script access allowed');
}

class Home extends CI_Controller {
	public function __construct() {
		parent::__construct();
		$this->load->model('users_model');
		$this->load->model('admin_model');

	}

	public function index() {
		$this->load->helper('url');
		$this->load->library('session');
		if ($this->session->userdata('logged_in') == "TRUE") {
			header('Location:' . base_url('systemUsers/dashboard'));
		} else {
			header('Location:' . base_url('systemUsers'));
		}
	}

}

