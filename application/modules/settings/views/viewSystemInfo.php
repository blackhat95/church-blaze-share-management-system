<!-- /. NAV SIDE  -->
<div id="page-wrapper">
	<div id="page-inner">
		<!--BEGIN TITLE & BREADCRUMB PAGE-->
		<div id="title-breadcrumb-option-demo" class="page-title-breadcrumb">
			<div class="page-header pull-left">
				<div class="page-title">
					System Settings
				</div>
			</div>
			<ol class="breadcrumb page-breadcrumb pull-right">
				<li><i class="fa fa-home"></i>&nbsp;<a href="<?php echo base_url() . "home" ?>">Home</a>&nbsp;&nbsp;
				</li>
				<li><i class="fa fa-gears"></i>&nbsp;&nbsp;<a href="<?php echo base_url() . 'settings/settingsHome' ?>">System
						Settings</a></li>
				<li class="active"><i class="fa fa-edit"></i>&nbsp;&nbsp;System Information</li>
			</ol>
			<div class="clearfix">
			</div>
		</div>
		<!--END TITLE & BREADCRUMB PAGE-->
		<div class="row">
			<div class="col-md-12">
				<div class="alert alert-info">
					<i class="fa fa-info-circle"></i>
					<strong>Heads up!</strong>
					This form helps with the addition of the system Infomation to be used as headers in the Reports!
				</div>
				<br/>
				<?php if (strlen($success) > 0) {
					?>
					<div class="alert alert-success" id="success"><i class="fa fa-check"></i>&nbsp;<?php echo $success
							. ''; ?>
					</div>
					<?php
				}
				?>
				<?php if (strlen($error) > 0) {
					?>
					<div class="alert alert-danger" id="error"><i class="fa fa-ban"></i>&nbsp;<?php echo $error . ''; ?>
					</div>
					<?php
				}
				?>
			</div>
		</div>
		<!-- /. ROW  -->
		<div class="row">
			<div class="col-md-12">
				<!-- Advanced Tables -->
				<div class="panel panel-pink">
					<div class="panel-heading">
						<div class="row">
							<div class="col-sm-6">
								<h3>View Ssytem Information</h3>
							</div>
							<div class="col-sm-6"><a class="btn btn-info btn-sm pull-right"
							                         href="<?php echo base_url() . "settings/addSystemInfo" ?>">Add
									System Information</a></div>
						</div>
					</div>
					<div class="panel-body">
						<div class="table-responsive">
							<table class="table table-striped table-hover table-condensed" id="stafftable">
								<thead>
								<tr>
									<?php foreach ($fields as $field_name => $field_Column): ?>
										<th>
											<?php echo $field_Column ?>
										</th>
									<?php endforeach; ?>
								</tr>
								</thead>
								<tbody>
								<?php foreach ($view_data as $key => $data): ?>
									<tr>
										<?php foreach ($fields as $field_name => $field_Column): ?>
											<td>
												<?php echo $data->$field_name ?>
											</td>
										<?php endforeach; ?>
										<td>
											<a class="btn btn-info btn-sm"
											   href="<?php echo base_url() . "settings/modifySystemInfo/" . $data->Id ?>">Edit</a>
										</td>
										<td>
											<a class="btn btn-danger btn-sm"
											   href="<?php echo base_url() . "settings/removeSystemInfo/" . $data->Id ?>"
											   onclick="return confirm('Are you sure you want to delete');">Delete</a>
										</td>
									</tr>
								<?php endforeach; ?>
								</tbody>
							</table>
						</div>
						<!-- /.row (nested) -->
					</div>
					<!-- /.panel-body -->
				</div>
				<!-- /.panel -->
			</div>
			<!-- /.col-lg-12 -->
		</div>
