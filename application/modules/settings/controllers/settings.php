<?php if (!defined('BASEPATH')) {
	exit('No direct script access allowed');
}

class Settings extends MX_Controller
{
	public function __construct()
	{
		parent::__construct();
		$this->load->model('settings/settings_model', 'settings');
		$this->load->model('payment/payment_model', 'payment');
	}

	public function viewShareTypes($sort_by = 'type', $sort_order = 'asc', $offset = 0)
	{
		if (($this->session->userdata('role') === "Admin" OR $this->session->userdata('role') === "Finance")) {
			if ($this->session->userdata('logged_in')) {
				$data['info'] = ("");
				$data['success'] = ("");
				$data['error'] = ("");
				$limit = 5;
				$data['fields'] =
					array("type" => "Share Series", "seriesPrice" => "Share Price", "sharetotal" => "Shares Total",
						"sharesold" => "Shares Subscribed", "amountexpected" => "Total Amount Expected",
						"amountreceived" => "Shares Subscribed Amount Paid", "Description" => "Description");
				$result = $this->settings->get($limit, $offset, $sort_by, $sort_order);

				$data['mainContent'] = 'viewShareTypes';
				$data['title'] = "View Share Types";
				$data['view_data'] = $result['rows'];
				$data['rownumber'] = $result['row_count'];
				//pagination
				$this->load->library('pagination');
				$config = array();
				$config['base_url'] = site_url("settings/viewShareTypes/$sort_by/$sort_order/");
				$config['total_rows'] = $data['rownumber'];
				$config['per_page'] = $limit;
				$config['uri_segment'] = 5;
				$this->pagination->initialize($config);
				$data['pagination'] = $this->pagination->create_links();

				$data['sort_by'] = $sort_by;
				$data['sort_order'] = $sort_order;
				$data['info'] = ("Share Series Loaded Successfully!");
				$this->load->view('Admin/adminRedirect', $data);
			} else {
				redirect('systemUsers/accessDenied');
			}
		} else {
			redirect('systemUsers/accessDenied');
		}

	}

	public function addShareTypes()
	{
		if (($this->session->userdata('role') === "Admin" OR $this->session->userdata('role') === "Finance")) {
			if ($this->session->userdata('logged_in')) {
				$data['success'] = ("");
				$data['error'] = ("");
				$data['mainContent'] = 'addShareTypes';
				$data['title'] = "Add Share Series";
				$data['view_data'] = '';

				$this->load->view('Admin/adminRedirect', $data);
			} else {
				redirect('systemUsers/accessDenied');
			}
		} else {
			redirect('systemUsers/accessDenied');
		}

	}

	public function addSystemInfo()
	{
		if (($this->session->userdata('role') === "Admin")) {
			if ($this->session->userdata('logged_in')) {
				$data['success'] = ("");
				$data['error'] = ("");
				$data['mainContent'] = 'systemInfo';
				$data['title'] = "Add System Infomation";
				$result = $this->settings->editSystemInfo();
				$data['view_data'] = $result;
				$this->load->view('Admin/adminRedirect', $data);
			} else {
				redirect('systemUsers/accessDenied');
			}
		} else {
			redirect('systemUsers/accessDenied');
		}

	}

	public function configureRegistrationPay()
	{
		if (($this->session->userdata('role') === "Admin" OR $this->session->userdata('role') === "Finance")) {
			if ($this->session->userdata('logged_in')) {
				$data['success'] = ("");
				$data['error'] = ("");
				$data['mainContent'] = 'addRegistration';
				$data['title'] = "Configure Registration Payment";
				$result = $this->settings->editRegPay();
				$data['view_data'] = $result;
				$this->load->view('Admin/adminRedirect', $data);
			} else {
				redirect('systemUsers/accessDenied');
			}
		} else {
			redirect('systemUsers/accessDenied');
		}

	}

	public function viewRegpay()
	{
		if (($this->session->userdata('role') === "Admin" OR $this->session->userdata('role') === "Finance")) {
			if ($this->session->userdata('logged_in')) {
				$data['success'] = ("");
				$data['error'] = ("");
				$data['mainContent'] = 'viewRegistration';
				$data['title'] = "Configure Registration Payment";
				$data['fields'] =
					array("amountPay" => 'Amount To Pay', "description" => 'Name', "dateadded" => 'Date Added',
						"AddedBy" => 'Added By');
				$result = $this->settings->getRegPay();
				$data['view_data'] = $result['rows'];
				$data['info'] = ('Registration payment Loaded Successfully!');
				$this->load->view('Admin/adminRedirect', $data);

			} else {
				redirect('systemUsers/accessDenied');
			}
		} else {
			redirect('systemUsers/accessDenied');
		}

	}

	public function viewSystemInfo()
	{
		if (($this->session->userdata('role') === "Admin")) {
			if ($this->session->userdata('logged_in')) {
				$data['success'] = ("");
				$data['error'] = ("");
				$data['mainContent'] = 'viewSystemInfo';
				$data['title'] = "View System Infomation";
				$data['fields'] = array("Name" => "Name", "addressone" => "Addres One", "addresstwo" => "Address Two",
					"phonenumberone" => "Phonenumber One", "phonenumbertwo" => "Phonenumber Two",
					"bankname" => "Bank Name", "bankbranch" => "Bank Branch", "accountname" => "Account Name",
					"accountnumber" => "Account Number", "paybillnumber" => "Paybill");
				$result = $this->settings->getSysInfo();
				$data['view_data'] = $result['rows'];
				$data['info'] = ("System Infomation Loaded Successfully!");
				$this->load->view('Admin/adminRedirect', $data);

			} else {
				redirect('systemUsers/accessDenied');
			}
		} else {
			redirect('systemUsers/accessDenied');
		}

	}

	public function settingsHome()
	{
		if (($this->session->userdata('role') === "Admin" OR $this->session->userdata('role') === "Finance")) {
			if ($this->session->userdata('logged_in')) {
				$data['success'] = ("");
				$data['error'] = ("");
				$data['mainContent'] = 'settingsHome';
				$data['title'] = "System Settings";
				$data['view_data'] = '';

				$this->load->view('Admin/adminRedirect', $data);
			} else {
				redirect('systemUsers/accessDenied');
			}
		} else {
			redirect('systemUsers/accessDenied');
		}

	}

	public function modify($id = null)
	{
		if (($this->session->userdata('role') === "Admin" OR $this->session->userdata('role') === "Finance")) {
			if ($this->session->userdata('logged_in')) {
				if ($id == null) {
					show_error('No shit found', 500);
				} else {
					$index['success'] = ("");
					$index['error'] = ("");
					$index['info'] = ("");
					$index['mainContent'] = 'editShareTypes';
					$data['title'] = "Edit Share Series";
					$index['view_data'] = $this->settings->editGet($id);

					$this->load->view('Admin/adminRedirect', $index);
				}
			} else {
				redirect('systemUsers/accessDenied');
			}
		} else {
			redirect('systemUsers/accessDenied');
		}

	}

	public function modifyRegPay($rpsId = null)
	{
		if (($this->session->userdata('role') === "Admin" OR $this->session->userdata('role') === "Finance")) {
			if ($this->session->userdata('logged_in')) {
				if ($rpsId === null) {
					show_error('No shit found', 500);
				} else {
					$index['success'] = ("");
					$index['error'] = ("");
					$index['info'] = ("");
					$index['mainContent'] = 'editRegistration';
					$index['title'] = 'Configure Registration Payment';
					$index['view_data'] = $this->settings->editGetReg($rpsId);

					$this->load->view('Admin/adminRedirect', $index);
				}
			} else {
				redirect('systemUsers/accessDenied');
			}
		} else {
			redirect('systemUsers/accessDenied');
		}

	}

	public function modifySystemInfo($id = null)
	{
		if (($this->session->userdata('role') == "Admin")) {
			if ($this->session->userdata('logged_in')) {
				if ($id == null) {
					show_error('No shit found', 500);
				} else {
					$index['success'] = ("");
					$index['error'] = ("");
					$index['info'] = ("");
					$index['mainContent'] = 'editSystemInfo';
					$index['title'] = "Edit System Information";
					$index['view_data'] = $this->settings->editSysInfo($id);

					$this->load->view('Admin/adminRedirect', $index);
				}
			} else {
				redirect('systemUsers/accessDenied');
			}
		} else {
			redirect('systemUsers/accessDenied');
		}

	}

	public function remove($id = null)
	{
		if (($this->session->userdata('role') == "Admin" OR $this->session->userdata('role') === "Finance")) {
			if ($this->session->userdata('logged_in')) {
				if ($id == null) {
					show_error('No shit provided', 500);
				} else {
					$this->settings->remove($id);
					$data['success'] = ("");
					$data['error'] = ("");
					$data['info'] = ("");
					$data['success'] = ("Staff deleted successfully");
					header('Location:' . base_url('settings/viewShareTypes', $data));
				}
			} else {
				redirect('systemUsers/accessDenied');
			}
		} else {
			redirect('systemUsers/accessDenied');
		}

	}

	public function removeSystemInfo($id = null)
	{
		if (($this->session->userdata('role') === "Admin")) {
			if ($this->session->userdata('logged_in')) {
				if ($id == null) {
					show_error('No shit provided', 500);
				} else {
					$this->settings->removeSysInfo($id);
					$data['success'] = ("");
					$data['error'] = ("");
					$data['info'] = ("");
					$data['success'] = ("System Information Deleted successfully");
					header('Location:' . base_url('settings/viewSystemInfo', $data));
				}
			} else {
				redirect('systemUsers/accessDenied');
			}
		} else {
			redirect('systemUsers/accessDenied');
		}

	}

	public function removeRegpay($rpsId = null)
	{
		if (($this->session->userdata('role') === "Admin" OR $this->session->userdata('role') === "Finance")) {
			if ($this->session->userdata('logged_in')) {
				if ($rpsId == null) {
					show_error('No shit provided', 500);
				} else {
					$this->settings->removeRegpay($rpsId);
					$data['success'] = ("");
					$data['error'] = ("");
					$data['info'] = ("");
					$data['success'] = ("System Information Deleted successfully");
					header('Location:' . base_url('settings/viewRegpay', $data));
				}
			} else {
				redirect('systemUsers/accessDenied');
			}
		} else {
			redirect('systemUsers/accessDenied');
		}

	}

	public function configurePay()
	{
		if (($this->session->userdata('role') === "Admin" OR $this->session->userdata('role') === "Finance")) {
			if ($this->session->userdata('logged_in')) {
				$data['success'] = ("");
				$data['error'] = ("");
				$data['info'] = ("");
				//$this->load->library('form_validation');
				$this->form_validation->set_rules('amount', 'Enter payment amount', 'required|trim');
				$this->form_validation->set_rules('desc', 'Payment Name', 'required|trim');
				$amount = $this->input->post('amount');
				$desc = $this->input->post('desc');
				$addedby = $this->input->post('addedby');
				if ($this->form_validation->run()) {
					if (isset($_POST) && $_POST['save'] === 'Configure Registration Payment') {
						$index['amountPay'] = $amount;
						$index['description'] = $desc;
						$index['AddedBy'] = $addedby;
						$index['dateadded'] = date('Y-m-d');
						$this->settings->addregpay($index);
						$data['success'] = ("");
						$data['error'] = ("");
						$data['info'] = ("");
						$result = $this->settings->editRegPay();
						$data['view_data'] = $result;
						$data['mainContent'] = 'addRegistration';
						$data['title'] = "Configure Registration Payment";
						$data['success'] = ("Registration pay was added successfully");
						$this->load->view('Admin/adminRedirect', $data);
					} else {
						$data['success'] = ("");
						$data['error'] = ("");
						$data['info'] = ("");
						$data['mainContent'] = 'addRegistration';
						$data['title'] = "Configure Registration Payment";
						$data['error'] = ("Registration pay Could not be registered");

						$this->load->view('Admin/adminRedirect', $data);
					}
				} else {
					$data['success'] = ("");
					$data['error'] = ("");
					$data['info'] = ("");
					$data['mainContent'] = 'addRegistration';
					$data['title'] = "Configure Registration Payment";
					$data['error'] = validation_errors();

					$this->load->view('Admin/adminRedirect', $data);
				}
			} else {
				redirect('systemUsers/accessDenied');
			}
		} else {
			redirect('systemUsers/accessDenied');
		}
	}

	public function editConfigurePay()
	{
		if (($this->session->userdata('role') === "Admin" OR $this->session->userdata('role') === "Finance")) {
			if ($this->session->userdata('logged_in')) {
				$data['success'] = ("");
				$data['error'] = ("");
				$data['info'] = ("");
				$this->load->library('form_validation');
				$this->form_validation->set_rules('amount', 'Enter payment amount', 'required|trim');
				$this->form_validation->set_rules('desc', 'Description', 'required|trim');
				$desc = $this->input->post('desc');
				$rpsId = $this->input->post('rpsId');
				$amount = $this->input->post('amount');
				$addedby = $this->input->post('addedby');
				$dateadded = $this->input->post('dateadded');
				$UpdatedBy = $this->input->post('UpdatedBy');
				if ($this->form_validation->run()) {
					if (isset($_POST) && $_POST['update'] === 'Edit Registration Payment Configuration') {
						$index['rpsId'] = $rpsId;
						$index['amountPay'] = $amount;
						$index['description'] = $desc;
						$index['AddedBy'] = $addedby;
						$index['dateadded'] = $dateadded;
						$index['UpdatedBy'] = $UpdatedBy;
						$index['dateupdated'] = date("Y-m-d");
						$this->settings->addregpay($index);
						header('Location:' . base_url('settings/viewRegpay'));
					} else {
						$data['success'] = ("");
						$data['error'] = ("");
						$data['info'] = ("");
						$data['mainContent'] = 'editRegistration';
						$data['title'] = 'Configure Registration Payment';
						$data['view_data'] = $this->settings->editGetReg($rpsId);
						$data['error'] = ("Registration pay Could not be edited");

						$this->load->view('Admin/adminRedirect', $data);
					}
				} else {
					$data['success'] = ("");
					$data['error'] = ("");
					$data['info'] = ("");
					$data['mainContent'] = 'editRegistration';
					$data['title'] = 'Configure Registration Payment';
					$data['view_data'] = $this->settings->editGetReg($rpsId);
					$data['error'] = ("Please fill all the spaces appropriately");
					$this->load->view('Admin/adminRedirect', $data);
				}
			} else {
				redirect('systemUsers/accessDenied');
			}
		} else {
			redirect('systemUsers/accessDenied');
		}
	}

	public function save()
	{
		if (($this->session->userdata('role') == "Admin" OR $this->session->userdata('role') === "Finance")) {
			if ($this->session->userdata('logged_in')) {
				$data['success'] = ("");
				$data['error'] = ("");
				$data['info'] = ("");
				$this->load->library('form_validation');
				$this->form_validation->set_rules('name', 'Share Name', 'required|trim');
				$this->form_validation->set_rules('price', 'Share Price', 'required|trim');
				$this->form_validation->set_rules('sharetotal', 'Share Total', 'required|trim');
				$this->form_validation->set_rules('desc', 'Description', 'required|trim');
				$price = $this->input->post('price');
				$shares = $this->input->post('sharetotal');
				if ($this->form_validation->run()) {
					if (isset($_POST) && $_POST['save'] == 'Add Share Series') {
						$index['type'] = $this->input->post('name');
						$index['seriesPrice'] = $price;
						$index['sharetotal'] = $shares;
						$index['amountexpected'] = ($shares * $price);
						$index['amountreceived'] = 0;
						$index['Description'] = $this->input->post('desc');
						$index['AddedBy'] = $this->input->post('addedby');
						$index['dateadded'] = date("Y-m-d");
						$this->settings->add($index);
						header('Location:' . base_url('settings/viewShareTypes'));
					} else {
						$data['success'] = ("");
						$data['error'] = ("");
						$data['info'] = ("");
						$data['mainContent'] = 'addShareTypes';
						$data['title'] = "Add Share Series";
						$data['view_data'] = '';
						$data['error'] = ("Share Series Could not be registered");

						$this->load->view('Admin/adminRedirect', $data);
					}
				} else {
					$data['success'] = ("");
					$data['error'] = ("");
					$data['info'] = ("");
					$data['mainContent'] = 'addShareTypes';
					$data['title'] = "Add Share Series";
					$data['view_data'] = '';
					$data['error'] = ("Please fill all the spaces appropriately");

					$this->load->view('Admin/adminRedirect', $data);
				}
			} else {
				redirect('systemUsers/accessDenied');
			}
		} else {
			redirect('systemUsers/accessDenied');
		}
	}

	public function saveSystemInfo()
	{
		if (($this->session->userdata('role') == "Admin")) {
			if ($this->session->userdata('logged_in')) {
				$data['success'] = ("");
				$data['error'] = ("");
				$data['info'] = ("");
				$this->load->library('form_validation');
				$this->form_validation->set_rules('name', 'System Name', 'required|trim');
				$this->form_validation->set_rules('addressone', 'Addres one', 'required|trim');
				$this->form_validation->set_rules('paybill', 'Paybill Number', 'required|trim');
				$this->form_validation->set_rules('phoneone', 'Phonenumber One', 'required|trim');
				$this->form_validation->set_rules('accountnumber', 'Account Number', 'required|trim');
				$this->form_validation->set_rules('bankname', 'Bank Name', 'required|trim');
				$this->form_validation->set_rules('branch', 'Bank Branch', 'required|trim');
				$this->form_validation->set_rules('accountname', 'Account Name', 'required|trim');
				if ($this->form_validation->run()) {
					if (isset($_POST) && $_POST['save'] == 'Add System Info') {
						$index['Name'] = $this->input->post('name');
						$index['addressone'] = $this->input->post('addressone');
						$index['addresstwo'] = $this->input->post('addresstwo');
						$index['phonenumberone'] = $this->input->post('phoneone');
						$index['phonenumbertwo'] = $this->input->post('phonetwo');
						$index['bankname'] = $this->input->post('bankname');
						$index['bankbranch'] = $this->input->post('branch');
						$index['accountname'] = $this->input->post('accountname');
						$index['accountnumber'] = $this->input->post('accountnumber');
						$index['paybillnumber'] = $this->input->post('paybill');
						$index['AddedBy'] = $this->input->post('addedby');
						$index['dateadded'] = date("Y-m-d");
						$this->settings->addSystemInfo($index);
						header('Location:' . base_url('settings/viewSystemInfo'));
					} else {
						$data['success'] = ("");
						$data['error'] = ("");
						$data['info'] = ("");
						$data['mainContent'] = 'systemInfo';
						$data['title'] = "Add System Infomation";
						$data['view_data'] = '';
						$data['error'] = ("Could Not set system Information");

						$this->load->view('Admin/adminRedirect', $data);
					}
				} else {
					$data['success'] = ("");
					$data['error'] = ("");
					$data['info'] = ("");
					$data['mainContent'] = 'systemInfo';
					$data['title'] = "Add System Infomation";
					$data['view_data'] = '';
					$data['error'] = ("Please fill all the spaces appropriately");

					$this->load->view('Admin/adminRedirect', $data);
				}
			} else {
				redirect('systemUsers/accessDenied');
			}
		} else {
			redirect('systemUsers/accessDenied');
		}
	}

	public function editSystemInfo()
	{
		if (($this->session->userdata('role') == "Admin")) {
			if ($this->session->userdata('logged_in')) {
				$data['success'] = ("");
				$data['error'] = ("");
				$data['info'] = ("");
				$this->load->library('form_validation');
				$this->form_validation->set_rules('name', 'System Name', 'required|trim');
				$this->form_validation->set_rules('addressone', 'Addres one', 'required|trim');
				$this->form_validation->set_rules('paybill', 'Paybill Number', 'required|trim');
				$this->form_validation->set_rules('phoneone', 'Phonenumber One', 'required|trim');
				$this->form_validation->set_rules('accountnumber', 'Account Number', 'required|trim');
				$this->form_validation->set_rules('bankname', 'Bank Name', 'required|trim');
				$this->form_validation->set_rules('branch', 'Bank Branch', 'required|trim');
				$this->form_validation->set_rules('accountname', 'Account Name', 'required|trim');
				if ($this->form_validation->run()) {
					if (isset($_POST) && $_POST['edit'] == 'Edit System Info') {
						$index['Id'] = $this->input->post('Id');
						$index['Name'] = $this->input->post('name');
						$index['addressone'] = $this->input->post('addressone');
						$index['addresstwo'] = $this->input->post('addresstwo');
						$index['phonenumberone'] = $this->input->post('phoneone');
						$index['phonenumbertwo'] = $this->input->post('phonetwo');
						$index['bankname'] = $this->input->post('bankname');
						$index['bankbranch'] = $this->input->post('branch');
						$index['accountname'] = $this->input->post('accountname');
						$index['accountnumber'] = $this->input->post('accountnumber');
						$index['paybillnumber'] = $this->input->post('paybill');
						$index['AddedBy'] = $this->input->post('addedby');
						$index['dateadded'] = $this->input->post('dateadded');
						$index['UpdatedBy'] = $this->input->post('editedby');
						$index['dateupdated'] = date("Y-m-d");
						$this->settings->addSystemInfo($index);
						header('Location:' . base_url('settings/viewSystemInfo'));
					} else {
						$data['success'] = ("");
						$data['error'] = ("");
						$data['info'] = ("");
						$data['mainContent'] = 'systemInfo';
						$data['title'] = "Add System Infomation";
						$data['view_data'] = '';
						$data['error'] = ("Could Not update system Information");

						$this->load->view('Admin/adminRedirect', $data);
					}
				} else {
					$data['success'] = ("");
					$data['error'] = ("");
					$data['info'] = ("");
					$data['mainContent'] = 'systemInfo';
					$data['title'] = "Add System Infomation";
					$data['view_data'] = '';
					$data['error'] = ("Please fill all the spaces appropriately");

					$this->load->view('Admin/adminRedirect', $data);
				}
			} else {
				redirect('systemUsers/accessDenied');
			}
		} else {
			redirect('systemUsers/accessDenied');
		}
	}

	public function update($id = null)
	{
		if (($this->session->userdata('role') == "Admin" OR $this->session->userdata('role') === "Finance")) {
			if ($this->session->userdata('logged_in')) {
				$data['success'] = ("");
				$data['error'] = ("");
				$data['info'] = ("");
				$this->load->library('form_validation');
				$this->form_validation->set_rules('name', 'Share Name', 'required|trim');
				$this->form_validation->set_rules('price', 'Share Price', 'required|trim');
				$this->form_validation->set_rules('sharetotal', 'Share Total', 'required|trim');
				$this->form_validation->set_rules('desc', 'Description', 'required|trim');
				$price = $this->input->post('price');
				$shares = $this->input->post('sharetotal');
				if ($this->form_validation->run()) {
					if (isset($_POST) && $_POST['update'] == 'Update Share Types') {
						$index['Id'] = $this->input->post('Id');
						$index['type'] = $this->input->post('name');
						$index['seriesPrice'] = $price;
						$index['sharetotal'] = $shares;
						$index['amountexpected'] = ($shares * $price);
						$index['Description'] = $this->input->post('desc');
						$index['AddedBy'] = $this->input->post('addedby');
						$index['dateadded'] = $this->input->post('dateadded');
						$index['UpdatedBy'] = $this->input->post('editedby');
						$index['dateupdated'] = date("Y-m-d");
						$this->settings->add($index);
						header('Location:' . base_url('settings/viewShareTypes'));
					} else {
						$data['success'] = ("");
						$data['error'] = ("");
						$data['info'] = ("");
						$data['mainContent'] = 'editShareTypes';
						$data['title'] = "Edit Share Series";
						$data['view_data'] = $this->settings->editGet($id);
						$data['error'] = ("Share Series Could not be updated");

						$this->load->view('Admin/adminRedirect', $data);
					}
				} else {
					$data['success'] = ("");
					$data['error'] = ("");
					$data['info'] = ("");
					$data['mainContent'] = 'editShareTypes';
					$data['title'] = "Edit Share Series";
					$data['view_data'] = $this->settings->editGet($id);
					$data['error'] = ("Please fill all the spaces appropriately");

					$this->load->view('Admin/adminRedirect', $data);
				}
			} else {
				redirect('systemUsers/accessDenied');
			}
		} else {
			redirect('systemUsers/accessDenied');
		}

	}

}
