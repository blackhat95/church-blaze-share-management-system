<?php if (!defined('BASEPATH')) {
	exit('No direct script access allowed');
}

class Staff extends MX_Controller
{

	public function __construct()
	{
		parent::__construct();
		$this->load->model('staff/staff_model', 'staff');
		$this->load->model('shareholders/shareholders_model', 'shareholders');
	}

	public function viewStaff($sort_by = 'Fname', $sort_order = 'asc', $offset = 0)
	{
		if ($this->session->userdata('role') == "Admin") {
			if ($this->session->userdata('logged_in')) {
				$limit = 15;
				$data['fields'] = array("WorkNumber" => "Work Id", "Department" => "Department", "Gender" => "Gender",
					"Email" => "Emails", "Mobile" => "Mobile Number");
				$data['headers'] = array("WorkNumber" => "Work Id", "Department" => "Department", "Gender" => "Gender",
					"Email" => "Emails", "Mobile" => "Mobile Number");

				$result = $this->staff->get($limit, $offset, $sort_by, $sort_order);
				$index = null;
				$result2 = $this->staff->getsearch($index);
				$data['info'] = ("");
				$data['success'] = ("");
				$data['error'] = ("");
				$data['mainContent'] = 'viewStaff';
				$data['title'] = "View Staff";
				$data['view_data2'] = $result2['rows'];
				$data['view_data'] = $result['rows'];
				$data['rownumber'] = $result['row_count'];
				$data['info'] = ("Staff Loaded Successfully!");
				log_message('info', 'User:- ' . $this->session->userdata('name') . ' ' . ' viewed staff successfully');
				//pagination
				$this->load->library('pagination');
				$config = array();
				$config['base_url'] = site_url("staff/viewStaff/$sort_by/$sort_order/");
				$config['total_rows'] = $data['rownumber'];
				$config['per_page'] = $limit;
				$config['uri_segment'] = 15;
				$this->pagination->initialize($config);
				$data['pagination'] = $this->pagination->create_links();

				$data['sort_by'] = $sort_by;
				$data['sort_order'] = $sort_order;

				$this->load->view('Admin/adminRedirect', $data);
				$this->load->view('Admin/includes/datatables');
			} else {
				redirect('systemUsers/accessDenied');
			}
		} else {
			redirect('systemUsers/accessDenied');
		}

	}

	public function search($sort_by = 'Id', $sort_order = 'asc', $offset = 0)
	{
		if ($this->session->userdata('role') == "Admin") {
			if ($this->session->userdata('logged_in')) {
				$data['success'] = ("");
				$data['error'] = ("");
				$limit = 15;
				$data['fields'] = array("WorkNumber" => "Work Id", "Department" => "Department", "Gender" => "Gender",
					"Email" => "Emails", "Mobile" => "Mobile Number");
				$data['headers'] = array("WorkNumber" => "Work Id", "Department" => "Department", "Gender" => "Gender",
					"Email" => "Emails", "Mobile" => "Mobile Number");

				$result = $this->staff->get($limit, $offset, $sort_by, $sort_order);
				$index = $this->input->post('search');
				$result2 = $this->staff->getsearch($index);
				$data['info'] = ("");
				$data['success'] = ("");
				$data['error'] = ("");
				$data['mainContent'] = 'viewStaff';
				$data['title'] = "View Staff";
				$data['view_data2'] = $result2['rows'];
				$data['view_data'] = $result['rows'];
				$data['rownumber'] = $result['row_count'];
				if ($result2['rows'] == null) {
					$data['error'] = ("No Staff with specified username was found");
				} else {
					$data['success'] = ("Staff Loaded Successfully!");
				}
				log_message('info', 'User:- ' . $this->session->userdata('name') . ' ' . ' searched for staff '. $index);
				//pagination
				$this->load->library('pagination');
				$config = array();
				$config['base_url'] = site_url("staff/search/$sort_by/$sort_order/");
				$config['total_rows'] = $data['rownumber'];
				$config['per_page'] = $limit;
				$config['uri_segment'] = 15;
				$this->pagination->initialize($config);
				$data['pagination'] = $this->pagination->create_links();

				$data['sort_by'] = $sort_by;
				$data['sort_order'] = $sort_order;

				$this->load->view('Admin/adminRedirect', $data);
				$this->load->view('Admin/includes/datatables');;
			} else {
				redirect('systemUsers/accessDenied');
			}
		} else {
			redirect('systemUsers/accessDenied');
		}
	}

	public function addStaff()
	{
		if ($this->session->userdata('role') == "Admin") {
			if ($this->session->userdata('logged_in')) {
				$data['success'] = ("");
				$data['error'] = ("");
				$data['mainContent'] = 'addStaff';
				$data['title'] = "Add Staff";
				$data['view_data'] = '';

				$this->load->view('Admin/adminRedirect', $data);
			} else {
				redirect('systemUsers/accessDenied');
			}
		} else {
			redirect('systemUsers/accessDenied');
		}

	}

	public function staffEdit($id = null)
	{
		if ($this->session->userdata('role') == "Admin") {
			if ($this->session->userdata('logged_in')) {
				if ($id == null) {
					show_error('No shit found', 500);
				} else {
					$index['success'] = ("");
					$index['error'] = ("");
					$index['info'] = ("");
					$index['mainContent'] = 'editStaff';
					$index['title'] = "Edit Staff";
					$index['view_data'] = $this->staff->editGet($id);

					$this->load->view('Admin/adminRedirect', $index);
				}
			} else {
				redirect('systemUsers/accessDenied');
			}
		} else {
			redirect('systemUsers/accessDenied');
		}

	}

	public function remove($id = null)
	{
		if ($this->session->userdata('role') == "Admin") {
			if ($this->session->userdata('logged_in')) {
				if ($id == null) {
					show_error('No shit provided', 500);
				} else {
					$this->staff->remove($id);
					$data['success'] = ("");
					$data['error'] = ("");
					$data['info'] = ("");
					$data['success'] = ("Staff deleted successfully");
					log_message('info', 'User:- ' . $this->session->userdata('name') . ' ' . $this->session->userdata('role')
						. ' deleted the staff member ' . $id);
					header('Location:' . base_url('staff/viewStaff', $data));
				}
			} else {
				redirect('systemUsers/accessDenied');
			}
		} else {
			redirect('systemUsers/accessDenied');
		}

	}

	public function save()
	{
		if ($this->session->userdata('role') == "Admin") {
			if ($this->session->userdata('logged_in')) {
				$data['success'] = ("");
				$data['error'] = ("");
				$data['info'] = ("");
				$this->load->library('form_validation');
				$this->form_validation->set_rules('fname', 'First Name', 'required|trim');
				$this->form_validation->set_rules('sname', 'Second Name', 'required|trim');
				$this->form_validation->set_rules('lname', 'Last Name', 'required|trim');
				$this->form_validation->set_rules('worknumber', 'Work number', 'required|trim');
				$this->form_validation->set_rules('idnumber', 'Id Number', 'required|trim');
				$this->form_validation->set_rules('department', 'Department', 'required|trim');
				$this->form_validation->set_rules('mobile', 'Mobile number', 'required|trim|min_length[10]');
				$this->form_validation->set_rules('email', 'Email address', 'required|trim|valid_email');
				$this->form_validation->set_rules('dob', 'Date of Birth', 'required|trim');
				$this->form_validation->set_message('is_unique', "This email has already been taken");
				$fname = $this->input->post('fname');
				$sname = $this->input->post('sname');
				$lname = $this->input->post('lname');
				$username = $this->input->post('idnumber');;
				$exist = $this->staff->getidnumber($username);
				if ($this->form_validation->run()) {
					if ($exist != $username) {
						if (isset($_POST) && $_POST['save'] == 'Add Staff') {
							$index['Fname'] = $fname;
							$index['Sname'] = $sname;
							$index['Lname'] = $lname;
							$index['WorkNumber'] = $this->input->post('worknumber');
							$index['idnumber'] = $username;
							$index['Department'] = $this->input->post('department');
							$index['Gender'] = $this->input->post('gender');
							$index['DOB'] = $this->input->post('dob');
							$index['Email'] = $this->input->post('email');
							$index['Mobile'] = $this->input->post('mobile');
							$index['AddedBy'] = $this->input->post('addedby');
							$index['dateadded'] = date("Y-m-d");
							$this->staff->add($index);
							$user['username'] = $username;
							$user['password'] = md5("password");
							$user['role'] = $this->input->post('role');
							$user['link2'] = $this->db->insert_id();;
							$this->staff->addUser($user);
							$data['success'] = ("");
							$data['error'] = ("");
							$data['info'] = ("");
							$data['mainContent'] = 'addStaff';
							$data['title'] = "Add Staff";
							$data['view_data'] = '';
							/*Start email to user*/
							//send email to user
							$config = Array($config['protocol'] = 'smtp',
								$config['smtp_host'] = 'ssl://root.server-ke68.com', $config['smtp_port'] = '465',
								$config['smtp_user'] = 'info@churchblazegroup.com', $config['smtp_pass'] = '@Mwal1mu1',
								$config['mailtype'] = 'html', $config['charset'] = 'utf-8', $config['wordwrap'] = TRUE,
								$config['newline'] = "\r\n", $config['protocol'] = "sendmail",
								$config['mailpath'] = '/usr/sbin/sendmail',);

							$this->load->library('email');
							$this->email->initialize($config);
							$this->email->from('info@churchblazegroup.com', 'Investor relations Management System Support');
							$this->email->to($this->input->post('email'));
							$this->email->subject("Investor relations Management System Credentials");

							$message = "Welcome to the Investor Management System.\r\n";
							$message .= "Your Username is :- $username.\r\n";
							$message .= "Your Password is :- Password.\r\n";
							$message .= "Please make sure you change your password after your first login. keep this information confidential\r\n";
							$message .= "Investor Management system Url:  " . base_url() . "\r\n";
							$message .= "Do not reply. This email was send automatically by the Investor Management
							System\r\n";

							$this->email->message($message);
							if ($this->email->send()) {
								$data['success'] = "Staff added Successfully";
							} else {
								show_error($this->email->print_debugger());
							}
							/*Complete email to users*/
							log_message('info',
								'User:- ' . $this->session->userdata('name') . ' ' . $this->session->userdata('role')
								. ' added the staff member ' . $fname . ' ' . $sname . ' ' . $lname);
							$this->load->view('Admin/adminRedirect', $data);
						} else {
							$data['success'] = ("");
							$data['error'] = ("");
							$data['info'] = ("");
							$data['mainContent'] = 'addStaff';
							$data['title'] = "Staff";
							$data['view_data'] = '';
							$data['error'] = ("Staff Could not be registered");
							log_message('error',
								'User:- ' . $this->session->userdata('name') . ' ' . $this->session->userdata('role')
								. ' could not add the staff member ' . $fname . ' ' . $sname . ' ' . $lname);
							$this->load->view('Admin/adminRedirect', $data);
						}
					} else {
						$data['success'] = ("");
						$data['error'] = ("");
						$data['info'] = ("");
						$data['mainContent'] = 'addStaff';
						$data['title'] = "Add Staff";
						$data['view_data'] = '';
						$data['error'] = ("The Id number entered already exists. You can not have staff with the same Id
						number");
						log_message('error', 'The Id number entered already exists. You can not have staff with the same Id
						number');
						$this->load->view('Admin/adminRedirect', $data);
					}
				} else {
					$data['success'] = ("");
					$data['error'] = ("");
					$data['info'] = ("");
					$data['mainContent'] = 'addStaff';
					$data['title'] = "Add Staff";
					$data['view_data'] = '';
					$data['error'] = ("Please fill all the spaces appropriately");
					log_message('error', 'Please all the necessary fields to add a staff member');
					$this->load->view('Admin/adminRedirect', $data);
				}
			} else {
				redirect('systemUsers/accessDenied');
			}
		} else {
			redirect('systemUsers/accessDenied');
		}

	}

	public function update($id = null)
	{
		if ($this->session->userdata('role') == "Admin") {
			if ($this->session->userdata('logged_in')) {
				$data['success'] = ("");
				$data['error'] = ("");
				$data['info'] = ("");
				$this->load->library('form_validation');
				$this->form_validation->set_rules('fname', 'First Name', 'required|trim');
				$this->form_validation->set_rules('sname', 'Second Name', 'required|trim');
				$this->form_validation->set_rules('lname', 'Last Name', 'required|trim');
				$this->form_validation->set_rules('worknumber', 'Work number', 'required|trim');
				$this->form_validation->set_rules('idnumber', 'Id number', 'required|trim');
				$this->form_validation->set_rules('department', 'Department', 'required|trim');
				$this->form_validation->set_rules('mobile', 'Mobile number', 'required|trim');
				$this->form_validation->set_rules('email', 'Email address', 'required|trim|valid_email');
				$this->form_validation->set_rules('dob', 'Date of Birth', 'required|trim');
				$this->form_validation->set_message('is_unique', "This email has already been taken");
				$fname = $this->input->post('fname');
				$sname = $this->input->post('sname');
				$lname = $this->input->post('lname');
				$ID = $this->input->post('Id');
				$username = $this->input->post('idnumber');;
				$exist = $this->staff->getidnumber($username);
				if ($this->form_validation->run()) {
					if (isset($_POST) && $_POST['update'] == 'Update') {
						$index['Id'] = $ID;
						$index['Fname'] = $fname;
						$index['Sname'] = $sname;
						$index['Lname'] = $lname;
						$index['WorkNumber'] = $this->input->post('worknumber');
						$index['idnumber'] = $username;
						$index['Department'] = $this->input->post('department');
						$index['Gender'] = $this->input->post('gender');
						$index['DOB'] = $this->input->post('dob');
						$index['Email'] = $this->input->post('email');
						$index['Mobile'] = $this->input->post('mobile');
						$index['AddedBy'] = $this->input->post('addedby');
						$index['dateadded'] = $this->input->post('dateadded');
						$index['UpdatedBy'] = $this->input->post('editedby');
						$index['dateupdated'] = date("Y-m-d");
						$this->staff->add($index);
						$user['id'] = $this->input->post('id');
						$user['username'] = $username;
						$user['password'] = md5("password");
						$user['role'] = $this->input->post('role');
						$user['link2'] = $this->input->post('Id');
						$this->staff->addUser($user);
						log_message('info',
							'User:- ' . $this->session->userdata('name') . ' ' . $this->session->userdata('role')
							. ' edited the staff member ' . $fname . ' ' . $sname . ' ' . $lname);
						$data['success'] = ("");
						$data['error'] = ("");
						$data['mainContent'] = 'editStaff';
						$data['title'] = "Edit Staff";
						$data['view_data'] = $this->staff->editGet($ID);
						$data['success'] = "Staff Updated Successfully";

						/*Start email to user*/
						//send email to user
						$config = Array($config['protocol'] = 'smtp', $config['smtp_host'] = 'ssl://root.server-ke68.com',
							$config['smtp_port'] = '465', $config['smtp_user'] = 'info@churchblazegroup.com',
							$config['smtp_pass'] = '@Mwal1mu1', $config['mailtype'] = 'html',
							$config['charset'] = 'utf-8', $config['wordwrap'] = TRUE, $config['newline'] = "\r\n",
							$config['protocol'] = "sendmail", $config['mailpath'] = '/usr/sbin/sendmail',);

						$this->load->library('email');
						$this->email->initialize($config);
						$this->email->from('info@churchblazegroup.com', 'Investor relations Management System Support');
						$this->email->to($this->input->post('email'));
						$this->email->subject("Investor relations Management System Credentials");

						$message = "Welcome to the Investor Management System.\r\n";
						$message .= "Your Username is :- $username.\r\n";
						$message .= "Your Password is :- Password.\r\n";
						$message .= "Please make sure you change your password after your first login. keep this information confidential\r\n";
						$message .= "Investor Management system Url:  " . base_url() . "\r\n";

						$this->email->message($message);
						if ($this->email->send()) {
							$data['success'] = "Staff Updated Successfully";
						} else {
							show_error($this->email->print_debugger());
						}
						/*Complete email to users*/

						$this->load->view('Admin/adminRedirect', $data);
					} else {
						$data['success'] = ("");
						$data['error'] = ("");
						$data['mainContent'] = 'editStaff';
						$data['title'] = "Edit Staff";
						$data['view_data'] = $this->staff->editGet($ID);
						$data['error'] = "Staff Could not be updated";
						log_message('error',
							'User:- ' . $this->session->userdata('name') . ' ' . $this->session->userdata('role')
							. ' could not edit the staff member ' . $fname . ' ' . $sname . ' ' . $lname);
						$this->load->view('Admin/adminRedirect', $data);
					}
				} else {
					$data['success'] = ("");
					$data['error'] = ("");
					$data['info'] = ("");
					$data['mainContent'] = 'editStaff';
					$data['title'] = "Edit Staff";
					$data['view_data'] = $this->staff->editGet($ID);
					$data['error'] = ("Please fill all the spaces appropriately");
					log_message('error', 'Please all the necessary fields to add a staff member');
					$this->load->view('Admin/adminRedirect', $data);
				}
			} else {
				redirect('systemUsers/accessDenied');
			}
		} else {
			redirect('systemUsers/accessDenied');
		}

	}
}
