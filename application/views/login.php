<?php if ( ! defined( 'BASEPATH' ) ) {
	exit( 'No direct script access allowed' );
} ?>
<!DOCTYPE html>
<html class="bg-black" lang="en">
<head>
	<meta charset="UTF-8" content="">
	<link rel="shortcut icon" href="<?php echo base_url( '/assets/img/favicon.ico' ) ?>" type="image/x-icon">
	<link rel="icon" href="<?php echo base_url( '/assets/img/favicon.ico' ) ?>" type="image/x-icon">
	<title>{CBSS} | <?php echo $title; ?></title>
	<meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>
	<link type="text/css" rel="stylesheet" href="<?php echo base_url(); ?>assets/css/font-awesome.css">
	<link type="text/css" rel="stylesheet" href="<?php echo base_url(); ?>assets/css/bootstrap.css">
	<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/css/login.css"/>
	<script src="<?php echo base_url(); ?>assets/js/jquery.min.js"></script>
	<script src="<?php echo base_url(); ?>assets/js/html5shiv.js" type="text/javascript"></script>
	<script src="<?php echo base_url(); ?>assets/js/respond.js" type="text/javascript"></script>
	<script type="text/javascript">
		window.setTimeout(function () {
			$("#error").fadeTo(500, 0).slideUp(500, function () {
				$(this).remove();
			});
		}, 2500);
	</script>
	<script type="text/javascript">
		window.setTimeout(function () {
			$("#success").fadeTo(500, 0).slideUp(500, function () {
				$(this).remove();
			});
		}, 2500);
	</script>
</head>
<body class="bg-black" onload='document.login-box.username.focus();'>

<div class="form-box" id="login-box">
	<div class="header">Investor Relations | System Login</div>
	<form  <?php echo form_open( 'index.php/systemUsers' ); ?>
	<div class="body bg-gray">
		<br/>
		<?php if ( strlen( $success ) > 0 ) {
			?>
			<div class="alert alert-success" id="success"><i class="fa fa-check"></i>&nbsp;<?php echo $success . ''; ?>
			</div>
			<?php
		}
		?>
		<?php if ( strlen( $error ) > 0 ) {
			?>
			<div class="alert alert-danger" id="error"><i class="fa fa-ban"></i>&nbsp;<?php echo $error . ''; ?></div>
			<?php
		}
		?>
		<div class="form-group">
			<input type="text" name="username" class="form-control" placeholder="Username"
			       id="username" pattern="[0-9]+" required="" title="Enter Username"
			       value="<?php echo set_value( 'username' ); ?>"/>

			<div id="username-error"> <?php echo form_error( 'username' ); ?></div>
		</div>
		<div class="form-group">
			<input type="password" name="password" class="form-control" placeholder="Password"
			       id="password" required=""
			       value="<?php echo set_value( 'password' ); ?>"/>

			<div id="password-error"> <?php echo form_error( 'password' ); ?></div>
		</div>
		<br/>
	</div>
	<div class="footer">
		<button type="submit" class="btn bg-navy btn-block" name="Login" id="Login"><i class="fa fa-sign-in fa-fw"></i>Sign
			in
		</button>
		<p class="text-center">Churchblaze Limited &copy;&nbsp;<?php echo date( "Y" ); ?> | Designed &amp; Maintained by : <a
				class="" rel="nofollow"
				href="http://www.api-crafttech.com"
				target="_blank">Api-Craft
				Technology</a></p>
	</div>
	</form>
</div>


<!-- Bootstrap -->
<script src='<?php echo base_url(); ?>assets/js/bootstrap.min.js'></script>

</body>
</html>
