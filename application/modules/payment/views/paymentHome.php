<!-- /. NAV SIDE  -->
<div id="page-wrapper">
	<div id="page-inner">
		<!--BEGIN TITLE & BREADCRUMB PAGE-->
		<div id="title-breadcrumb-option-demo" class="page-title-breadcrumb">
			<div class="page-header pull-left">
				<div class="page-title">
					Payment Management
				</div>
			</div>
			<ol class="breadcrumb page-breadcrumb pull-right">
				<li><i class="fa fa-home"></i>&nbsp;<a href="<?php echo base_url() . "home" ?>">Home</a>&nbsp;&nbsp;
				</li>
				<li><i class="fa fa-money"></i>&nbsp;&nbsp;Payment Home</li>
			</ol>
			<div class="clearfix">
			</div>
		</div>
		<!--END TITLE & BREADCRUMB PAGE-->
		<div class="row">
			<div class="alert alert-info">
				<i class="fa fa-info-circle"></i>
				<strong>Heads up!</strong>
				Allows you to choose the kind of payment to Add OR View!
			</div>
			<hr/>
			<ul class="nav nav-tabs nav-justified">
				<li class="active"><a data-toggle="tab" href="#sharespayment"><h5><i class="fa fa-credit-card"></i>&nbsp;Shares
							Payment</h5></a></li>
				<li><a data-toggle="tab" href="#registrationpay"><h5><i class="fa fa-money"></i>&nbsp;Registration Payment
						</h5></a>
				</li>
			</ul>
			<div class="tab-content">
				<div id="sharespayment" class="tab-pane fade in active">
					<hr/>
					<div class="col-md-6 col-sm-12 col-xs-12">
						<div class="panel panel-primary text-center no-boder bg-color-brown">
							<div class="panel-body">

								<a href="<?php echo base_url() . 'payment/addPayment' ?>"><i
										class="fa fa-credit-card fa-10x bg-color-brown"></i></a>

								<h3></h3>
							</div>
							<div class="panel-footer back-footer-brown">
								<a class="white" href="<?php echo base_url() . 'payment/addPayment' ?>">Add Share
									Payment</a>
							</div>
						</div>
					</div>
					<?php
					if (($this->session->userdata('role') === 'Admin') OR ($this->session->userdata('role') === 'Finance')) {
						?>
						<div class="col-md-6 col-sm-12 col-xs-12">
							<div class="panel panel-primary text-center no-boder bg-color-blue">
								<div class="panel-body">
									<a href="<?php echo base_url() . 'payment/viewPayment' ?>"><i
											class="fa fa-edit fa-10x bg-color-blue"></i></a>

									<h3></h3>
								</div>
								<div class="panel-footer back-footer-blue">
									<a class="white"
									   href="<?php echo base_url() . 'payment/viewPayment' ?>">View
										Share Payment</a>
								</div>
							</div>
						</div>
					<?php } ?>
				</div>
				<div id="registrationpay" class="tab-pane fade">
					<hr/>
					<div class="col-md-6 col-sm-12 col-xs-12">
						<div class="panel panel-primary text-center no-boder bg-color-royalpurple">
							<div class="panel-body">
								<a href="<?php echo base_url() . 'payment/addRegPayemnt' ?>"><i
										class="fa fa-money fa-10x bg-color-royalpurple"></i></a>

								<h3></h3>
							</div>
							<div class="panel-footer back-footer-royalpurple">
								<a class="white" href="<?php echo base_url() . 'payment/addRegPayemnt' ?>">Add
									Registration Payment</a>
							</div>
						</div>
					</div>
					<?php
					if (($this->session->userdata('role') === 'Admin') OR ($this->session->userdata('role') === 'Finance')) {
					?>
					<div class="col-md-6 col-sm-12 col-xs-12">
						<div class="panel panel-primary text-center no-boder bg-color-TribalTriumph">
							<div class="panel-body">
								<a href="<?php echo base_url() . 'payment/viewRegPayment' ?>"><i
										class="fa fa-edit fa-10x bg-color-TribalTriumph"></i></a>

								<h3></h3>
							</div>
							<div class="panel-footer back-footer-TribalTriumph">
								<a class="white"
								   href="<?php echo base_url() . 'payment/viewRegPayment' ?>">View
									Registration Payment</a>
							</div>
						</div>
					</div>
				</div>
				<?php } ?>
			</div>
		</div>
	</div>



