<?php

class admin_model extends CI_Model {
	private $username;
	private $request_id;

	public function __construct() {
		$this->load->database();
	}

	public function get_admin($username) {
		$query = $this->db->query("select * from user where user.username = '" . $username . "'");

		return $query->result_array();
	}

	public function get_staff() {
		$query = $this->db->query("select * from user where user.account_type != " . "'admin'");

		return $query->result_array();
	}

	public function registersupervisor() {
		$username = $this->input->post("username");
		$useremail = $this->input->post("email");
		$type = $this->input->post("type");

		$sql = "INSERT INTO user (username,useremail,account_type,password) " . "VALUES (" . $this->db->escape($username)
				. "," . $this->db->escape($useremail) . "," . $this->db->escape($type) . ","
				. $this->db->escape(md5("success")) . ")";
		$this->db->query($sql);

	}

	public function deletesupervisor($userid) {

		$sql = "DELETE FROM user WHERE userid = '" . $userid . "'";
		$this->db->query($sql);
	}

	public function load_staff() {

		$query = $this->db->query('select * from user where account_type !=' . '"admin"');

		return $query->result_array();

	}

	//prescription

	public function deleteprescription() {

		$prescription_id = $this->get_prescriptionid();

		$sql = "DELETE FROM prescriptions WHERE prescription_id = '" . $prescription_id . "'";
		$this->db->query($sql);
	}

	public function get_prescriptionid() {
		$prescriptionname = $this->input->post("prescription");
		$query = $this->db->query("select * from prescriptions where prescription_name = '" . $prescriptionname
				. "' ORDER BY prescription_id DESC");
		foreach ($query->result() as $row) {
			return $row->prescription_id;
		}
	}

	public function get_prescriptions() {
		$query = $this->db->query('select * from prescriptions');

		return $query->result_array();
	}

	public function get_prescription($patient_id) {
		$query = $this->db->query('select * from prescriptions where patient_id = ' . $patient_id
				. 'and prescription_edate >=CURDATE() ');

		return $query->result_array();
	}

	public function get_patientprescription($patient_id) {
		$query = $this->db->query('select * from prescriptions where patient_id = ' . $patient_id
				. 'ORDER BY prescription_edate DESC ');

		return $query->result_array();
	}

	public function get_prescriptiondetails($patient_id) {
		$query = $this->db->query('select * from prescription_details where patient_id = ' . $patient_id
				. 'and prescription_edate >=CURDATE() ');

		return $query->result_array();
	}

	public function addprescription() {
		$prescriptionname = $this->input->post("name");
		$patient_id = $this->input->post("patient_id");
		$s_date = $this->input->post("s_date");
		$e_date = $this->input->post("e_date");
		$prescriptiondesc = $this->input->post("desc");

		$sql =
				"INSERT INTO prescriptions (prescription_name,patient_id,prescription_description,prescription_sdate,prescription_edate) "
				. "VALUES (" . $this->db->escape($prescriptionname) . "," . $this->db->escape($patient_id) . ","
				. $this->db->escape($prescriptiondesc) . "," . $this->db->escape($s_date) . "," . $this->db->escape($e_date)
				. ")";
		$this->db->query($sql);

	}

	public function editprescription() {
		$prescription_id = $this->input->post("pid");
		$prescriptionname = $this->input->post("name");
		$patient_id = $this->input->post("category");
		$s_date = $this->input->post("s_date");
		$e_date = $this->input->post("e_date");
		$prescriptiondesc = $this->input->post("desc");

		$sql = "UPDATE prescriptions SET prescription_name = " . $this->db->escape($prescriptionname) . ",
             patient_id = " . $this->db->escape($patient_id) . ", prescription_description = "
				. $this->db->escape($prescriptiondesc) . ",prescription_sdate = " . $this->db->escape($s_date)
				. ",prescription_edate =" . $this->db->escape($e_date) . "WHERE prescription_id = '" . $prescription_id
				. "'";
		$this->db->query($sql);

	}

	public function add_prescriptiondetails($prescription_id) {
		$time = $this->input->post("time");

		$sql = "INSERT INTO prescription_details (prescription_id,time) " . "VALUES (" . $this->db->escape($prescription_id)
				. "," . $this->db->escape($time) . ")";
		$this->db->query($sql);

	}

	public function check_patient($patient_id) {

		$query = $this->db->query('select * from patient_details where patient_id = "' . $patient_id . '"');

		return $query->result_array();
	}

	public function logindetails() {
		$username = $this->input->post("username");
		$query = $this->db->query("select * from user where username = '" . $username . "'");
		foreach ($query->result() as $row) {
			return $row->password;
		}
	}

	public function editadmin() {
		$this->load->library('session');
		$user = $this->session->userdata('username');
		$username = $this->input->post("username");
		$email = $this->input->post("email");

		$sql = "UPDATE user SET user.username = " . $this->db->escape($username) . ",
   useremail = " . $this->db->escape($email) . " where user.username = '" . $user . "'";
		$this->db->query($sql);

	}

	public function changeuseravatar() {
		$upload_data = $this->upload->data();
		$ppic = $upload_data['file_name'];
		$this->load->helper('url');
		$pic = base_url("assets/img/users/" . $ppic);

		$this->load->library('session');
		$user = $this->session->userdata('username');

		$sql = "UPDATE user SET user_avatar = '" . $pic . "' where user.username = '" . $user . "'";

		$this->db->query($sql);
	}

	public function get_patients() {
		$query = $this->db->query("select * from patient_details");

		return $query->result_array();
	}

	public function get_staffs() {
		$query = $this->db->query("select * from user");

		return $query->result_array();
	}

}
