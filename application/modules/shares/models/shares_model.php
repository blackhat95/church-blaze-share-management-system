<?php if (!defined('BASEPATH')) {
	exit('No direct script access allowed');
}

class Shares_model extends CI_Model
{

	public function __construct()
	{
		parent::__construct();
	}

	public function get($limit, $offset, $sort_by, $sort_order)
	{
		$sort_order = ($sort_order == 'desc') ? 'desc' : 'asc';
		$sort_columns = array('IDNo', 'Location', 'AgName', 'Number', 'Telephone', 'Email');
		$sort_by = (in_array($sort_by, $sort_columns)) ? $sort_by : 'AgName';

		$q = $this->db->select('*')->from('agents as ag')->join('users', 'users.link = ag.Id')->limit($limit, $offset)
			->order_by($sort_by, $sort_order);

		$query = $q->get()->result();

		$q = $this->db->select('COUNT(*) as count', false)->from('agents');
		$tmp = $q->get()->result();

		$result['rows'] = $query;
		$result['row_count'] = $tmp[0]->count;

		return $result;

	}

	public function editGet($buyId = null)
	{
		$this->db->select('*')->from('shareholders')->join('sharesBought as sG', 'sG.shareholder = shareholders.Id')
			->where('buyId', $buyId);

		if ($buyId != null) {
			$this->db->where('buyId', $buyId);;
		}
		$query = $this->db->get();
		if ($buyId != null) {
			return $query->row_array();
		}
	}

	public function editGetReceipt($receipt = null)
	{
		$this->db->select('*')->from('shareholders')->join('sharesBought as sG', 'sG.shareholder = shareholders.Id')
			->where('Receiptnumber', $receipt);

		if ($receipt != null) {
			$this->db->where('Receiptnumber', $receipt);;
		}
		$query = $this->db->get();
		if ($receipt != null) {
			return $query->row_array();
		}
	}

	public function updateShareTypes($index)
	{
		$this->db->where('Id', $index['Id']);
		$this->db->update('sharetypes', $index);
	}

	public function remove($id)
	{
		$this->db->where('buyId', $id);
		$this->db->delete('sharesBought');
	}

	public function add($index)
	{
		if (isset($index['buyId'])) {
			$this->db->where('buyId', $index['buyId']);
			$this->db->update('sharesBought', $index);
		} else {
			$this->db->insert('sharesBought', $index);
		}
	}

	public function updateShareType($user)
	{
		if (isset($user['Id'])) {
			$this->db->where('Id', $user['Id']);
			$this->db->update('sharetypes', $user);
		} else {
			$this->db->insert('sharetypes', $user);
		}

	}

	function getshareTypes()
	{
		$q = $this->db->select('*')->from('sharetypes')->order_by('type');
		$query = $q->get()->result();
		$result['rows'] = $query;

		return $result;
	}

	function getTotal($type)
	{
		$this->db->select('*');
		$this->db->from('sharetypes');
		$this->db->where('Id', $type);
		$query = $this->db->get();
		foreach ($query->result() as $row) {
			return $row->sharetotal;
		}
	}

	public function getAmount($type)
	{
		$this->db->select('*');
		$this->db->from('sharetypes');
		$this->db->where('Id', $type);
		$query = $this->db->get();
		foreach ($query->result() as $row) {
			return $row->seriesPrice;
		}
	}

	public function getTotalSharesSubscriptionAmount($type)
	{
		$this->db->select('*');
		$this->db->from('sharetypes');
		$this->db->where('Id', $type);
		$query = $this->db->get();
		foreach ($query->result() as $row) {
			return $row->shareAmountExpected;
		}
	}

	public function getSold($type)
	{
		$this->db->select('*');
		$this->db->from('sharetypes');
		$this->db->where('Id', $type);
		$query = $this->db->get();
		foreach ($query->result() as $row) {
			return $row->sharesold;
		}
	}

	public function getDesc($type)
	{
		$this->db->select('*');
		$this->db->from('sharetypes');
		$this->db->where('Id', $type);
		$query = $this->db->get();
		foreach ($query->result() as $row) {
			return $row->Description;
		}
	}

	public function getName($type)
	{
		$this->db->select('*');
		$this->db->from('sharetypes');
		$this->db->where('Id', $type);
		$query = $this->db->get();
		foreach ($query->result() as $row) {
			return $row->type;
		}
	}

	function ReceiptNumGen($length = 5)
	{
		$buycode = substr(str_shuffle("0123456789"), 0, $length);
		$buycode1 = "CBB/" . $buycode . "/" . date("Y");

		return $buycode1;
	}

	public function getShares($idnumber, $limit, $offset, $sort_by, $sort_order)
	{

		$sort_order = ($sort_order == 'desc') ? 'desc' : 'asc';
		$sort_columns = array('buyId', 'shareholder', 'sharetype', 'shareNumber', 'expectedAmount', 'Receiptnumber',
			'datebought');
		$sort_by = (in_array($sort_by, $sort_columns)) ? $sort_by : 'buyId';

		$q = $this->db->select('*')->from('shareholders')->join('sharesBought as sG', 'sG.shareholder = shareholders.Id')
			->where('idnumber', $idnumber)->limit($limit, $offset)->order_by($sort_by, $sort_order);
		$query = $q->get()->result();

		$q1 = $this->db->select('COUNT(*) as count', false)->from('sharesBought')->where('idnumber', $idnumber);
		$tmp = $q1->get()->result();

		$result['rows'] = $query;
		$result['row_count'] = $tmp[0]->count;

		return $result;
	}

	function get_shareType($type)
	{
		$this->db->select('*');
		$this->db->from('sharetypes');
		$this->db->where(array('Id' => $type));
		$query = $this->db->get();
		foreach ($query->result() as $row) {
			return $row->type;
		}
	}

	function getshareTotal($buyId)
	{
		$this->db->select('*');
		$this->db->from('sharetypes');
		$this->db->join('sharesBought', 'sharesBought.sharetype = sharetypes.Id');
		$this->db->where(array('buyId' => $buyId));
		$query = $this->db->get();
		foreach ($query->result() as $row) {
			return $row->sharetotal;
		}
	}

	function getshareTotalSold($buyId)
	{
		$this->db->select('*');
		$this->db->from('sharetypes');
		$this->db->join('sharesBought', 'sharesBought.sharetype = sharetypes.Id');
		$this->db->where(array('buyId' => $buyId));
		$query = $this->db->get();
		foreach ($query->result() as $row) {
			return $row->sharesold;
		}
	}

	function getsharesBought($buyId)
	{
		$this->db->select('*');
		$this->db->from('sharesBought');
		$this->db->where(array('buyId' => $buyId));
		$query = $this->db->get();
		foreach ($query->result() as $row) {
			return $row->shareNumber;
		}
	}

	function getsharetypeId($buyId)
	{
		$this->db->select('*');
		$this->db->from('sharetypes');
		$this->db->join('sharesBought', 'sharesBought.sharetype = sharetypes.Id');
		$this->db->where(array('buyId' => $buyId));
		$query = $this->db->get();
		foreach ($query->result() as $row) {
			return $row->Id;
		}
	}

	function getsharetypePrice($buyId)
	{
		$this->db->select('*');
		$this->db->from('sharetypes');
		$this->db->join('sharesBought', 'sharesBought.sharetype = sharetypes.Id');
		$this->db->where(array('buyId' => $buyId));
		$query = $this->db->get();
		foreach ($query->result() as $row) {
			return $row->seriesPrice;
		}
	}

	function getsharetypePriceReceipt($receipt)
	{
		$this->db->select('*');
		$this->db->from('sharetypes');
		$this->db->join('sharesBought', 'sharesBought.sharetype = sharetypes.Id');
		$this->db->where(array('Receiptnumber' => $receipt));
		$query = $this->db->get();
		foreach ($query->result() as $row) {
			return $row->seriesPrice;
		}
	}

	function getSum()
	{
		$result = mysql_query('SELECT SUM(sharetotal) AS value_sum FROM sharetypes');
		$row = mysql_fetch_assoc($result);
		$sum = $row['value_sum'];

		return $sum;
	}

	function getSoldSum()
	{
		$result = mysql_query('SELECT SUM(sharesold) AS value_sum FROM sharetypes');
		$row = mysql_fetch_assoc($result);
		$soldsum = $row['value_sum'];

		return $soldsum;
	}

	function getPaidSum()
	{
		$result = mysql_query('SELECT SUM(amountreceived) AS value_sum FROM sharetypes');
		$row = mysql_fetch_assoc($result);
		$paidsum = $row['value_sum'];

		return $paidsum;
	}

	function getAmountExpected()
	{
		$result = mysql_query('SELECT SUM(amountexpected) AS value_sum FROM sharetypes');
		$row = mysql_fetch_assoc($result);
		$expected = $row['value_sum'];

		return $expected;
	}

	function getAmountSubExpected()
	{
		$result = mysql_query('SELECT SUM(sharetypes.amountexpected) AS value_sum FROM sharetypes');
		$row = mysql_fetch_assoc($result);
		$subExpected = $row['value_sum'];

		return $subExpected;
	}

	function totalPrice()
	{
		$result = mysql_query('SELECT SUM(seriesPrice) AS value_sum FROM sharetypes WHERE sharesold != 0');
		$row = mysql_fetch_assoc($result);
		$price = $row['value_sum'];

		return $price;
	}

	function totalReg()
	{
		$result = mysql_query('SELECT SUM(amount) AS value_sum FROM regpay');
		$row = mysql_fetch_assoc($result);
		$totalreg = $row['value_sum'];

		return $totalreg;
	}

	function getShareholders()
	{
		$q1 = $this->db->select('COUNT(*) as count', false)->from('shareholders');
		$tmp = $q1->get()->result();
		$result['row_count'] = $tmp[0]->count;

		return $result;
	}

	function getAgents()
	{
		$q1 = $this->db->select('COUNT(*) as count', false)->from('agents');
		$tmp = $q1->get()->result();
		$agents['row_count'] = $tmp[0]->count;

		return $agents;
	}

	function getShareTypesCount()
	{
		$q1 = $this->db->select('COUNT(*) as count', false)->from('sharetypes');
		$tmp = $q1->get()->result();
		$agents['row_count'] = $tmp[0]->count;

		return $agents;
	}
}
