<!-- /. NAV SIDE  -->
<div id="page-wrapper">
	<div id="page-inner">
		<!--BEGIN TITLE & BREADCRUMB PAGE-->
		<div id="title-breadcrumb-option-demo" class="page-title-breadcrumb">
			<div class="page-header pull-left">
				<div class="page-title">
					Payment Management
				</div>
			</div>
			<ol class="breadcrumb page-breadcrumb pull-right">
				<li><i class="fa fa-home"></i>&nbsp;<a href="<?php echo base_url() . "home" ?>">Home</a>&nbsp;&nbsp;
				</li>
				<li><i class="fa fa-money"></i><a href='<?php echo base_url() . 'payment/paymentHome' ?>'>&nbsp;&nbsp;Payment
						Management</a></li>
				<li class="active"><i class="fa fa-edit"></i>&nbsp;&nbsp;View Registration Payment</li>
			</ol>
			<div class="clearfix">
			</div>
		</div>
		<!--END TITLE & BREADCRUMB PAGE-->
		<div class="row no-print">
			<div class="col-md-12">
				<div class="alert alert-info">
					<i class="fa fa-info-circle"></i>
					<strong>Heads up!</strong>
					This helps check the share holders who have paid for registration!
				</div>
				<br/>
				<?php if (strlen($success) > 0) {
					?>
					<div class="alert alert-success" id="success"><i class="fa fa-check"></i>&nbsp;<?php echo $success
							. ''; ?>
					</div>
					<?php
				}
				?>
				<?php if (strlen($error) > 0) {
					?>
					<div class="alert alert-danger" id="error"><i class="fa fa-ban"></i>&nbsp;<?php echo $error . ''; ?>
					</div>
					<?php
				}
				?>
			</div>
		</div>
		<!-- /. ROW  -->
		<div class="row">
			<div class="col-md-12">
				<!-- Advanced Tables -->
				<?php if (($this->session->userdata('role') == "Admin") OR ($this->session->userdata('role') == "Finance")
				OR ($this->session->userdata('role') == "Admin2")) { ?>
				<div class="panel panel-pink">
					<?php } ?>
					<?php if (($this->session->userdata('role') == "Agent")) { ?>
					<div class="panel panel-green">
						<?php } ?>
						<div class="panel-heading">
							<div class="row">
								<div class="col-sm-6">
									<h3>Share Holder Search</h3>
								</div>
							</div>
						</div>
						<div class="panel-body">
							<?php $this->load->helper('form'); ?>
							<?php echo form_open('payment/viewSearch'); ?>
							<div class="form-body pal">
								<div class="row">
									<div class="col-md-3">
										<div class="form-group">
											<label>Write the Share holder's Id Number / KRA pin to search</label>
										</div>
									</div>
									<div class="col-md-7">
										<div class="form-group">
											<?php echo form_input(array("class" => "form-control",
												"placeholder" => "Enter Id Number / KRA Pin Number",
												"name" => "search")) ?>
										</div>
									</div>
									<div class="col-md-2">
										<div class="form-group">
											<?php echo form_submit('save', 'Search',
												'class="btn btn-success pull-left margin-right"'); ?>

										</div>
									</div>
								</div>
							</div>
							</form>
							<!-- /.row (nested) -->
						</div>
					</div>
					<!-- /.panel-body -->
				</div>
				<!-- /.panel -->
			</div>
			<!-- /.col-lg-12 -->
		</div>
		<br/>
		<!-- /. ROW  -->

		<div class="row">
			<div class="col-md-12">
				<!-- Advanced Tables -->
				<?php if ($view_data != null) { ?>
				<?php if (($this->session->userdata('role') == "Admin") OR ($this->session->userdata('role') == "Finance")
				OR ($this->session->userdata('role') == "Admin2")) { ?>
				<div class="panel panel-pink">
					<?php } ?>
					<?php if (($this->session->userdata('role') == "Agent")) { ?>
					<div class="panel panel-green">
						<?php } ?>
						<div class="panel-heading">
							<div class="row">
								<div class="col-sm-6">
									<h3>View Registration Payment</h3>
								</div>
							</div>
						</div>
						<div class="panel-body">
							<div class="row">
								<div class="col-sm-6">
								</div>
								<div class="col-sm-6">
									<p class=" records">Found&nbsp;<?php echo $rownumber; ?>&nbsp;Entries</p>
								</div>
							</div>
							<div class="table-responsive">
								<table class="table table-striped table-hover table-condensed" id="stafftable">
									<thead>
									<tr>
										<?php foreach ($fields as $field_name => $field_Column): ?>
											<th><?php echo $field_Column ?>
											</th>
										<?php endforeach; ?>
									</tr>
									</thead>
									<tbody>
									<?php foreach ($view_data as $key => $data): ?>
										<tr>
											<?php foreach ($fields as $field_name => $field_Column): ?>
												<td>
													<?php echo $data->$field_name ?>
												</td>
											<?php endforeach; ?>
											<td>
												<a class="btn btn-success btn-sm"
												   href="<?php echo base_url() . "payment/regReceipt/" . $data->payId ?>"
												   data-toggle="tooltip"
												   data-placement="top"
												   title="Download"><i class="fa fa-download"></i></a>
											</td>
											<td>
												<a class="btn btn-info btn-sm"
												   href="<?php echo base_url() . "payment/modifyReg/" . $data->payId ?>"
												   data-toggle="tooltip"
												   data-placement="top"
												   title="Edit"><i class="fa fa-edit"></i></a>
											</td>
											<td>
												<a class="btn btn-danger btn-sm"
												   href="<?php echo base_url() . "payment/removeReg/" . $data->payId ?>"
												   onclick="return confirm('Are you sure you want to delete');"
												   data-toggle="tooltip"
												   data-placement="top"
												   title="Delete"><i
														class="fa fa-trash-o"></i></a>
											</td>
										</tr>
									<?php endforeach; ?>
									</tbody>
								</table>
							</div>
							<div class="row">
								<div class="col-sm-6">
								</div>
								<div class="col-sm-6">
									<?php if (strlen($pagination)) {
										;
									}
									{ ?>
										<p class=" records">Pages&nbsp;<?php echo $pagination; ?>&nbsp;</p>
									<?php } ?>
								</div>
							</div>
							<!-- /.row (nested) -->
						</div>
						<!-- /.panel-body -->
					</div>
					<?php } ?>
					<!-- /.panel -->
				</div>
				<!-- /.col-lg-12 -->
			</div>
