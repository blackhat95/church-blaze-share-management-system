<!-- /. NAV SIDE  -->
<div id="page-wrapper">
	<div id="page-inner">
		<!--BEGIN TITLE & BREADCRUMB PAGE-->
		<div id="title-breadcrumb-option-demo" class="page-title-breadcrumb no-print">
			<div class="page-header pull-left">
				<div class="page-title">
					General Reporting
				</div>
			</div>
			<ol class="breadcrumb page-breadcrumb pull-right">
				<li><i class="fa fa-home"></i>&nbsp;<a href="<?php echo base_url() . "home" ?>">Home</a>&nbsp;&nbsp;
				</li>
				<li><i class="fa fa-bar-chart-o"></i>&nbsp;&nbsp;<a href="<?php echo base_url() . "reports/reportsHome" ?>">General
						Reporting</a></li>
				<li><i class="fa fa-print"></i>&nbsp;&nbsp;Shareholders</li>
			</ol>
			<div class="clearfix">
			</div>
		</div>
		<!--END TITLE & BREADCRUMB PAGE-->
		<div class="row no-print">
			<div class="col-md-12">
				<div class="alert alert-info">
					<i class="fa fa-info-circle"></i>
					<strong>Heads up!</strong>
					This helps in the generation of a share register for the shareholders!
				</div>
				<br/>
				<?php if (strlen($success) > 0) {
					?>
					<div class="alert alert-success" id="success"><i class="fa fa-check"></i>&nbsp;<?php echo $success
							. ''; ?>
					</div>
					<?php
				}
				?>
				<?php if (strlen($error) > 0) {
					?>
					<div class="alert alert-danger" id="error"><i class="fa fa-ban"></i>&nbsp;<?php echo $error . ''; ?>
					</div>
					<?php
				}
				?>
			</div>
		</div>
		<div class="row no-print">
			<div class="col-md-12">
				<!-- Advanced Tables -->
				<?php if (($this->session->userdata('role') == "Admin") OR ($this->session->userdata('role') == "Finance")
				OR ($this->session->userdata('role') == "Admin2")) { ?>
				<div class="panel panel-pink">
					<?php } ?>
					<?php if (($this->session->userdata('role') == "Agent")) { ?>
					<div class="panel panel-green">
						<?php } ?>
						<div class="panel-heading">
							<div class="row">
								<div class="col-sm-6">
									<h3>Register Dates Definition</h3>
								</div>
							</div>
						</div>
						<div class="panel-body">
							<?php $this->load->helper('form'); ?>
							<?php echo form_open('reports/share_register_download'); ?>
							<div class="form-body pal">
								<div class="row">
									<div class="col-md-12">
										<div class="form-group">
											<label>Choose dates that you want the register generated from</label>
										</div>
									</div>
								</div>
								<div class="row">
									<div class="col-md-5">
										<div class="form-group">
											<label>First Date</label>
											<?php echo form_input(array("class" => "form-control datepicker",
												"placeholder" => "Date. Format 1900-12-01", "name" => "dateone",
												"id" => "dateone", "required" => "true", "type" => "date")) ?>
										</div>
									</div>
									<div class="col-md-5">
										<div class="form-group">
											<label>Second Date</label>
											<?php echo form_input(array("class" => "form-control datepicker",
												"placeholder" => "Date. Format 1900-12-01", "name" => "datetwo",
												"id" => "datetwo", "required" => "true", "type" => "date")) ?>
										</div>
									</div>
									<div class="col-md-2">
										<div class="form-group">
											<br/>
											<?php echo form_submit('save', 'Get Register', 'class="btn btn-success"'); ?>

										</div>
									</div>
								</div>
							</div>
							</form>
							<!-- /.row (nested) -->
						</div>
					</div>
					<!-- /.panel-body -->
				</div>
				<!-- /.panel -->
			</div>
			<!-- /.col-lg-12 -->
		</div>
		<br class="no-print"/>
		<!-- /. ROW  -->
		<?php if ($view_data != null) { ?>
			<div class="row">
				<div class="col-md-12">
					<div class="table-responsive">
						<table class="table table-responsive table-striped" id="stafftable">
							<thead>
							<tr>
								<?php foreach ($fields as $field_name => $field_Column): ?>
									<th><?php echo $field_Column ?>
									</th>
								<?php endforeach; ?>
							</tr>
							</thead>
							<tbody>
							<?php foreach ($view_data as $key => $data): ?>
								<tr>
									<?php foreach ($fields as $field_name => $field_Column): ?>
										<td>
											<?php echo $data->$field_name ?>
										</td>
									<?php endforeach; ?>
								</tr>
							<?php endforeach; ?>
							</tbody>
						</table>
					</div>
					<!-- /.panel -->
				</div>
				<!-- /.col-lg-12 -->
			</div>
		<?php } ?>
