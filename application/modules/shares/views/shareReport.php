<!-- /. NAV SIDE  -->
<div id="page-wrapper">
	<div id="page-inner">
		<!--BEGIN TITLE & BREADCRUMB PAGE-->
		<div id="title-breadcrumb-option-demo" class="page-title-breadcrumb no-print">
			<div class="page-header pull-left">
				<div class="page-title">
					Share Buying Receipt
				</div>
			</div>
			<ol class="breadcrumb page-breadcrumb pull-right no-print">
				<li><i class="fa fa-home"></i>&nbsp;<a href="<?php echo base_url() . "home" ?>">Home</a>&nbsp;&nbsp;
				</li>
				<li><i class="fa fa-stack-exchange"></i><a href="<?php echo base_url() . 'shares/dashboard'
					?>">&nbsp;&nbsp;Shares Management</a></li>
				<li class="active"><i class="fa fa-print"></i>&nbsp;&nbsp;Shares Report</li>
			</ol>
			<div class="clearfix">
			</div>
		</div>
		<!--END TITLE & BREADCRUMB PAGE-->
		<div class="row">
			<div class="col-md-12">
				<!-- Advanced Tables -->
				<?php if (($this->session->userdata('role') == "Admin") OR ($this->session->userdata('role') == "Finance")
				OR ($this->session->userdata('role') == "Admin2")) { ?>
				<div class="panel panel-pink">
					<?php } ?>
					<?php if (($this->session->userdata('role') == "Agent")) { ?>
					<div class="panel panel-green">
						<?php } ?>
						<div class="panel-heading no-print">
							<div class="row">
								<div class="col-sm-6">
									<h3>Share Receipt</h3>
								</div>
							</div>
						</div>
						<div class="panel-body">
							<?php $this->load->helper('form'); ?>
							<?php echo form_open('shares/save'); ?>
							<div class="form-body pal">
								<div class="row">
									<h2 class="center receiptheader"><?php echo $view_data1['Name'] ?></h2>

									<h3 class="center receiptheader"><?php echo $view_data1['addressone'] ?>
										,&nbsp;<?php echo $view_data1['addresstwo'] ?></h3>

									<h3 class="center receiptheader">Phonenumber
										:-&nbsp;<?php echo $view_data1['phonenumberone'] ?>
										,&nbsp;<?php echo $view_data1['phonenumbertwo'] ?></h3>
								</div>
								<hr/>
								<div class="row">
									<h3 class="center heading"><b>Shares Subscription Receipt</b></h3>

									<div class="col-md-6">
										<h4 class="pull-left"><b>CB Form Number:-</b>&nbsp;<b
												class="text-red"><?php echo $view_data['physicalFormNumber'] ?></b></h4>
									</div>
									<div class="col-md-6">
										<h4 class="pull-right"><b>Receipt Number:-</b>&nbsp;<b
												class="text-red"><?php echo $view_data['Receiptnumber'] ?></b></h4>
									</div>
								</div>
								<div class="row">
									<table class="table">
										<tr>
											<td><p class="pull-left">Shareholder: &nbsp;<b
														class="heading"><?php echo $view_data['Name'] ?></b>&nbsp;
												</p></td>
											<td><p class="center">Id Number/KRA Pin:&nbsp;<b
														class="heading"><?php echo $view_data['idnumber'] ?></b>
												</p></td>
											<td><p class="pull-right">Date Bought: &nbsp;<b
														class="heading"><?php echo $view_data['datebought'] ?></b>&nbsp;
												</p></td>
										</tr>
									</table>
									<div class="table-responsive">
										<table class="table table-condensed" id="stafftable">
											<thead>
											<tr>
												<th>Share Type</th>
												<th>Subscribed Shares</th>
												<th>Price Per Share</th>
												<th>Total Price</th>
											</tr>
											</thead>
											<tbody>
											<tr>
												<td><?php echo $view_data['shareTypeName'] ?></td>
												<td><?php echo $view_data['shareNumber'] ?></td>
												<td><?php echo $price ?></td>
												<td><?php echo $view_data['expectedAmount'] ?></td>
											</tr>
											</tbody>
										</table>
									</div>
									<div class="table-responsive">
										<table class="table table-condensed" id="stafftable">
											<tbody>
											<tr>
												<td><p class="pull-left">Sold By:<b class="heading">
															&nbsp;<?php echo $view_data['AddedBy'] ?></b>&nbsp;</p></td>
												<td><p class="pull-right"><?php if (($this->session->userdata('role')
																== 'Admin') OR ($this->session->userdata('role')
																== 'Admin2') OR ($this->session->userdata('role')
																== 'Finance')
														) { ?>Worknumber: <b class="heading">
															&nbsp;<?php echo $this->session->userdata('idnumber') ?></b>&nbsp;<?php } ?><?php if ($this->session->userdata('role')
														== 'Agent') { ?>Agent Number :<b class="heading">
															&nbsp;<?php echo $this->session->userdata('idnumber') ?></b>&nbsp;<?php } ?></b>
														&nbsp;</p></td>
												<td><!--<p class="pull-right">Phonenumber: <b class="heading">
															&nbsp;<?php /*echo $this->session->userdata('phone') */ ?></b>&nbsp;
													</p>-->
												</td>
											</tr>
											</tbody>
										</table>
									</div>
								</div>
								<div class="row ">
									<div class="col-md-12">
										<table class="table table-condensed">
											<tr>
												<td class="pull-left col-md-6"><p><b>Signature:....................................................</b>
													</p></td>
												<td class="pull-right col-md-6"><p><b>Date:.....................................................</b>
													</p></td>
											</tr>
										</table>
										<table class="table table-condensed">
											<tr>
												<td class="pull-left col-md-6"><p><b>Signature:....................................................</b>
													</p></td>
												<td class="pull-right col-md-6"><p><b>Date:.....................................................</b>
													</p></td>
											</tr>
										</table>
									</div>
								</div>
								<div class="row ">
									<div class="col-md-6">
										<p><b>Terms & Conditions</b></p>
										<ol>
											<li>Applicant must remain with a copy of the application form, pay-in-slip and
												original cash receipt
											</li>
										</ol>
									</div>
								</div>
								<div class="row">
									<div class="col-md-6">

										<p><b>NB:</b>&nbsp;All cash must be deposited in the following back account</p>
										<ul>
											<li><b><?php echo $view_data1['bankname'] ?></b></li>
											<li><b><?php echo $view_data1['bankbranch'] ?></b></li>
											<li><b>A/C Name: <?php echo $view_data1['accountname'] ?></b></li>
											<li><b>A/C NO.<?php echo $view_data1['accountnumber'] ?></b></li>
											<li><b>Paybill Number: <?php echo $view_data1['paybillnumber'] ?></b></li>
										</ul>
									</div>
								</div>
								<hr/>
								<hr/>
								<div class="row no-print">
									<div class="col-md-4">
										<div class="form-group">
										</div>
									</div>
									<div class="col-md-4">
										<div class="form-group">
										</div>
									</div>
									<div class="col-md-4">
										<div class="form-group">
											<a class="btn btn-bitbucket pull-right" href="javascript:window.print();"><i
													class="fa fa-print"></i>&nbsp;Print
											</a>
										</div>
									</div>
								</div>
							</div>
							</form>
							<!-- /.row (nested) -->
						</div>
						<!-- /.panel-body -->
					</div>
					<!-- /.panel -->
				</div>
				<!-- /.col-lg-12 -->
			</div>
