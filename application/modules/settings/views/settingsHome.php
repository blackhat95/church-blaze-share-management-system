<!-- /. NAV SIDE  -->
<div id="page-wrapper">
	<div id="page-inner">
		<!--BEGIN TITLE & BREADCRUMB PAGE-->
		<div id="title-breadcrumb-option-demo" class="page-title-breadcrumb">
			<div class="page-header pull-left">
				<div class="page-title">
					System Settings
				</div>
			</div>
			<ol class="breadcrumb page-breadcrumb pull-right">
				<li><i class="fa fa-home"></i>&nbsp;<a href="<?php echo base_url() . "home" ?>">Home</a>&nbsp;&nbsp;
				</li>
				<li><i class="fa fa-gears"></i>&nbsp;&nbsp;System Setting</li>
			</ol>
			<div class="clearfix">
			</div>
		</div>
		<!--END TITLE & BREADCRUMB PAGE-->
		<div class="row">
			<div class="alert alert-info">
				<i class="fa fa-info-circle"></i>
				<strong>Heads up!</strong>
				All system settings are to be handled here in the system!
			</div>
			<hr/>
			<ul class="nav nav-tabs nav-justified">
				<li class="active"><a data-toggle="tab" href="#sectionA"><h5><i class="fa fa-shopping-cart"></i>&nbsp;Share
							Series</h5></a></li>
				<li><a data-toggle="tab" href="#sectionB"><h5><i class="fa fa-info-circle"></i>&nbsp;System Info</h5></a>
				</li>
			</ul>
			<div class="tab-content">
				<div id="sectionA" class="tab-pane fade in active">
					<hr/>
					<div class="row">
						<div class="col-md-3 col-sm-12 col-xs-12">
							<div class="panel panel-primary text-center no-boder bg-color-green">
								<div class="panel-body">

									<a href="<?php echo base_url() . "settings/viewShareTypes" ?>"><i
											class="fa fa-gear fa-5x bg-color-green"></i></a>

									<h3></h3>
								</div>
								<div class="panel-footer back-footer-green">
									<a class="white" href="<?php echo base_url() . "settings/viewShareTypes" ?>">View
										Share
										Series</a>
								</div>
							</div>
						</div>
						<div class="col-md-3 col-sm-12 col-xs-12">
							<div class="panel panel-primary text-center no-boder bg-color-brown">
								<div class="panel-body">
									<a href="<?php echo base_url() . "settings/addShareTypes" ?>"><i
											class="fa fa-shopping-cart fa-5x bg-color-brown"></i></a>

									<h3></h3>
								</div>
								<div class="panel-footer back-footer-brown">
									<a class="white" href="<?php echo base_url() . "settings/addShareTypes" ?>">Add
										Share
										Series</a>
								</div>
							</div>
						</div>
						<div class="col-md-3 col-sm-12 col-xs-12">
							<div class="panel panel-primary text-center no-boder bg-color-TribalTriumph">
								<div class="panel-body">

									<a href="<?php echo base_url() . 'settings/viewRegpay' ?>"><i
											class="fa fa-money fa-5x bg-color-TribalTriumph"></i></a>

									<h3></h3>
								</div>
								<div class="panel-footer back-footer-TribalTriumph">
									<a class="white" href="<?php echo base_url() . 'settings/viewRegpay' ?>">View
										Registration Amount</a>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div id="sectionB" class="tab-pane fade">
					<hr/>
					<?php
					if (($this->session->userdata('role') === 'Admin')) {
						?>
						<div class="row">
							<div class="col-md-3 col-sm-12 col-xs-12">
								<div class="panel panel-primary text-center no-boder bg-color-blue">
									<div class="panel-body">
										<a href="<?php echo base_url() . 'settings/addSystemInfo' ?>"><i
												class="fa fa-info-circle fa-5x bg-color-blue"></i></a>

										<h3></h3>
									</div>
									<div class="panel-footer back-footer-blue">
										<a class="white" href="<?php echo base_url() . "settings/addSystemInfo" ?>">Add
											System Information</a>
									</div>
								</div>
							</div>
						</div>
					<?php } ?>
				</div>
			</div>

