<!-- /. NAV SIDE  -->
<div id="page-wrapper">
	<div id="page-inner">
		<!--BEGIN TITLE & BREADCRUMB PAGE-->
		<div id="title-breadcrumb-option-demo" class="page-title-breadcrumb">
			<div class="page-header pull-left">
				<div class="page-title">
					Main Panel
				</div>
			</div>
			<ol class="breadcrumb page-breadcrumb pull-right">
				<li><i class="fa fa-home"></i>&nbsp;<a href="<?php echo base_url() . "home" ?>">Home</a>&nbsp;&nbsp;
				</li>
				<li><i class="fa fa-dashboard"></i>&nbsp;&nbsp;Main Panel</li>
			</ol>
			<div class="clearfix">
			</div>
		</div>
		<!--END TITLE & BREADCRUMB PAGE-->

		<div class="row">
			<div class="row">
				<div class="col-md-3 col-sm-12 col-xs-12">
					<div class="panel panel-primary text-center no-boder">
						<div class="panel-body">
							<img alt='user' src='<?php echo base_url(); ?>assets/img/images/dashboard.png' width='150px'
							     height='150px'/>

							<h3>Dashboard</h3>

							<p>This Contains the different summaries from the system to give the users a heads up on the
								system</p>
							<a class="btn button-dashboard" href="<?php echo base_url() . "redirect/dashboard"
							?>"><i class="fa fa-dashboard"></i>&nbsp;Dashboard</a>
						</div>
					</div>
				</div>
				<div class="col-md-3 col-sm-12 col-xs-12">
					<div class="panel panel-primary text-center no-boder ">
						<div class="panel-body">
							<img alt='user' src='<?php echo base_url(); ?>assets/img/images/shareholders.png' width='150px'
							     height='150px'/>

							<h3>Shareholders</h3>

							<p>This contains functionality for the addition, editing, viewing &amp; of both the Individual
								&amp; Company shareholders. It also contains the functionality to add the Next of Kins for
								the Individual shareholders</p>
							<a class="btn button-dashboard" href='<?php echo base_url() . 'shareholders/shareholderAdd' ?>'><i class="fa fa-users"></i>&nbsp;Shareholders</a>
						</div>
					</div>
				</div>
				<div class="col-md-3 col-sm-12 col-xs-12">
					<div class="panel panel-primary text-center no-boder ">
						<div class="panel-body">
							<img alt='user' src='<?php echo base_url(); ?>assets/img/images/subscription.png' width='150px'
							     height='150px'/>

							<h3>Share Subscription</h3>

							<p>This contains functionality to help in the subscription of shares by sharelhoders.</p>
							<a class="btn button-dashboard" href='<?php echo base_url() . 'shares/dashboard' ?>'><i class="fa fa-stack-exchange"></i>&nbsp;Share
								Subscription</a>
						</div>
					</div>
				</div>
				<div class="col-md-3 col-sm-12 col-xs-12">
					<div class="panel panel-primary text-center no-boder">
						<div class="panel-body">
							<img alt='user' src='<?php echo base_url(); ?>assets/img/images/finance.png' width='150px'
							     height='150px'/>

							<h3>Payments</h3>

							<p>This contains functionality for the addition, editing, viewing &amp; of both the Registration
								&amp; Shares payments</p>
							<a class="btn button-dashboard" href='<?php echo base_url() . 'payment/paymentHome' ?>'><i
										class="fa fa-money"></i>&nbsp;Payments</a>
						</div>
					</div>
				</div>
			</div>
			<div class="row">
				<?php if ($this->session->userdata('role') == 'Admin' || $this->session->userdata('role') == 'Admin2') {?>
				<div class="col-md-3 col-sm-12 col-xs-12">
					<div class="panel panel-primary text-center no-boder">
						<div class="panel-body">
							<img alt='user' src='<?php echo base_url(); ?>assets/img/images/agent.jpg' width='150px'
							     height='150px'/>

							<h3>Agents</h3>

							<p>This contains functionality for the addition, editing &amp; viewing of the agents</p>
							<a class="btn button-dashboard" href='<?php echo base_url() . 'agents/dashboard'
							?>'><i class="fa fa-user-md"></i>&nbsp;Agents</a>
						</div>
					</div>
				</div>
				<?php } ?>
				<div class="col-md-3 col-sm-12 col-xs-12">
					<div class="panel panel-primary text-center no-boder ">
						<div class="panel-body">
							<img alt='user' src='<?php echo base_url(); ?>assets/img/images/reports.png' width='150px'
							     height='150px'/>

							<h3>Reports</h3>

							<p>This contains functionality for the generate the genral reports that might be needed from the
								system </p>
							<a class="btn button-dashboard" href='<?php echo base_url() . 'reports/reportsHome' ?>'><i
										class="fa fa-bar-chart-o"></i>&nbsp;Reports</a>
						</div>
					</div>
				</div>
				<?php if ($this->session->userdata('role') == 'Admin') { ?>
					<div class="col-md-3 col-sm-12 col-xs-12">
						<div class="panel panel-primary text-center no-boder">
							<div class="panel-body">
								<img alt='user' src='<?php echo base_url(); ?>assets/img/images/users.png' width='150px'
								     height='150px'/>

								<h3>Staff&nbsp;&amp;&nbsp;User Operation</h3>

								<p>This contains functionality for the addition, editing, viewing &amp; Retiring of the staff
									&amp; Users
								</p>
								<a class="btn button-dashboard" href='<?php echo base_url() . 'users/dashboard'
								?>'><i class="fa fa-user-secret"></i>&nbsp;Staff&nbsp;&amp;&nbsp;User Operation</a>
							</div>
						</div>
					</div>
					<div class="col-md-3 col-sm-12 col-xs-12">
						<div class="panel panel-primary text-center no-boder ">
							<div class="panel-body">
								<img alt='user' src='<?php echo base_url(); ?>assets/img/images/settings.png' width='150px'
								     height='150px'/>

								<h3>Settings</h3>

								<p>This contains functionality for the necessary system settings.</p>
								<a class="btn button-dashboard" href='<?php echo base_url() . 'settings/settingsHome' ?>'><i
											class="fa fa-gears"></i>&nbsp;Settings</a>
							</div>
						</div>
					</div>
				<?php } ?>
			</div>
		</div>
		<!-- /. ROW  -->
