<!-- /. NAV SIDE  -->
<div id="page-wrapper">
	<div id="page-inner">
		<!--BEGIN TITLE & BREADCRUMB PAGE-->
		<div id="title-breadcrumb-option-demo" class="page-title-breadcrumb">
			<div class="page-header pull-left">
				<div class="page-title">
					Staff &amp; Users Management
				</div>
			</div>
			<ol class="breadcrumb page-breadcrumb pull-right">
				<li><i class="fa fa-home"></i>&nbsp;<a href="<?php echo base_url() . "home" ?>">Home</a>&nbsp;&nbsp;
				</li>
				<li><i class="fa fa-user-secret"></i>&nbsp;&nbsp;Staff &amp; Users Management</li>
			</ol>
			<div class="clearfix">
			</div>
		</div>
		<!--END TITLE & BREADCRUMB PAGE-->
		<div class="row">
			<div class="alert alert-info">
				<i class="fa fa-info-circle"></i>
				<strong>Heads up!</strong>
				This contains functionality for the addition, editing, viewing &amp; Retiring of the staff
				&amp; Users
			</div>
			<hr/>
			<ul class="nav nav-tabs nav-justified">
				<li class="active"><a data-toggle="tab" href="#staff"><h5><i class="fa fa-user-secret"></i>&nbsp;
							Staff Operation</h5></a></li>
				<li><a data-toggle="tab" href="#users"><h5><i class="fa fa-user-secret"></i>&nbsp;
							Users Operation</h5></a></li>
			</ul>
			<div class="tab-content">
				<div id="staff" class="tab-pane fade in active">
					<hr/>
					<div class="col-md-6 col-sm-12 col-xs-12">
						<div class="panel panel-primary text-center no-boder bg-color-royalpurple">
							<div class="panel-body">

								<a href='<?php echo base_url() . 'staff/addStaff' ?>'><i
										class="fa fa-user-secret fa-10x bg-color-royalpurple"></i></a>

								<h3></h3>
							</div>
							<div class="panel-footer back-footer-royalpurple">
								<a class="white" href='<?php echo base_url() . 'staff/addStaff' ?>'>Add Staff</a>
							</div>
						</div>
					</div>
					<?php
					if (($this->session->userdata('role') === "Admin") OR ($this->session->userdata('role') === "Admin2")
						OR ($this->session->userdata('role') === "Finance")
					) {
						?>
						<div class="col-md-6 col-sm-12 col-xs-12">
							<div class="panel panel-primary text-center no-boder bg-color-blue">
								<div class="panel-body">
									<a href='<?php echo base_url() . 'staff/viewStaff' ?>'><i
											class="fa fa-edit fa-10x bg-color-blue"></i></a>
									
									<h3></h3>
								</div>
								<div class="panel-footer back-footer-blue">
									<a class="white"
									   href='<?php echo base_url() . 'staff/viewStaff' ?>'>View
										Staff</a>
								</div>
							</div>
						</div>
					<?php } ?>
				</div>
				<div id="users" class="tab-pane fade in">
					<hr/>
					<?php
					if (($this->session->userdata('role') === "Admin") OR ($this->session->userdata('role') === "Admin2")
						OR ($this->session->userdata('role') === "Finance")
					) {
						?>
						<div class="col-md-6 col-sm-12 col-xs-12">
							<div class="panel panel-primary text-center no-boder bg-color-green">
								<div class="panel-body">
									<a href='<?php echo base_url() . 'users' ?>'><i
											class="fa fa-user-secret fa-10x bg-color-green"></i></a>

									<h3></h3>
								</div>
								<div class="panel-footer back-footer-green">
									<a class="white"
									   href='<?php echo base_url() . 'users' ?>'>User Operations</a>
								</div>
							</div>
						</div>
					<?php } ?>
				</div>
			</div>
		</div>
	</div>



