<?php

class users_model extends CI_Model {
	private $username;
	private $vacancyid;

	public function __construct() {
		$this->load->database();
	}

	public function get_emails() {
		$query = $this->db->query("SELECT cbUsersEmail FROM cbusers");

		return $query->result_array();
	}

	public function get_patients() {
		$query = $this->db->query("SELECT * FROM patient_details");

		return $query->result_array();
	}

	public function get_patient($patient_id) {
		$query = $this->db->query("SELECT * FROM patient_details WHERE patient_id = '" . $patient_id . "'");

		return $query->result_array();
	}

	public function get_userid($username) {
		$query = $this->db->query("SELECT userid FROM user WHERE user.username = '" . $username . "'");
		$query->result();
		foreach ($query->result() as $row) {
			return $row->userid;
		}
	}

	public function editpatientdetails() {

		$this->load->library('session');
		$this->load->helper('url');

		$patient_id = $this->input->post("patient_id");
		$patient_name = $this->input->post("patient_name");
		$patient_gender = $this->input->post("patient_gender");
		$patient_dob = $this->input->post("patient_dob");
		$patient_email = $this->input->post("patient_email");
		$patient_phone = $this->input->post("patient_phone");
		$patient_nationality = $this->input->post("patient_nationality");
		$patient_nextofkin = $this->input->post("patient_nextofkin");
		$patient_nextofkincontact = $this->input->post("patient_nextofkincontact");

		$sql = "INSERT INTO patient_details (patient_id , patient_name , patient_gender , patient_dob , patient_email ,
            patient_phone , patient_nationality , patient_nextofkin , patient_nextofkincontact  ) VALUES ('$patient_id', '$patient_name',
             '$patient_gender' , '$patient_dob','$patient_email','$patient_phone','$patient_nationality','$patient_nextofkin',
             '$patient_nextofkincontact')";

		$this->db->query($sql);

	}

	public function patient_edit($id_patient) {

		$this->load->library('session');
		$this->load->helper('url');

		$patient_id = $this->input->post("patient_id");
		$patient_name = $this->input->post("patient_name");
		$patient_gender = $this->input->post("patient_gender");
		$patient_dob = $this->input->post("patient_dob");
		$patient_email = $this->input->post("patient_email");
		$patient_phone = $this->input->post("patient_phone");
		$patient_nationality = $this->input->post("patient_nationality");
		$patient_nextofkin = $this->input->post("patient_nextofkin");
		$patient_nextofkincontact = $this->input->post("patient_nextofkincontact");

		$sql = "UPDATE patient_details  SET patient_id  = '$patient_id', patient_name  = '$patient_name',
              patient_gender = '$patient_gender', patient_dob = '$patient_dob' , patient_email = '$patient_email' ,
              patient_phone = '$patient_phone', patient_nationality ='$patient_nationality' , patient_nextofkin  = '$patient_nextofkin', 
              patient_nextofkincontact = '$patient_nextofkincontact' WHERE patient_id = '$id_patient'";

		$this->db->query($sql);

	}

	public function editpatienthistory($patient_id) {

		$this->load->library('session');
		$this->load->helper('url');

		$patient_id = $patient_id;

		$patient_insuarance = "";

		foreach ($this->input->post("patient_insuarance") as $item) {

			$patient_insuarance = $patient_insuarance . "," + $item;

		}
		$relevantinformation = $this->input->post("relevantinformation");

		$sql = "INSERT INTO patient_history (patient_id , patient_insuarance , relevantinformation )

            VALUES ('$patient_id', '$patient_insuarance','$relevantinformation')";

		$this->db->query($sql);

	}

	public function edit_history($patient_id) {

		$this->load->library('session');
		$this->load->helper('url');
		$patient_id = $patient_id;

		$patient_insuarance = "";

		foreach ($this->input->post("patient_insuarance") as $item) {

			$patient_insuarance = $patient_insuarance . "," + $item;

		}
		$relevantinformation = $this->input->post("relevantinformation");

		$sql = "UPDATE patient_history SET patient_insuarance = '$patient_insuarance' ,
            relevantinformation = '$relevantinformation' WHERE patient_id = '$patient_id' ";
		$this->db->query($sql);

	}

	public function edituser() {

		$this->load->library('session');
		$user = $this->session->userdata('username');

		$username = $this->input->post("username");
		$name = $this->input->post("name");

		$sql = "UPDATE user  SET name = '$name' ,
            username = '$username' WHERE username = '$user' ";
		$this->db->query($sql);

	}

	public function changeuseravatar() {
		$upload_data = $this->upload->data();
		$ppic = $upload_data['file_name'];
		$this->load->helper('url');
		$pic = base_url("assets/img/users/" . $ppic);

		$this->load->library('session');
		$user = $this->session->userdata('username');

		$sql = "UPDATE user SET user_avatar = '" . $pic . "' WHERE user.username = '" . $user . "'";

		$this->db->query($sql);
	}

}
