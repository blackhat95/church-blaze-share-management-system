<?php if (!defined('BASEPATH')) {
	exit('No direct script access allowed');
}

class Payment_model extends CI_Model
{

	public function __construct()
	{
		parent::__construct();
	}

	public function get($receipt, $limit, $offset, $sort_by, $sort_order)
	{
		$sort_order = ($sort_order == 'desc') ? 'desc' : 'asc';
		$sort_columns =
			array('PaymentUUID', 'Type', 'Amount', 'numberofshares', 'Receiver', 'date', 'holder', 'holderName',
				'code');
		$sort_by = (in_array($sort_by, $sort_columns)) ? $sort_by : 'PaymentUUID';

		$q = $this->db->select('*')->from('sharetypes')->join('payment', 'payment.shareType = sharetypes.Id')
			->where('buyreceipt', $receipt)->limit($limit, $offset)->order_by($sort_by, $sort_order);

		$query = $q->get()->result();

		$q = $this->db->select('COUNT(*) as count', false)->from('payment')->where('buyreceipt', $receipt);
		$tmp = $q->get()->result();

		$result['rows'] = $query;
		$result['row_count'] = $tmp[0]->count;

		return $result;

	}

	public function edit($id = null)
	{
		$this->db->select()->from('payment');
		if ($id != null) {
			$this->db->where('Id', $id);
		}
		$query = $this->db->get();
		if ($id != null) {
			return $query->row_array();
		}

	}

	public function editReg($payId = null)
	{
		$this->db->select()->from('regpay');
		if ($payId != null) {
			$this->db->where('payId', $payId);
		}
		$query = $this->db->get();
		if ($payId != null) {
			return $query->row_array();
		}

	}

	public function editReg1($receipt = null)
	{
		$this->db->select()->from('regpay');
		if ($receipt != null) {
			$this->db->where('receipt', $receipt);
		}
		$query = $this->db->get();
		if ($receipt != null) {
			return $query->row_array();
		}

	}

	function getReg2()
	{
		$this->db->select('*');
		$this->db->from('regpaysetup');
		$query = $this->db->get();
		foreach ($query->result() as $row) {
			return $row->amountPay;
		}
	}

	public function edit2($receipt = null)
	{
		$this->db->select()->from('payment');
		if ($receipt != null) {
			$this->db->where('PaymentUUID', $receipt);
		}
		$query = $this->db->get();
		if ($receipt != null) {
			return $query->row_array();
		}

	}

	public function remove($id)
	{
		$this->db->where('Id', $id);
		$this->db->delete('payment');
	}

	public function removeReg($payId)
	{
		$this->db->where('payId', $payId);
		$this->db->delete('regpay');
	}

	public function add($index)
	{
		if (isset($index['payId'])) {
			$this->db->where('payId', $index['payId']);
			$this->db->update('regpay', $index);
		} else {
			$this->db->insert('regpay', $index);
		}
	}

	public function addreg($index)
	{
		if (isset($index['Id'])) {
			$this->db->where('Id', $index['Id']);
			$this->db->update('payment', $index);
		} else {
			$this->db->insert('payment', $index);
		}
	}

	public function addUser($user)
	{
		if (isset($user['id'])) {
			$this->db->where('id', $user['id']);
			$this->db->update('users', $user);
		} else {
			$this->db->insert('users', $user);
		}

	}

	function getreceipt($id)
	{
		$this->db->select('*');
		$this->db->from('payment');
		$this->db->where(array('Id' => $id));
		$query = $this->db->get();
		foreach ($query->result() as $row) {
			return $row->buyreceipt;
		}
	}

	function get_receipt($receipt)
	{
		$this->db->select('*');
		$this->db->from('sharesBought');
		$this->db->where(array('Receiptnumber' => $receipt));
		$query = $this->db->get();

		return $query->row_array();
	}

	function getSharesBought($receipt)
	{
		$this->db->select('*');
		$this->db->from('sharesBought');
		$this->db->where(array('Receiptnumber' => $receipt));
		$query = $this->db->get();
		foreach ($query->result() as $row) {
			return $row->shareNumber;
		}
	}

	function get_shareType($receipt)
	{
		$this->db->select('*');
		$this->db->from('sharetypes');
		$this->db->join('sharesBought', 'sharesBought.sharetype = sharetypes.Id');
		$this->db->where(array('Receiptnumber' => $receipt));
		$query = $this->db->get();
		foreach ($query->result() as $row) {
			return $row->type;
		}
	}

	function getShareAmount($receipt)
	{
		$this->db->select('*');
		$this->db->from('sharetypes');
		$this->db->join('sharesBought', 'sharesBought.sharetype = sharetypes.Id');
		$this->db->where(array('Receiptnumber' => $receipt));
		$query = $this->db->get();
		foreach ($query->result() as $row) {
			return $row->seriesPrice;
		}
	}

	function getShareAmount2($id)
	{
		$this->db->select('*');
		$this->db->from('sharetypes');
		$this->db->join('payment', 'payment.shareType = sharetypes.Id');
		$this->db->where(array('payment.Id' => $id));
		$query = $this->db->get();
		foreach ($query->result() as $row) {
			return $row->seriesPrice;
		}
	}

	public function updateShareType($user)
	{
		if (isset($user['Id'])) {
			$this->db->where('Id', $user['Id']);
			$this->db->update('sharetypes', $user);
		} else {
			$this->db->insert('sharetypes', $user);
		}

	}

	function getReceivedAmount($receipt)
	{
		$this->db->select('*');
		$this->db->from('sharetypes');
		$this->db->join('sharesBought', 'sharesBought.sharetype = sharetypes.Id');
		$this->db->where(array('Receiptnumber' => $receipt));
		$query = $this->db->get();
		foreach ($query->result() as $row) {
			return $row->amountreceived;
		}
	}

	function getReceivedAmount2($id)
	{
		$this->db->select('*');
		$this->db->from('sharetypes');
		$this->db->join('payment', 'payment.shareType = sharetypes.Id');
		$this->db->where(array('payment.Id' => $id));
		$query = $this->db->get();
		foreach ($query->result() as $row) {
			return $row->amountreceived;
		}
	}

	function getshareType($Id)
	{
		$this->db->select('*');
		$this->db->from('sharetypes');
		$this->db->join('payment', 'payment.shareType = sharetypes.Id');
		$this->db->where('payment.Id', $Id);
		$query = $this->db->get();
		foreach ($query->result() as $row) {
			return $row->type;
		}
	}

	function getshareTypeId($Id)
	{
		$this->db->select('*');
		$this->db->from('payment');
		$this->db->where('Id', $Id);
		$query = $this->db->get();
		foreach ($query->result() as $row) {
			return $row->shareType;
		}
	}

	function getPaymentTotal($Id)
	{
		$this->db->select('*');
		$this->db->from('payment');
		$this->db->where('Id', $Id);
		$query = $this->db->get();
		foreach ($query->result() as $row) {
			return $row->Amount;
		}
	}

	function getshareType2($receipt)
	{
		$this->db->select('*');
		$this->db->from('sharetypes');
		$this->db->join('payment', 'payment.shareType = sharetypes.Id');
		$this->db->where('payment.PaymentUUID', $receipt);
		$query = $this->db->get();
		foreach ($query->result() as $row) {
			return $row->type;
		}
	}

	function getsharePrice($Id)
	{
		$this->db->select('sharetypes.seriesPrice');
		$this->db->from('sharetypes');
		$this->db->join('payment', 'payment.shareType = sharetypes.Id');
		$this->db->where('payment.Id', $Id);
		$query = $this->db->get();
		foreach ($query->result() as $row) {
			return $row->seriesPrice;
		}
	}

	function getsharePrice2($receipt)
	{
		$this->db->select('sharetypes.seriesPrice');
		$this->db->from('sharetypes');
		$this->db->join('payment', 'payment.shareType = sharetypes.Id');
		$this->db->where('payment.PaymentUUID', $receipt);
		$query = $this->db->get();
		foreach ($query->result() as $row) {
			return $row->seriesPrice;
		}
	}

	function get_shareHolders($receipt)
	{
		$this->db->select('*');
		$this->db->from('shareholders');
		$this->db->join('sharesBought', 'sharesBought.shareholder = shareholders.Id');
		$this->db->where(array('Receiptnumber' => $receipt));
		$query = $this->db->get();
		foreach ($query->result() as $row) {
			return $row->Name;
		}
	}

	function get_shareHoldersIdnumber($receipt)
	{
		$this->db->select('*');
		$this->db->from('shareholders');
		$this->db->join('sharesBought', 'sharesBought.shareholder = shareholders.Id');
		$this->db->where(array('Receiptnumber' => $receipt));
		$query = $this->db->get();
		foreach ($query->result() as $row) {
			if (($row->type) == 0) {
				return $row->IdNo;
			} else {
				return $row->krapin;
			}
		}
	}

	function receiptNumber($length = 4)
	{
		$receipt = substr(str_shuffle("0123456789"), 0, $length);
		$receipt1 = "CBP/" . $receipt . "/" . date("Y");

		return $receipt1;
	}

	function regReceipt($length = 4)
	{
		$receipt = substr(str_shuffle("0123456789"), 0, $length);
		$receipt1 = "REG/" . $receipt . "/" . date("Y");

		return $receipt1;
	}

	function payUuid($length = 6)
	{
		$receipt = substr(str_shuffle("0123456789"), 0, $length);
		$receipt1 = md5($receipt);

		return $receipt1;
	}

	public function getReg($idnumber, $limit, $offset, $sort_by, $sort_order)
	{

		$sort_order = ($sort_order == 'desc') ? 'desc' : 'asc';
		$sort_columns = array('receipt', 'paymentmode', 'code', 'amount', 'datepaid', 'shareholder', 'receiver');
		$sort_by = (in_array($sort_by, $sort_columns)) ? $sort_by : 'payId';

		$q = $this->db->select('*')->from('regpay')->where('idnumber', $idnumber)->limit($limit, $offset)
			->order_by($sort_by, $sort_order);
		$query = $q->get()->result();

		$q1 = $this->db->select('COUNT(*) as count', false)->from('sharesBought')->where('idnumber', $idnumber);
		$tmp = $q1->get()->result();

		$result['rows'] = $query;
		$result['row_count'] = $tmp[0]->count;

		return $result;

	}

	public function getReg1($id)
	{
		$this->db->select('*')->from('regpay')->where('shareholderId', $id);
		$query = $this->db->get();
		foreach ($query->result() as $row) {
			return $row->shareholderId;
		}
	}
}
