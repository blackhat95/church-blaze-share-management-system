<?php if (!defined('BASEPATH')) {
	exit('No direct script access allowed');
}

class Agents_model extends CI_Model
{

	public function __construct()
	{
		parent::__construct();
	}

	public function get($limit, $offset, $sort_by, $sort_order)
	{
		$sort_order = ($sort_order == 'desc') ? 'desc' : 'asc';
		$sort_columns = array('IDNo', 'Location', 'AgName', 'Number', 'Telephone', 'Email');
		$sort_by = (in_array($sort_by, $sort_columns)) ? $sort_by : 'AgName';

		$q = $this->db->select('*')->from('agents as ag')->join('users', 'users.link = ag.Id')->limit($limit, $offset)
			->order_by($sort_by, $sort_order);

		$query = $q->get()->result();

		$q = $this->db->select('COUNT(*) as count', false)->from('agents');
		$tmp = $q->get()->result();

		$result['rows'] = $query;
		$result['row_count'] = $tmp[0]->count;

		return $result;

	}

	public function editGet($id = null)
	{
		$this->db->select('*');
		$this->db->from('agents as ag');
		$this->db->join('users', 'users.link = ag.Id');

		if ($id != null) {
			$this->db->where(array('ag.Id' => $id));
		}
		$query = $this->db->get();
		if ($id != null) {
			return $query->row_array();
		}
	}

	public function getsearch($index)
	{
		$q = $this->db->select('*')->from('agents')->where('agents.IDNo', $index)->or_where('agents.Number', $index)
			->join('users', 'users.link = agents.Id');
		$query = $q->get()->result();

		$result['rows'] = $query;

		return $result;
	}

	public function remove($id)
	{
		$this->db->where('Id', $id);
		$this->db->delete('agents');
	}

	public function add($index)
	{
		if (isset($index['Id'])) {
			$this->db->where('Id', $index['Id']);
			$this->db->update('agents', $index);
		} else {
			$this->db->insert('agents', $index);
		}
	}

	public function addUser($user)
	{
		if (isset($user['id'])) {
			$this->db->where('id', $user['id']);
			$this->db->update('users', $user);
		} else {
			$this->db->insert('users', $user);
		}

	}

	function agentNumberGen($length = 5)
	{
		$string = substr(str_shuffle("0123456789"), 0, $length);
		$formNum = "CBA/" . $string . "/" . date("Y");

		return $formNum;
	}
}
