<!-- /. NAV SIDE  -->
<div id="page-wrapper">
	<div id="page-inner">
		<!--BEGIN TITLE & BREADCRUMB PAGE-->
		<div id="title-breadcrumb-option-demo" class="page-title-breadcrumb">
			<div class="page-header pull-left">
				<div class="page-title">
					System Settings
				</div>
			</div>
			<ol class="breadcrumb page-breadcrumb pull-right">
				<li><i class="fa fa-home"></i>&nbsp;<a href="<?php echo base_url() . "home" ?>">Home</a>&nbsp;&nbsp;
				</li>
				<li><i class="fa fa-gears"></i>&nbsp;&nbsp;<a href="<?php echo base_url() . 'settings/settingsHome' ?>">System
						Settings</a></li>
				<li class="active"><i class="fa fa-edit"></i>&nbsp;&nbsp;Edit Share Types</li>
			</ol>
			<div class="clearfix">
			</div>
		</div>
		<!--END TITLE & BREADCRUMB PAGE-->
		<div class="row">
			<div class="col-md-12">
				<div class="alert alert-info">
					<i class="fa fa-info-circle"></i>
					<strong>Heads up!</strong>
					This form helps with the addition of sharetypes in the system!
				</div>
				<br/>
				<?php if (strlen($success) > 0) {
					?>
					<div class="alert alert-success" id="success"><i class="fa fa-check"></i>&nbsp;<?php echo $success
							. ''; ?>
					</div>
					<?php
				}
				?>
				<?php if (strlen($error) > 0) {
					?>
					<div class="alert alert-danger" id="error"><i class="fa fa-ban"></i>&nbsp;<?php echo $error . ''; ?>
					</div>
					<?php
				}
				?>
			</div>
		</div>
		<!-- /. ROW  -->
		<div class="row">
			<div class="col-md-12">
				<!-- Advanced Tables -->
				<div class="panel panel-pink">
					<div class="panel-heading">
						<div class="row">
							<div class="col-sm-6">
								<h3>Update Sharetypes</h3>
							</div>
						</div>
					</div>
					<div class="panel-body">
						<?php $this->load->helper('form'); ?>
						<?php echo form_open('settings/update'); ?>
						<div class="form-body pal">
							<div class="row">
								<div class="col-md-4">
									<div class="form-group">
										<label>Share Name</label>
										<?php echo form_input(array("class" => "form-control",
											"placeholder" => "Share Name", "name" => "name",
											"value" => $view_data['type'], "required" => "true")) ?>
									</div>
								</div>
								<div class="col-md-4">
									<div class="form-group">
										<label>Share Total</label>
										<?php echo form_input(array("class" => "form-control",
											"placeholder" => "Share Total", "name" => "sharetotal",
											"value" => $view_data['sharetotal'], "required" => "true")) ?>
									</div>
								</div>
								<div class="col-md-4">
									<div class="form-group">
										<label>Share Price</label>
										<?php echo form_input(array("class" => "form-control",
											"placeholder" => "Share Price", "name" => "price",
											"value" => $view_data['seriesPrice'], "readonly" => "true")) ?>
									</div>
								</div>
							</div>
							<div class="row">
								<div class="col-md-8">
									<div class="form-group">
										<label>Description</label>
										<?php echo form_textarea(array("class" => "form-control",
											"placeholder" => "Description", "name" => "desc",
											"value" => $view_data['Description'], "required" => "true")) ?>
									</div>
								</div>
								<div class="col-md-4">
									<div class="form-group">
										<label>Share Sold</label>
										<?php echo form_input(array("class" => "form-control",
											"placeholder" => "Share Sold", "name" => "sharesold",
											"value" => $view_data['sharesold'], "readonly" => "true")) ?>
									</div>
									<div class="form-group hidden">
										<label>Share Type Id</label>
										<?php echo form_input(array("class" => "form-control", "placeholder" => "Id",
											"name" => "Id", "value" => $view_data['Id'], "readonly" => "true")) ?>
									</div>
									<div class="form-group">
										<label>Available Shares</label>
										<?php $total = $view_data['sharetotal']; ?>
										<?php $used = $view_data['sharesold'] ?>
										<?php $avail = ($total - $used) ?>
										<?php echo form_input(array("class" => "form-control", "placeholder" => "",
											"name" => "available", "value" => $avail, "readonly" => "true")) ?>
									</div>
								</div>
							</div>
							<div class="row">
								<div class="col-md-6">
									<div class="form-group">
										<label>Added By</label>
										<?php echo form_input(array("class" => "form-control", "placeholder" => "Location",
											"name" => "addedby", "readonly" => "true",
											"value" => $view_data['AddedBy'])) ?>
									</div>
								</div>
								<div class="col-md-6">
									<div class="form-group">
										<label>Edited By</label>
										<?php echo form_input(array("class" => "form-control", "placeholder" => "Location",
											"name" => "editedby", "readonly" => "true",
											"value" => $this->session->userdata('name'))) ?>
									</div>
								</div>
							</div>
							<hr/>
							<div class="row">
								<div class="col-md-4">
									<div class="form-group">
										<?php echo form_hidden('dateadded', $view_data['dateadded'],
											'class="form-control"'); ?>
									</div>
								</div>
								<div class="col-md-4">
									<div class="form-group">
									</div>
								</div>
								<div class="col-md-4">
									<div class="form-group">
										<button type="reset" class="btn btn-danger pull-right">Cancel</button>
										<?php echo form_submit('update', 'Update Share Types',
											'class="btn btn-success pull-right margin-right"'); ?>

									</div>
								</div>
							</div>
						</div>
						</form>
						<!-- /.row (nested) -->
					</div>
					<!-- /.panel-body -->
				</div>
				<!-- /.panel -->
			</div>
			<!-- /.col-lg-12 -->
		</div>
