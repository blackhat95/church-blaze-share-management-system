<!-- /. NAV SIDE  -->
<div id="page-wrapper">
	<div id="page-inner">
		<!--BEGIN TITLE & BREADCRUMB PAGE-->
		<div id="title-breadcrumb-option-demo" class="page-title-breadcrumb">
			<div class="page-header pull-left">
				<div class="page-title">
					System Dashboard
				</div>
			</div>
			<ol class="breadcrumb page-breadcrumb pull-right">
				<li><i class="fa fa-home"></i>&nbsp;<a href="<?php echo base_url() . "home" ?>">Home</a>&nbsp;&nbsp;
				</li>
				<li><i class="fa fa-dashboard"></i>&nbsp;&nbsp;System Dashboard</li>
			</ol>
			<div class="clearfix">
			</div>
		</div>
		<!--END TITLE & BREADCRUMB PAGE-->
		<div class="row">
			<ul class="nav nav-tabs nav-justified">
				<li class="active"><a data-toggle="tab" href="#sectionA"><h5><i class="fa fa-bar-chart-o"></i>&nbsp;Totals
						</h5></a></li>
				<li><a data-toggle="tab" href="#sectionB"><h5><i class="fa fa-shopping-cart"></i>&nbsp;Subscribed Report</h5>
					</a>
				</li>
				<li><a data-toggle="tab" href="#sectionC"><h5><i class="fa fa-comments-o"></i>&nbsp;Agents</h5></a>
				</li>
				<li><a data-toggle="tab" href="#sectionD"><h5><i class="fa fa-users"></i>&nbsp;Shareholders</h5></a>
				</li>
			</ul>
			<div class="tab-content">
				<div id="sectionA" class="tab-pane fade in active">
					<hr/>
					<div class="row">
						<div class="col-md-3 col-sm-12 col-xs-12">
							<div class="panel panel-primary text-center no-boder bg-color-royalpurple">
								<div class="panel-body">
									<i class="fa fa-bar-chart-o fa-5x"></i>

									<h3><?php echo $sharesum ?> </h3>
								</div>
								<div class="panel-footer back-footer-royalpurple">
									Total Number of Shares
								</div>
							</div>
						</div>
						<div class="col-md-3 col-sm-12 col-xs-12">
							<div class="panel panel-primary text-center no-boder bg-color-lavetafebbraio">
								<div class="panel-body">
									<i class="fa fa-money fa-5x"></i>

									<h3><?php echo $expected ?> </h3>
								</div>
								<div class="panel-footer back-footer-lavetafebbraio">
									Total Expected Amount
								</div>
							</div>
						</div>
						<div class="col-md-3 col-sm-12 col-xs-12">
							<div class="panel panel-primary text-center no-boder bg-color-mwendwayellow">
								<div class="panel-body">
									<i class="fa fa-shopping-cart fa-5x"></i>

									<h3><?php echo $sharesold ?> </h3>
								</div>
								<div class="panel-footer back-footer-mwendwayellow">
									Subscribed Shares
								</div>
							</div>
						</div>
						<div class="col-md-3 col-sm-12 col-xs-12">
							<div class="panel panel-primary text-center no-boder bg-color-blue">
								<div class="panel-body">
									<i class="fa fa-pie-chart fa-5x"></i>

									<h3><?php echo $avail ?> </h3>
								</div>
								<div class="panel-footer back-footer-blue">
									UnSubscribed Shares
								</div>
							</div>
						</div>
						<div class="col-md-3 col-sm-12 col-xs-12">
							<div class="panel panel-primary text-center no-boder bg-color-Sun-RipenedBikini">
								<div class="panel-body">
									<i class="fa fa-credit-card fa-5x"></i>

									<h3><?php echo $totalReg ?> </h3>
								</div>
								<div class="panel-footer back-footer-Sun-RipenedBikini">
									Registration total amount
								</div>
							</div>
						</div>
					</div>
				</div>
				<div id="sectionB" class="tab-pane fade">
					<hr/>
					<div class="row">
						<div class="col-md-3 col-sm-12 col-xs-12">
							<div class="panel panel-primary text-center no-boder bg-color-sunflower">
								<div class="panel-body">
									<i class="fa fa-bitcoin fa-5x"></i>

									<h3><?php echo $subtotal ?> </h3>
								</div>
								<div class="panel-footer back-footer-sunflower">
									Subcribed Share Expected Amount
								</div>
							</div>
						</div>
						<div class="col-md-3 col-sm-12 col-xs-12">
							<div class="panel panel-primary text-center no-boder bg-color-Sun-RipenedBikini">
								<div class="panel-body">
									<i class="fa fa-dollar fa-5x"></i>

									<h3><?php echo $paidsum ?> </h3>
								</div>
								<div class="panel-footer back-footer-Sun-RipenedBikini">
									Subcribed Share Paid Amount
								</div>
							</div>
						</div>
						<div class="col-md-3 col-sm-12 col-xs-12">
							<div class="panel panel-primary text-center no-boder bg-color-TribalTriumph">
								<div class="panel-body">
									<i class="fa fa-credit-card fa-5x"></i>

									<h3><?php echo $notpaid ?> </h3>
								</div>
								<div class="panel-footer back-footer-TribalTriumph">
									Subcribed Share UnPaid Amount
								</div>
							</div>
						</div>
					</div>
				</div>
				<div id="sectionC" class="tab-pane fade">
					<hr/>
					<div class="row">
						<div class="col-md-3 col-sm-12 col-xs-12">
							<div class="panel panel-primary text-center no-boder bg-color-red">
								<div class="panel-body">
									<i class="fa fa fa-comments fa-5x"></i>

									<h3><?php echo $agents ?> </h3>
								</div>
								<div class="panel-footer back-footer-red">
									Total Number of Agents
								</div>
							</div>
						</div>
					</div>
				</div>
				<div id="sectionD" class="tab-pane fade">
					<hr/>
					<div class="row">
						<div class="col-md-3 col-sm-12 col-xs-12">
							<div class="panel panel-primary text-center no-boder bg-color-brown">
								<div class="panel-body">
									<i class="fa fa-users fa-5x"></i>

									<h3><?php echo $shareholders ?> </h3>
								</div>
								<div class="panel-footer back-footer-brown">
									No. of Shareholders
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<hr/>
		<hr/>
		<hr/>
		<!-- /. ROW  -->
