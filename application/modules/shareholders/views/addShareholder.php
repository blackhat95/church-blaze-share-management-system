<!-- /. NAV SIDE  -->
<div id="page-wrapper">
	<div id="page-inner">
		<!--BEGIN TITLE & BREADCRUMB PAGE-->
		<div id="title-breadcrumb-option-demo" class="page-title-breadcrumb">
			<div class="page-header pull-left">
				<div class="page-title">
					Shareholder Management
				</div>
			</div>
			<ol class="breadcrumb page-breadcrumb pull-right">
				<li><i class="fa fa-home"></i>&nbsp;<a href="<?php echo base_url() . "home" ?>">Home</a>&nbsp;&nbsp;
				</li>
				<li><i class="fa fa-users"></i><a href='<?php echo base_url() . 'shareholders/shareholderAdd' ?>'>&nbsp;&nbsp;Shareholder
						Management</a></li>
				<li class="active"><i class="fa fa-user-plus"></i>&nbsp;&nbsp;Add Individual Shareholders</li>
			</ol>
			<div class="clearfix">
			</div>
		</div>
		<!--END TITLE & BREADCRUMB PAGE-->
		<div class="row">
			<div class="col-md-12">
				<div class="alert alert-info">
					<i class="fa fa-info-circle"></i>
					<strong>Heads up!</strong>
					The addition of the Individual Shareholders into the system!
				</div>
				<br/>
				<?php if (strlen($success) > 0) {
					?>
					<div class="alert alert-success" id="success"><i class="fa fa-check"></i>&nbsp;<?php echo $success
							. ''; ?>
					</div>
					<?php
				}
				?>
				<?php if (strlen($error) > 0) {
					?>
					<div class="alert alert-danger" id="error"><i class="fa fa-ban"></i>&nbsp;<?php echo $error . ''; ?>
					</div>
					<?php
				}
				?>
			</div>
		</div>
		<!-- /. ROW  -->
		<div class="row">
			<div class="col-md-12">
				<!-- Advanced Tables -->
				<?php if (($this->session->userdata('role') == "Admin") OR ($this->session->userdata('role') == "Finance")
				OR ($this->session->userdata('role') == "Admin2")) { ?>
				<div class="panel panel-pink">
					<?php } ?>
					<?php if (($this->session->userdata('role') == "Agent")) { ?>
					<div class="panel panel-green">
						<?php } ?>
						<div class="panel-heading">
							<div class="row">
								<div class="col-sm-6">
									<h3>Add Individual Shareholders</h3>
								</div>
							</div>
						</div>
						<div class="panel-body">
							<?php $this->load->helper('form'); ?>
							<?php echo form_open('shareholders/save'); ?>
							<div class="form-body pal">
								<div class="row">
									<div class="col-md-12">
										<div class="form-group">
											<label>Shareholder Name</label>
											<?php echo form_input(array("class" => "form-control",
												"placeholder" => "Enter Shareholder names", "name" => "name",
												"required" => "true")) ?>
										</div>
									</div>
								</div>
								<div class="row">
									<div class="col-md-4">
										<div class="form-group">
											<label>Country</label>
											<?php echo form_input(array("class" => "form-control",
												"placeholder" => "Country", "name" => "country",
												"required" => "true")) ?>
										</div>
									</div>
									<div class="col-md-4">
										<div class="form-group">
											<label>County</label>
											<?php echo form_input(array("class" => "form-control",
												"placeholder" => "County", "name" => "county", "required" => "true")) ?>
										</div>
									</div>
									<div class="col-md-4">
										<div class="form-group">
											<label>Town</label>
											<?php echo form_input(array("class" => "form-control", "placeholder" => "Town",
												"name" => "town", "required" => "true")) ?>
										</div>
									</div>
								</div>
								<div class="row">
									<div class="col-md-4">
										<div class="form-group">
											<label>Village</label>
											<?php echo form_input(array("class" => "form-control",
												"placeholder" => "Village", "name" => "village",
												"required" => "true")) ?>
										</div>
									</div>
									<div class="col-md-4">
										<div class="form-group">
											<label>Id / Passport Number</label>
											<?php echo form_input(array("class" => "form-control",
												"placeholder" => "Id / Passport Number", "name" => "idnumber",
												"required" => "true")) ?>
										</div>
									</div>
									<div class="col-md-4">
										<div class="form-group">
											<label>Postal Address</label>
											<?php echo form_input(array("class" => "form-control",
												"placeholder" => "Postal Address", "name" => "postaladdress",
												"required" => "true")) ?>
										</div>
									</div>
								</div>
								<div class="row">
									<div class="col-md-4">
										<label>Mobile Number</label>
										<?php echo form_input(array("class" => "form-control",
											"placeholder" => "Mobilenumber | Minimum length of 10 Characters",
											"name" => "mobile", "required" => "true")) ?>
									</div>
									<div class="col-md-4">
										<div class="form-group">
											<label>Email</label>
											<?php echo form_input(array("class" => "form-control",
												"placeholder" => "Email address", "name" => "email",
												"type" => "email")) ?>
										</div>
									</div>
									<div class="col-md-4">
										<?php if (($this->session->userdata('role') == "Admin")
											OR ($this->session->userdata('role') == "Finance")
											OR ($this->session->userdata('role') == "Admin2")
										) { ?>
											<div class="form-group">
												<label>Reffered by Agent</label>
												<select class="form-control" id="agents" name="agents">
													<option value="0">Please Select One....</option>
													<?php foreach ($view_data as $data): ?>
														<option
															value="<?php echo $data->Id; ?>"><?php echo $data->AgName; ?></option>
													<?php endforeach; ?>
												</select>
											</div>
										<?php } ?>
										<?php if (($this->session->userdata('role') == "Agent")) { ?>
											<div class="form-group">
												<label>Reffered by Agent</label>
												<?php echo form_input(array("class" => "form-control",
													"placeholder" => "Agent Name", "name" => "agentname",
													"readonly" => "true",
													"value" => $this->session->userdata('name'))) ?>
												<?php echo form_hidden('agents', $this->session->userdata('id'),
													'class="form-control"'); ?>
											</div>
										<?php } ?>
									</div>
								</div>
								<div class="row">
									<div class="col-md-6">
										<div class="form-group">
											<label>Physical Form Number</label>
											<?php echo form_input(array("class" => "form-control",
												"placeholder" => "Physical Form Number", "name" => "fnumber",
												"required" => "true", "type" => "text")) ?>
										</div>
									</div>
									<!--<div class="col-md-6">
										<div class="form-group">
											<label>Shareholder Image</label>
											<?php /*echo form_upload(array ( 'class' => 'form-control', 'placeholder' => 'Add
											the shareholders image', 'name' => 'image', 'id' => 'image', 'required' => 'true'
											)) */ ?>
										</div>
									</div>-->
								</div>
								<div class="row">
									<div class="col-md-6">
										<div class="form-group">
											<label>User Role</label>
											<?php echo form_input(array("class" => "form-control", "placeholder" => "Role",
												"name" => "role", "value" => "Shareholder", "readonly" => "true")) ?>
										</div>
									</div>
									<div class="col-md-6">
										<div class="form-group">
											<label>Added By</label>
											<?php echo form_input(array("class" => "form-control",
												"placeholder" => "Location", "name" => "addedby", "readonly" => "true",
												"value" => $this->session->userdata('name'))) ?>
											<?php echo form_hidden('type', 0, 'class="form-control"'); ?>
										</div>
									</div>
								</div>
								<hr/>
								<div class="row">
									<div class="col-md-4">
										<div class="form-group">
										</div>
									</div>
									<div class="col-md-4">
										<div class="form-group">
										</div>
									</div>
									<div class="col-md-4">
										<div class="form-group">
											<button type="reset" class="btn btn-danger pull-right">Cancel</button>
											<?php echo form_submit('save', 'Add Shareholder',
												'class="btn btn-success pull-right margin-right"'); ?>

										</div>
									</div>
								</div>
							</div>
							</form>
							<!-- /.row (nested) -->
						</div>
						<!-- /.panel-body -->
					</div>
					<!-- /.panel -->
				</div>
				<!-- /.col-lg-12 -->
			</div>
