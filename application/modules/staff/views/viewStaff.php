<!-- /. NAV SIDE  -->
<div id="page-wrapper">
	<div id="page-inner">
		<!--BEGIN TITLE & BREADCRUMB PAGE-->
		<div id="title-breadcrumb-option-demo" class="page-title-breadcrumb">
			<div class="page-header pull-left">
				<div class="page-title">
					Staff Management
				</div>
			</div>
			<ol class="breadcrumb page-breadcrumb pull-right">
				<li><i class="fa fa-home"></i>&nbsp;<a href="<?php echo base_url() . "home" ?>">Home</a>&nbsp;&nbsp;
				</li>
				<li><i class="fa fa-user-secret"></i><a href="<?php echo base_url() . "users/dashboard" ?>">&nbsp;
						&nbsp;Staff Management</a></li>
				<li class="active"><i class="fa fa-edit"></i>&nbsp;&nbsp;View Staff</li>
			</ol>
			<div class="clearfix">
			</div>
		</div>
		<!--END TITLE & BREADCRUMB PAGE-->
		<div class="row">
			<div class="col-md-12">
				<div class="alert alert-info">
					<i class="fa fa-info-circle"></i>
					<strong>Heads up!</strong>
					This Displays The lists of the staff in churchblaze!
				</div>
				<br/>
				<?php if (strlen($success) > 0) {
					?>
					<div class="alert alert-success" id="success"><i class="fa fa-check"></i>&nbsp;<?php echo $success
							. ''; ?>
					</div>
					<?php
				}
				?>
				<?php if (strlen($error) > 0) {
					?>
					<div class="alert alert-danger" id="error"><i class="fa fa-ban"></i>&nbsp;<?php echo $error . ''; ?>
					</div>
					<?php
				}
				?>
				<?php if (strlen($info) > 0) {
					?>
					<div class="alert alert-info" id="info"><i class="fa fa-info-circle"></i>&nbsp;<?php echo $info . ''; ?>
					</div>
					<?php
				}
				?>
			</div>
		</div>
		<!-- /. ROW  -->
		<div class="row">
			<div class="col-md-12">
				<!-- Advanced Tables -->
				<?php if (($this->session->userdata('role') == "Admin") OR ($this->session->userdata('role') == "Finance")
				OR ($this->session->userdata('role') == "Admin2")) { ?>
				<div class="panel panel-pink">
					<?php } ?>
					<?php if (($this->session->userdata('role') == "Agent")) { ?>
					<div class="panel panel-green">
						<?php } ?>
						<div class="panel-heading">
							<div class="row">
								<div class="col-sm-6">
									<h3>Staff Search</h3>
								</div>
							</div>
						</div>
						<div class="panel-body">
							<?php $this->load->helper('form'); ?>
							<?php echo form_open('staff/search'); ?>
							<div class="form-body pal">
								<div class="row">
									<div class="col-md-3">
										<div class="form-group">
											<label>Enter staff Work Number to search</label>
										</div>
									</div>
									<div class="col-md-7">
										<div class="form-group">
											<?php echo form_input(array("class" => "form-control",
												"placeholder" => "Enter work number to search", "name" => "search")) ?>
										</div>
									</div>
									<div class="col-md-2">
										<div class="form-group">
											<?php echo form_submit('save', 'Search',
												'class="btn btn-success pull-left margin-right"'); ?>

										</div>
									</div>
								</div>
							</div>
							</form>
							<!-- /.row (nested) -->
						</div>
						<?php
						if ($view_data2 != null) {
							?>
							<hr/>
							<div class="panel-body">
								<div class="table-responsive">
									<table class="table table-striped table-hover table-condensed" id="datatable-icons">
										<thead class="bold">
										<tr>
											<th>Full Names</th>
											<?php foreach ($headers as $field_name => $field_Column): ?>
												<th>
													<?php echo $field_Column ?>
												</th>
											<?php endforeach; ?>
										</tr>
										</thead>
										<tbody>
										<?php foreach ($view_data2 as $data): ?>
											<tr>
												<td>
													<?php echo $data->Fname; ?>&nbsp;<?php echo $data->Sname; ?>
													&nbsp;<?php echo $data->Lname; ?>
												</td>
												<?php foreach ($headers as $field_name => $field_Column): ?>
													<td>
														<?php echo $data->$field_name ?>
													</td>
												<?php endforeach; ?>
												<td>
													<a class="btn btn-info btn-sm"
													   href="<?php echo base_url() . "staff/staffEdit/" . $data->Id; ?>"
													   data-toggle="tooltip"
													   data-placement="top"
													   title="Edit">Edit</a>
												</td>
												<td>
													<a class="btn btn-danger btn-sm"
													   href="<?php echo base_url() . "staff/remove/" . $data->Id; ?>"
													   onclick="return confirm('Are you sure you want to delete');"
													   data-toggle="tooltip"
													   data-placement="top"
													   title="Delete">Delete</a>
												</td>
											</tr>
										<?php endforeach; ?>
										</tbody>
									</table>
								</div>
							</div>
						<?php } ?>
					</div>
					<!-- /.panel-body -->
				</div>

			</div>
		</div>
		<hr/>
		<div class="row">
			<div class="col-md-12">
				<!-- Advanced Tables -->
				<div class="panel panel-pink">
					<div class="panel-heading">
						<div class="row">
							<div class="col-sm-6">
								<h3>Churchblaze Staff</h3>
							</div>
							<div class="col-sm-6"><a class="btn btn-info btn-sm pull-right"
							                         href="<?php echo base_url() . "staff/addStaff" ?>">Add
									Staff</a></div>
						</div>
					</div>
					<div class="panel-body">
						<div class="row">
							<div class="col-sm-6">
							</div>
							<div class="col-sm-6">
								<p class=" records">Found&nbsp;<?php echo $rownumber; ?>&nbsp;Entries</p>
							</div>
						</div>
						<div class="table-responsive">
							<table class="table table-striped table-hover table-condensed" id="datatable-icons">
								<thead class="bold">
								<tr>
									<th <?php if ($sort_by == "Fname")
										echo "class=\"sort_$sort_order\"" ?>><?php echo anchor("staff/viewStaff/Fname/"
											. (($sort_order == "asc" && $sort_by == "Fname") ? "desc" : "asc"),
											"Full Names") ?></th>
									<?php foreach ($fields as $field_name => $field_Column): ?>
										<th <?php if ($sort_by == $field_name)
											echo " class=\"sort_$sort_order\"" ?>>
											<?php echo anchor("staff/viewStaff/$field_name/" . (($sort_order == "asc"
													&& $sort_by == $field_name) ? "desc" : "asc"), $field_Column) ?>
										</th>
									<?php endforeach; ?>
								</tr>
								</thead>
								<tbody>
								<?php foreach ($view_data as $data): ?>
									<tr>
										<td>
											<?php echo $data->Fname; ?>&nbsp;<?php echo $data->Sname; ?>
											&nbsp;<?php echo $data->Lname; ?>
										</td>
										<?php foreach ($fields as $field_name => $field_Column): ?>
											<td>
												<?php echo $data->$field_name ?>
											</td>
										<?php endforeach; ?>
										<td>
											<a class="btn btn-info btn-sm"
											   href="<?php echo base_url() . "staff/staffEdit/" . $data->Id; ?>"
											   data-toggle="tooltip"
											   data-placement="top"
											   title="Edit">Edit</a>
										</td>
										<td>
											<a class="btn btn-danger btn-sm"
											   href="<?php echo base_url() . "staff/remove/" . $data->Id; ?>"
											   onclick="return confirm('Are you sure you want to delete');"
											   data-toggle="tooltip"
											   data-placement="top"
											   title="Delete">Delete</a>
										</td>
									</tr>
								<?php endforeach; ?>
								</tbody>
							</table>
						</div>
						<div class="row">
							<div class="col-sm-6">
							</div>
							<div class="col-sm-6">
								<?php if (strlen($pagination)) {
									;
								}
								{ ?>
									<p class=" records">Pages&nbsp;<?php echo $pagination; ?>&nbsp;</p>
								<?php } ?>
							</div>
						</div>
						<!-- /.row (nested) -->
					</div>
					<!-- /.panel-body -->
				</div>
				<!-- /.panel -->
			</div>
			<!-- /.col-lg-12 -->
		</div>
