<?php

class user_model extends CI_Model {
	private $username;

	public function __construct() {
		$this->load->database();
	}

	public function get_user($username) {
		$query = $this->db->query("SELECT * FROM users WHERE users.username = '" . $username . "'");

		return $query->result_array();
	}

	public function getRole() {
		$username = $this->input->post("username");
		$passcode = md5($this->input->post("password"));
		$this->db->select('*');
		$this->db->from('users');
		$this->db->where('username', $username);
		$this->db->where('password', $passcode);
		$query = $this->db->get();
		foreach ($query->result() as $row) {
			return $row->role;
		}
	}

	public function logindetails() {
		$username = $this->input->post("username");
		$query = $this->db->query("SELECT * FROM users WHERE username = '" . $username . "'");
		foreach ($query->result() as $row) {
			return $row->password;
		}
	}

	public function userstatus() {
		$username = $this->input->post("username");
		$passcode = md5($this->input->post("password"));
		$this->db->select('*');
		$this->db->from('users');
		$this->db->where('username', $username);
		$this->db->where('password', $passcode);
		$query = $this->db->get();
		foreach ($query->result() as $row) {
			return $row->status;
		}
	}

	public function changepassword() {
		$this->load->library('session');
		$user = $this->session->userdata('username');
		$password = $this->input->post("password");

		$sql = "UPDATE user SET password = " . $this->db->escape(md5($password)) . "WHERE username = '" . $user . "'";

		$this->db->query($sql);

	}

	public function getStaffName() {
		$username = $this->input->post("username");
		$passcode = md5($this->input->post("password"));
		$this->db->select('*');
		$this->db->from('staff');
		$this->db->join('users', 'users.link2 = staff.Id');
		$this->db->where('username', $username);
		$this->db->where('password', $passcode);
		$query = $this->db->get();
		foreach ($query->result() as $row) {
			$fname = $row->Fname;
			$sname = $row->Sname;
			$lname = $row->Lname;
			$Name = $fname . " " . $sname . " " . $lname;

			return $Name;
		}
	}

	public function getStaffId() {
		$username = $this->input->post("username");
		$passcode = md5($this->input->post("password"));
		$this->db->select('*');
		$this->db->from('staff');
		$this->db->join('users', 'users.link2 = staff.Id');
		$this->db->where('username', $username);
		$this->db->where('password', $passcode);
		$query = $this->db->get();
		foreach ($query->result() as $row) {
			return $row->Id;
		}
	}

	public function getStaffId1($Id) {
		$this->db->select('*');
		$this->db->from('staff');
		$this->db->join('users', 'users.link2 = staff.Id');
		$this->db->where('staff.Id', $Id);
		$query = $this->db->get();
		foreach ($query->result() as $row) {
			return $row->password;
		}
	}

	public function getUserId($Id) {
		$this->db->select('*');
		$this->db->from('staff');
		$this->db->join('users', 'users.link2 = staff.Id');
		$this->db->where('staff.Id', $Id);
		$query = $this->db->get();
		foreach ($query->result() as $row) {
			return $row->id;
		}
	}

	public function getAgentId1($Id) {
		$this->db->select('*');
		$this->db->from('agents');
		$this->db->join('users', 'users.link = agents.Id');
		$this->db->where('agents.Id', $Id);
		$query = $this->db->get();
		foreach ($query->result() as $row) {
			return $row->password;
		}
	}

	public function getUserId2($Id) {
		$this->db->select('*');
		$this->db->from('agents');
		$this->db->join('users', 'users.link = agents.Id');
		$this->db->where('agents.Id', $Id);
		$query = $this->db->get();
		foreach ($query->result() as $row) {
			return $row->id;
		}
	}

	public function getShareholderId1($Id) {
		$this->db->select('*');
		$this->db->from('shareholderss');
		$this->db->join('users', 'users.link3 = shareholders.Id');
		$this->db->where('shareholders.Id', $Id);
		$query = $this->db->get();
		foreach ($query->result() as $row) {
			return $row->password;
		}
	}

	public function getUserId3($Id) {
		$this->db->select('*');
		$this->db->from('shareholderss');
		$this->db->join('users', 'users.link3 = shareholders.Id');
		$this->db->where('shareholders.Id', $Id);
		$query = $this->db->get();
		foreach ($query->result() as $row) {
			return $row->id;
		}
	}

	public function getStaffIdnumber() {
		$username = $this->input->post("username");
		$passcode = md5($this->input->post("password"));
		$this->db->select('*');
		$this->db->from('staff');
		$this->db->join('users', 'users.link2 = staff.Id');
		$this->db->where('username', $username);
		$this->db->where('password', $passcode);
		$query = $this->db->get();
		foreach ($query->result() as $row) {
			return $row->WorkNumber;
		}
	}

	public function getStaffphone() {
		$username = $this->input->post("username");
		$passcode = md5($this->input->post("password"));
		$this->db->select('*');
		$this->db->from('staff');
		$this->db->join('users', 'users.link2 = staff.Id');
		$this->db->where('username', $username);
		$this->db->where('password', $passcode);
		$query = $this->db->get();
		foreach ($query->result() as $row) {
			return $row->Mobile;
		}
	}

	public function getAgentName() {
		$username = $this->input->post("username");
		$passcode = md5($this->input->post("password"));
		$this->db->select('*');
		$this->db->from('agents');
		$this->db->join('users', 'users.link = agents.Id');
		$this->db->where('username', $username);
		$this->db->where('password', $passcode);
		$query = $this->db->get();
		foreach ($query->result() as $row) {
			return $row->AgName;
		}
	}

	public function getAgentId() {
		$username = $this->input->post("username");
		$passcode = md5($this->input->post("password"));
		$this->db->select('*');
		$this->db->from('agents');
		$this->db->join('users', 'users.link = agents.Id');
		$this->db->where('username', $username);
		$this->db->where('password', $passcode);
		$query = $this->db->get();
		foreach ($query->result() as $row) {
			return $row->Id;
		}
	}

	public function getAgentIdnumber() {
		$username = $this->input->post("username");
		$passcode = md5($this->input->post("password"));
		$this->db->select('*');
		$this->db->from('agents');
		$this->db->join('users', 'users.link = agents.Id');
		$this->db->where('username', $username);
		$this->db->where('password', $passcode);
		$query = $this->db->get();
		foreach ($query->result() as $row) {
			return $row->Number;
		}
	}

	public function getAgentPhone() {
		$username = $this->input->post("username");
		$passcode = md5($this->input->post("password"));
		$this->db->select('*');
		$this->db->from('agents');
		$this->db->join('users', 'users.link = agents.Id');
		$this->db->where('username', $username);
		$this->db->where('password', $passcode);
		$query = $this->db->get();
		foreach ($query->result() as $row) {
			return $row->Telephone;
		}
	}

	public function getShareholdersName() {
		$username = $this->input->post("username");
		$passcode = md5($this->input->post("password"));
		$this->db->select('*');
		$this->db->from('shareholders');
		$this->db->join('users', 'users.link3 = shareholders.Id');
		$this->db->where('username', $username);
		$this->db->where('password', $passcode);
		$query = $this->db->get();
		foreach ($query->result() as $row) {
			return $row->Name;
		}
	}

	public function getShareholdersId() {
		$username = $this->input->post("username");
		$passcode = md5($this->input->post("password"));
		$this->db->select('*');
		$this->db->from('shareholders');
		$this->db->join('users', 'users.link3 = shareholders.Id');
		$this->db->where('username', $username);
		$this->db->where('password', $passcode);
		$query = $this->db->get();
		foreach ($query->result() as $row) {
			return $row->Id;
		}
	}

	public function change($index) {
		if (isset($index['id'])) {
			$this->db->where('id', $index['id']);
			$this->db->update('users', $index);
		}
	}

	public function changeUserAvatar($data) {
		$user = $this->session->userdata('username');
		$sql = "UPDATE users SET avatar = '" . $data . "' where users.username = '" . $user . "'";
		$this->db->query($sql);
	}

}
