<?php if (!defined('BASEPATH')) {
	exit('No direct script access allowed');
}

class Redirect extends CI_Controller {
	private $request_id;

	public function __construct() {
		parent::__construct();
		$this->load->model('shares/shares_model', 'shares');
		$this->load->model('settings/settings_model', 'settings');
	}

	//function loads the user page on admin dashboard

	public function index() {

		$this->load->library('session');

		$this->load->helper('url');
		if ($this->session->userdata('logged_in')) {
			if ($this->session->userdata('sysinfo') <= 0) {
				$data['success'] = ("");
				$data['error'] = ("");
				$data['mainContent'] = 'settings/systemInfo';
				$data['title'] = "Add System Infomation";
				$result = $this->settings->editSystemInfo();
				$data['view_data'] = $result;
				$this->load->view('Admin/redirect2', $data);
			} elseif ($this->session->userdata('sharetypes') <= 0) {
				$data['success'] = ("");
				$data['error'] = ("");
				$data['mainContent'] = 'settings/addShareTypes';
				$data['title'] = "Add Share Series";
				$data['view_data'] = '';

				$this->load->view('Admin/redirect2', $data);
			} else {
				if ($this->session->userdata('role') == "Admin") {
					$data['mainContent'] = 'Admin/home';
				} elseif ($this->session->userdata('role') == "Admin2") {
					$data['sharesum'] = $this->shares->getSum();
					$data['mainContent'] = 'Admin/home';
				} elseif ($this->session->userdata('role') == "Agent") {
					$data['mainContent'] = 'Admin/home';
				} elseif ($this->session->userdata('role') == "Finance") {
					$data['mainContent'] = 'Admin/home';
				} elseif ($this->session->userdata('role') == "Shareholder") {
					$data['mainContent'] = 'Admin/shareholderHome';
				} else {
					redirect('systemUsers/accessDenied');
				}
				$sharesum = $this->shares->getSum();
				$paidsum = $this->shares->getPaidSum();
				$price = $this->shares->totalPrice();
				$expected = $this->shares->getAmountExpected();
				$subexpected = $this->shares->getAmountSubExpected();
				$sharesold = $this->shares->getSoldSum();
				$totalReg = $this->shares->totalReg();
				/*$subtotal = $sharesold * $price;*/
				$avail = ($sharesum - $sharesold);
				$notpaid = ($subexpected - $paidsum);
				$data['sharesum'] = $sharesum;
				$data['sharesold'] = $sharesold;
				$data['expected'] = $expected;
				$data['paidsum'] = $paidsum;
				$data['subtotal'] = $subexpected;
				$data['notpaid'] = $notpaid;
				$data['totalReg'] = $totalReg;
				$data['avail'] = $avail;
				$result = $this->shares->getShareholders();
				$data['shareholders'] = $result['row_count'];
				$agents = $this->shares->getAgents();
				$data['agents'] = $agents['row_count'];
				$data['title'] = "Main Panel";
				$this->load->view('Admin/adminRedirect', $data);

			}
		} else {
			redirect('systemUsers/accessDenied');
		}

	}

	public function dashboard() {
		$this->load->library('session');

		$this->load->helper('url');
		if ($this->session->userdata('logged_in')) {
			$data['mainContent'] = 'Admin/adminHome';
			$sharesum = $this->shares->getSum();
			$paidsum = $this->shares->getPaidSum();
			$price = $this->shares->totalPrice();
			$expected = $this->shares->getAmountExpected();
			$subexpected = $this->shares->getAmountSubExpected();
			$sharesold = $this->shares->getSoldSum();
			$totalReg = $this->shares->totalReg();
			/*$subtotal = $sharesold * $price;*/
			$avail = ($sharesum - $sharesold);
			$notpaid = ($subexpected - $paidsum);
			$data['sharesum'] = $sharesum;
			$data['sharesold'] = $sharesold;
			$data['expected'] = $expected;
			$data['paidsum'] = $paidsum;
			$data['subtotal'] = $subexpected;
			$data['notpaid'] = $notpaid;
			$data['totalReg'] = $totalReg;
			$data['avail'] = $avail;
			$result = $this->shares->getShareholders();
			$data['shareholders'] = $result['row_count'];
			$agents = $this->shares->getAgents();
			$data['agents'] = $agents['row_count'];
			$data['title'] = "Main Panel";
			$this->load->view('Admin/adminRedirect', $data);
		} else {
			redirect('systemUsers/accessDenied');
		}
	}
}
