<!-- /. NAV SIDE  -->
<div id="page-wrapper">
	<div id="page-inner">
		<!--BEGIN TITLE & BREADCRUMB PAGE-->
		<div id="title-breadcrumb-option-demo" class="page-title-breadcrumb">
			<div class="page-header pull-left">
				<div class="page-title">
					Shares Management
				</div>
			</div>
			<ol class="breadcrumb page-breadcrumb pull-right">
				<li><i class="fa fa-home"></i>&nbsp;<a href="<?php echo base_url() . "home" ?>">Home</a>&nbsp;&nbsp;
				</li>
				<li><i class="fa fa-stack-exchange"></i><a href="<?php echo base_url() . 'shares/dashboard'
					?>">&nbsp;&nbsp;Shares Management</a></li>
				<li class="active"><i class="fa fa-edit"></i>&nbsp;&nbsp;Edit Bought Shares</li>
			</ol>
			<div class="clearfix">
			</div>
		</div>
		<!--END TITLE & BREADCRUMB PAGE-->
		<div class="row">
			<div class="col-md-12">
				<div class="alert alert-info">
					<i class="fa fa-info-circle"></i>
					<strong>Heads up!</strong>
					This helps in editing shares bought for existing shareholders!
				</div>
				<br/>
				<?php if (strlen($success) > 0) {
					?>
					<div class="alert alert-success" id="success"><i class="fa fa-check"></i>&nbsp;<?php echo $success
							. ''; ?>
					</div>
					<?php
				}
				?>
				<?php if (strlen($error) > 0) {
					?>
					<div class="alert alert-danger" id="error"><i class="fa fa-ban"></i>&nbsp;<?php echo $error . ''; ?>
					</div>
					<?php
				}
				?>
			</div>
		</div>
		<!-- /. ROW  -->
		<div class="row">
			<div class="col-md-12">
				<!-- Advanced Tables -->
				<?php if (($this->session->userdata('role') == "Admin") OR ($this->session->userdata('role') == "Finance")
				OR ($this->session->userdata('role') == "Admin2")) { ?>
				<div class="panel panel-pink">
					<?php } ?>
					<?php if (($this->session->userdata('role') == "Agent")) { ?>
					<div class="panel panel-green">
						<?php } ?>
						<div class="panel-heading">
							<div class="row">
								<div class="col-sm-6">
									<h3>Edit Shares</h3>
								</div>
							</div>
						</div>
						<div class="panel-body">
							<?php $this->load->helper('form'); ?>
							<?php echo form_open('shares/update'); ?>
							<div class="form-body pal">
								<div class="row">
									<div class="col-md-4">
										<div class="form-group">
											<label>Share Types</label>
											<select class="form-control" id="type" name="type">
												<option value="0">Please Select One....</option>
												<?php foreach ($view_data1 as $data): ?>
													<option
														value="<?php echo $data->Id; ?>"><?php echo $data->type; ?></option>
												<?php endforeach; ?>
											</select>
										</div>
									</div>
									<div class="col-md-4">
										<div class="form-group">
											<label>Number of Shares</label>
											<?php echo form_input(array("class" => "form-control",
												"placeholder" => "Number of shares", "name" => "shares",
												"value" => $view_data['shareNumber'], "required" => "true")) ?>
										</div>
									</div>
									<div class="col-md-4">
										<label>Expected Amount</label>
										<?php echo form_input(array("class" => "form-control",
											"placeholder" => "Number of shares", "name" => "expected",
											"value" => $view_data['expectedAmount'], "readonly" => "true")) ?>
									</div>
								</div>
								<div class="row">
									<div class="col-md-12">
										<div class="form-group">
											<label>Physical Form Number</label>
											<?php echo form_input(array("class" => "form-control",
												"placeholder" => "Physical Form Number", "name" => "fnumber",
												"required" => "true", "type" => "text",
												"value" => $view_data['physicalFormNumber'],)) ?>
										</div>
									</div>
								</div>
								<div class="row">
									<div class="col-md-4">
										<div class="form-group">
											<label>Receipt Number</label>
											<?php echo form_input(array("class" => "form-control",
												"placeholder" => "Number of shares", "name" => "receipt",
												"value" => $view_data['Receiptnumber'], "readonly" => "true")) ?>
										</div>
									</div>
									<div class="col-md-4">
										<div class="form-group">
											<label>Share Type</label>
											<?php echo form_input(array("class" => "form-control",
												"placeholder" => "Number of shares", "name" => "sharetypeName",
												"value" => $view_data['shareTypeName'], "readonly" => "true")) ?>
										</div>
									</div>
									<div class="col-md-4">
										<label>Date Bought</label>
										<?php echo form_input(array("class" => "form-control",
											"placeholder" => "Number of shares", "name" => "datebought",
											"value" => $view_data['datebought'], "readonly" => "true")) ?>
									</div>
								</div>
								<div class="row">
									<div class="col-md-4">
										<div class="form-group">
											<?php if ($view_data != null) { ?>
												<label>Share Holder Name</label>
												<?php echo form_input(array("class" => "form-control",
													"placeholder" => "Shareholder", "name" => "holder",
													"readonly" => "true", "value" => $view_data['Name'])) ?>
											<?php } ?>
										</div>
									</div>
									<div class="col-md-4">
										<div class="form-group">
											<?php if ($view_data != null) { ?>
												<label>Share Holder Id Number / KRA pin</label>
												<?php echo form_input(array("class" => "form-control",
													"placeholder" => "Shareholder", "name" => "idnumber",
													"readonly" => "true", "value" => $view_data['idnumber'])) ?>
											<?php } ?>
										</div>
									</div>
									<div class="col-md-4">
										<div class="form-group">
											<label>Added By</label>
											<?php echo form_input(array("class" => "form-control",
												"placeholder" => "Location", "name" => "addedby", "readonly" => "true",
												"value" => $view_data['AddedBy'])) ?>
										</div>
									</div>
								</div>
								<div class="row">
									<div class="col-md-4">
										<div class="form-group">
											<div class="form-group">
												<label>Updated By</label>
												<?php echo form_input(array("class" => "form-control",
													"placeholder" => "Location", "name" => "Updatedby",
													"readonly" => "true",
													"value" => $this->session->userdata('name'))) ?>
											</div>
										</div>
									</div>
									<div class="col-md-4">
										<div class="form-group">
											<?php if ($view_data != null) { ?>
												<?php echo form_hidden('buyId', $view_data['buyId'],
													'class="form-control"'); ?>
											<?php } ?>
											<?php if ($view_data != null) { ?>
												<?php echo form_hidden('Id', $view_data['Id'], 'class="form-control"'); ?>
											<?php } ?>
											<?php if ($view_data != null) { ?>
												<?php echo form_hidden('dateadded', $view_data['dateadded'],
													'class="form-control"'); ?>
											<?php } ?>
											<?php if ($view_data != null) { ?>
												<?php echo form_hidden('sharenumber', $view_data['shareNumber'],
													'class="form-control"'); ?>
											<?php } ?>
										</div>
									</div>
									<div class="col-md-4">
										<div class="form-group">
											<button type="reset" class="btn btn-danger pull-right">Cancel</button>
											<?php if ($view_data != null) { ?>
												<?php echo form_submit('edit', 'Edit Shares',
													'class="btn btn-success pull-right margin-right"'); ?>
											<?php } ?>
										</div>
									</div>
								</div>
							</div>
							</form>
							<!-- /.row (nested) -->
						</div>
						<!-- /.panel-body -->
					</div>
					<!-- /.panel -->
				</div>
				<!-- /.col-lg-12 -->
			</div>
