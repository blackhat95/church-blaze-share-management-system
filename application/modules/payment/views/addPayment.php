<!-- /. NAV SIDE  -->
<div id="page-wrapper">
	<div id="page-inner">
		<!--BEGIN TITLE & BREADCRUMB PAGE-->
		<div id="title-breadcrumb-option-demo" class="page-title-breadcrumb">
			<div class="page-header pull-left">
				<div class="page-title">
					Payment Management
				</div>
			</div>
			<ol class="breadcrumb page-breadcrumb pull-right">
				<li><i class="fa fa-home"></i>&nbsp;<a href="<?php echo base_url() . "home" ?>">Home</a>&nbsp;&nbsp;
				</li>
				<li><i class="fa fa-money"></i><a href='<?php echo base_url() . 'payment/paymentHome' ?>'>&nbsp;&nbsp;Payment
						Management</a></li>
				<li class="active"><i class="fa fa-plus"></i>&nbsp;&nbsp;Add Payments</li>
			</ol>
			<div class="clearfix">
			</div>
		</div>
		<!--END TITLE & BREADCRUMB PAGE-->
		<div class="row">
			<div class="col-md-12">
				<div class="alert alert-info">
					<i class="fa fa-info-circle"></i>
					<strong>Heads up!</strong>
					This helps in buying shares for existing shareholders!
				</div>
				<br/>
				<?php if (strlen($success) > 0) {
					?>
					<div class="alert alert-success" id="success"><i class="fa fa-check"></i>&nbsp;<?php echo $success
							. ''; ?>
					</div>
					<?php
				}
				?>
				<?php if (strlen($error) > 0) {
					?>
					<div class="alert alert-danger" id="error"><i class="fa fa-ban"></i>&nbsp;<?php echo $error . ''; ?>
					</div>
					<?php
				}
				?>
			</div>
		</div>
		<!-- /. ROW  -->
		<div class="row">
			<div class="col-md-12">
				<!-- Advanced Tables -->
				<?php if (($this->session->userdata('role') == "Admin") OR ($this->session->userdata('role') == "Finance")
				OR ($this->session->userdata('role') == "Admin2")) { ?>
				<div class="panel panel-pink">
					<?php } ?>
					<?php if (($this->session->userdata('role') == "Agent")) { ?>
					<div class="panel panel-green">
						<?php } ?>
						<div class="panel-heading">
							<div class="row">
								<div class="col-sm-6">
									<h3>Share Holder Search</h3>
								</div>
							</div>
						</div>
						<div class="panel-body">
							<?php $this->load->helper('form'); ?>
							<?php echo form_open('payment/receiptSearch'); ?>
							<div class="form-body pal">
								<div class="row">
									<div class="col-md-3">
										<div class="form-group">
											<label>Enter the the buy receipt number</label>
										</div>
									</div>
									<div class="col-md-7">
										<div class="form-group">
											<?php echo form_input(array("class" => "form-control",
												"placeholder" => "Enter receipt number", "name" => "receipt",
												"required" => "true")) ?>
										</div>
									</div>
									<div class="col-md-2">
										<div class="form-group">
											<?php echo form_submit('save', 'Search',
												'class="btn btn-success pull-left margin-right"'); ?>

										</div>
									</div>
								</div>
							</div>
							</form>
							<!-- /.row (nested) -->
						</div>
					</div>
					<!-- /.panel-body -->
				</div>
				<!-- /.panel -->
			</div>
			<!-- /.col-lg-12 -->
		</div>
		<br/>
		<!-- /. ROW  -->
		<div class="row">
			<div class="col-md-12">
				<!-- Advanced Tables -->
				<?php if (($this->session->userdata('role') == "Admin") OR ($this->session->userdata('role') == "Finance")
				OR ($this->session->userdata('role') == "Admin2")) { ?>
				<div class="panel panel-pink">
					<?php } ?>
					<?php if (($this->session->userdata('role') == "Agent")) { ?>
					<div class="panel panel-green">
						<?php } ?>
						<div class="panel-heading">
							<div class="row">
								<div class="col-sm-6">
									<h3>Add Share Payment</h3>
								</div>
							</div>
						</div>
						<div class="panel-body">
							<?php $this->load->helper('form'); ?>
							<?php echo form_open('payment/save'); ?>
							<div class="form-body pal">
								<div class="row">
									<div class="col-md-4">
										<div class="form-group">
											<label>Payment Type</label>
											<select class="form-control" id="type" name="type">
												<option value="choose">Please Select One....</option>
												<option value="M-Pesa">M-Pesa</option>
												<option value="Cash">Cash</option>
												<option value="Cheque">Cheque</option>
												<option value="Bank Deposit">Bank Deposit</option>

											</select>
										</div>
									</div>
									<div class="col-md-4">
										<div class="form-group">
											<label>Amount of Shares Paid For</label>
											<?php echo form_input(array("class" => "form-control",
												"placeholder" => "Number of shares", "name" => "shares",
												"required" => "true")) ?>
										</div>
									</div>
									<div class="col-md-4">
										<div class="form-group">
											<label>Amount Paid</label>
											<?php echo form_input(array("class" => "form-control",
												"placeholder" => "Amount Paid", "name" => "paid",
												"required" => "true")) ?>
										</div>
									</div>
								</div>
								<div class="row">
									<div class="col-md-4">
										<div class="form-group">
											<label>Payment Code</label>
											<?php echo form_input(array("class" => "form-control",
												"placeholder" => "Payment Code", "name" => "code")) ?>
										</div>
									</div>
									<div class="col-md-4">
										<div class="form-group">
											<label>Physical Receipt Number</label>
											<?php echo form_input(array("class" => "form-control",
												"placeholder" => "Physical Receipt Number", "name" => "fnumber",
												"required" => "true", "type" => "text")) ?>
										</div>
									</div>
									<div class="col-md-4">
										<div class="form-group">
											<label>Receiver Name</label>
											<?php echo form_input(array("class" => "form-control",
												"placeholder" => "Receiver Name", "name" => "receiver",
												"readonly" => "true", "value" => $this->session->userdata('name'))) ?>
										</div>
									</div>
								</div>
								<div class="row">
									<div class="col-md-4">
										<?php if ($sharetypes != null) { ?>
											<label>Share Type</label>
											<?php echo form_input(array("class" => "form-control",
												"placeholder" => "Share Types", "name" => "typename",
												"readonly" => "true", "value" => $sharetypes)) ?>
										<?php } ?>

									</div>
									<div class="col-md-4">
										<div class="form-group">
											<?php if ($holders != null) { ?>
												<label>Shareholder's Name</label>
												<?php echo form_input(array("class" => "form-control",
													"placeholder" => "Shareholder's Name", "name" => "holdername",
													"readonly" => "true", "value" => $holders)) ?>
											<?php } ?>

										</div>
									</div>
									<div class="col-md-4">
										<?php if ($view_data != null) { ?>
											<label>Shares Bought</label>
											<?php echo form_input(array("class" => "form-control",
												"placeholder" => "Shares Bought", "name" => "bought",
												"readonly" => "true", "value" => $view_data['shareNumber'])) ?>
										<?php } ?>
									</div>
									<div class="col-md-4">
									</div>
								</div>
								<div class="row">
									<div class="col-md-6">
										<?php if ($view_data != null) { ?>
											<?php echo form_hidden('buyId', $view_data['buyId'], 'class="form-control"'); ?>
										<?php } ?>
										<?php if ($idnumber != null) { ?>
											<?php echo form_hidden('idnumber', $idnumber, 'class="form-control"'); ?>
										<?php } ?>
										<?php if ($view_data != null) { ?>
											<?php echo form_hidden('sharetype', $view_data['sharetype'],
												'class="form-control"'); ?>
										<?php } ?>
										<?php if ($view_data != null) { ?>
											<?php echo form_hidden('shareholder', $view_data['shareholder'],
												'class="form-control"'); ?>
										<?php } ?>
										<?php if ($shareprice != null) { ?>
											<?php echo form_hidden('price', $shareprice, 'class="form-control"'); ?>
										<?php } ?>
										<?php if ($amountreceived != null) { ?>
											<?php echo form_hidden('amountreceived', $amountreceived,
												'class="form-control"'); ?>
										<?php } ?>
									</div>
									<div class="col-md-6">
										<div class="form-group">
										</div>
									</div>
								</div>
								<div class="row">
									<div class="col-md-4">
										<div class="form-group">
											<?php if ($view_data != null) { ?>
												<?php echo form_hidden('Receiptnumber', $view_data['Receiptnumber'],
													'class="form-control"'); ?>
											<?php } ?>
										</div>
									</div>
									<div class="col-md-4">
										<div class="form-group">
										</div>
									</div>
									<div class="col-md-4">
										<div class="form-group">
											<button type="reset" class="btn btn-danger pull-right">Cancel</button>
											<?php if ($view_data != null) { ?>
												<?php echo form_submit('save', 'Add Payment',
													'class="btn btn-success pull-right margin-right"'); ?>
											<?php } ?>
										</div>
									</div>
								</div>
							</div>
							</form>
							<!-- /.row (nested) -->
						</div>
						<!-- /.panel-body -->
					</div>
					<!-- /.panel -->
				</div>
				<!-- /.col-lg-12 -->
			</div>
