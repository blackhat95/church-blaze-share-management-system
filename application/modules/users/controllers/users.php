<?php if (!defined('BASEPATH')) {
	exit('No direct script access allowed');
}

class Users extends MX_Controller
{

	public function __construct()
	{
		parent::__construct();
		$this->load->model('users/users_model', 'users');
		$this->load->library('session');
	}

	public function index($sort_by = 'username', $sort_order = 'asc', $offset = 0)
	{
		if ($this->session->userdata('role') == "Admin") {
			if ($this->session->userdata('logged_in')) {
				$limit = 15;
				$data['fields'] = array("username" => "Username", "status" => "Status", "role" => "Role",
					"datecreated" => "Date Created");
				$data['headers'] = array("username" => "Username", "status" => "Status", "role" => "Role",
					"datecreated" => "Date Created");
				$result = $this->users->get($limit, $offset, $sort_by, $sort_order);
				$index = null;
				$result2 = $this->users->getsearch($index);
				$data['view_data2'] = $result2['rows'];
				$data['success'] = ("");
				$data['error'] = ("");
				$data['info'] = ("");
				$data['mainContent'] = 'usersView';
				$data['title'] = "System Users";
				$data['view_data'] = $result['rows'];
				$data['rownumber'] = $result['row_count'];
				$data['info'] = ("System Users Loaded Successfully!");

				//pagination
				$this->load->library('pagination');
				$config = array();
				$config['base_url'] = site_url("users/index/$sort_by/$sort_order/");
				$config['total_rows'] = $data['rownumber'];
				$config['per_page'] = $limit;
				$config['uri_segment'] = 15;
				$config['first_link'] = 'First';
				$config['last_link'] = 'Last';
				$this->pagination->initialize($config);
				$data['pagination'] = $this->pagination->create_links();

				$data['sort_by'] = $sort_by;
				$data['sort_order'] = $sort_order;
				$this->load->view('Admin/adminRedirect', $data);
				log_message('info', 'User:- ' . $this->session->userdata('name') . ' ' . ' viewed system users');
			} else {
				redirect('systemUsers/accessDenied');
			}
		} else {
			redirect('systemUsers/accessDenied');
		}

	}

	public function search($sort_by = 'Id', $sort_order = 'asc', $offset = 0)
	{
		if ($this->session->userdata('role') == "Admin") {
			if ($this->session->userdata('logged_in')) {
				$data['success'] = ("");
				$data['error'] = ("");
				$limit = 15;
				$data['fields'] = array("username" => "Username", "status" => "Status", "role" => "Role",
					"datecreated" => "Date Created");
				$data['headers'] = array("username" => "Username", "status" => "Status", "role" => "Role",
					"datecreated" => "Date Created");

				$result = $this->users->get($limit, $offset, $sort_by, $sort_order);
				$index = $this->input->post('search');
				$result2 = $this->users->getsearch($index);
				$data['success'] = ("");
				$data['error'] = ("");
				$data['info'] = ("");
				$data['mainContent'] = 'usersView';
				$data['title'] = "System Users";
				$data['view_data'] = $result['rows'];
				$data['view_data2'] = $result2['rows'];
				$data['rownumber'] = $result['row_count'];
				if ($result2['rows'] == null) {
					$data['error'] = ("No user with specified username was found");
				} else {
					$data['success'] = ("User Loaded Successfully!");
				}
				//pagination
				$this->load->library('pagination');
				$config = array();
				$config['base_url'] = site_url("users/search/$sort_by/$sort_order/");
				$config['total_rows'] = $data['rownumber'];
				$config['per_page'] = $limit;
				$config['uri_segment'] = 15;
				$this->pagination->initialize($config);
				$data['pagination'] = $this->pagination->create_links();

				$data['sort_by'] = $sort_by;
				$data['sort_order'] = $sort_order;

				$this->load->view('Admin/adminRedirect', $data);
				$this->load->view('Admin/includes/datatables');;
				log_message('info', 'User:- ' . $this->session->userdata('name') . ' ' . ' Search for system user '. ' '. $index);
			} else {
				redirect('systemUsers/accessDenied');
			}
		} else {
			redirect('systemUsers/accessDenied');
		}
	}

	public function retire($id = null)
	{
		if ($this->session->userdata('role') == "Admin") {
			if ($this->session->userdata('logged_in')) {
				if ($id == null) {
					show_error('No shit found', 500);
				} else {
					$index['success'] = ("");
					$index['error'] = ("");
					$index['info'] = ("");
					$index['mainContent'] = 'retireUser';
					$index['title'] = "Retire User";
					$index['view_data'] = $this->users->edit($id);
					$role = $this->users->getrole($id);
					if ($role == 'Admin' OR $role == 'Admin2' OR $role == 'Finance') {
						$name = $this->users->getStaff($id, $role);
					} elseif ($role == 'Agent') {
						$name = $this->users->getAgent($id, $role);
					} else {
						$name = $this->users->getShareholder($id, $role);
					}
					$index['name'] = $name;
					$this->load->view('Admin/adminRedirect', $index);
				}
			} else {
				redirect('systemUsers/accessDenied');
			}
		} else {
			redirect('systemUsers/accessDenied');
		}

	}

	public function updateStatus()
	{
		if ($this->session->userdata('role') == "Admin") {
			if ($this->session->userdata('logged_in')) {
				$data['success'] = ("");
				$data['error'] = ("");
				$data['info'] = ("");
				$role = $this->input->post('role');
				$id = $this->input->post('id');
				$status = $this->input->post('status');
				if (isset($_POST) && $_POST['updateStatus'] == 'Save') {
					$index['id'] = $id;
					$index['username'] = $this->input->post('username');
					$index['password'] = $this->input->post('password');
					$index['role'] = $role;
					$index['status'] = $status;
					$index['datecreated'] = $this->input->post('datecreated');
					$this->users->add($index);
					if ($status == 1) {
						$data['success'] = ("User Activated Successfully");
					} else {
						$data['success'] = ("User Retired Successfully");
					}
					$data['mainContent'] = 'retireUser';
					$data['title'] = "System Users";
					$data['view_data'] = $this->users->edit($id);
					if ($role == 'Admin' OR $role == 'Admin2' OR $role == 'Finance') {
						$name = $this->users->getStaff($id, $role);
					} elseif ($role == 'Agent') {
						$name = $this->users->getAgent($id, $role);
					} else {
						$name = $this->users->getShareholder($id, $role);
					}
					$data['name'] = $name;
					$this->load->view('Admin/adminRedirect', $data);
					log_message('info', 'User:- ' . $this->session->userdata('name') . ' ' . ' retired/activated system user '. $this->input->post('username'));
				} else {
					$data['error'] = ("User was not Retired! Please Contact System Admin");
					log_message('info', 'User:- ' . $this->session->userdata('name') . ' ' . ' could not retire/activate system User.');
					$data['mainContent'] = 'retireUser';
					$data['title'] = "System Users";
					$data['view_data'] = $this->users->edit($id);
					if ($role == 'Admin' OR $role == 'Admin2' OR $role == 'Finance') {
						$name = $this->users->getStaff($id, $role);
					} elseif ($role == 'Agent') {
						$name = $this->users->getAgent($id, $role);
					} else {
						$name = $this->users->getShareholder($id, $role);
					}
					$data['name'] = $name;
					$this->load->view('Admin/adminRedirect', $data);
				}
			} else {
				redirect('systemUsers/accessDenied');
			}
		} else {
			redirect('systemUsers/accessDenied');
		}

	}

	public function dashboard()
	{
		if ($this->session->userdata('role') == "Admin") {
			if ($this->session->userdata('logged_in')) {
				$data['success'] = ("");
				$data['error'] = ("");
				$data['mainContent'] = 'staff/staffDashboard';
				$data['title'] = "Staff & Users Dashboard";
				$data['view_data'] = '';

				$this->load->view('Admin/adminRedirect', $data);
			} else {
				redirect('systemUsers/accessDenied');
			}
		} else {
			redirect('systemUsers/accessDenied');
		}

	}
}
