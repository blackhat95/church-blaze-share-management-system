<!-- /. NAV SIDE  -->
<div id="page-wrapper">
	<div id="page-inner">
		<!--BEGIN TITLE & BREADCRUMB PAGE-->
		<div id="title-breadcrumb-option-demo" class="page-title-breadcrumb">
			<div class="page-header pull-left">
				<div class="page-title">
					Shareholders Management
				</div>
			</div>
			<ol class="breadcrumb page-breadcrumb pull-right">
				<li><i class="fa fa-home"></i>&nbsp;<a href="<?php echo base_url() . "home" ?>">Home</a>&nbsp;&nbsp;
				</li>
				<li><i class="fa fa-users"></i>&nbsp;&nbsp;Shareholders</li>
			</ol>
			<div class="clearfix">
			</div>
		</div>
		<!--END TITLE & BREADCRUMB PAGE-->
		<div class="row">
			<div class="alert alert-info">
				<i class="fa fa-info-circle"></i>
				<strong>Heads up!</strong>
				Allows you to choose the kind of share holders to Add OR View!
			</div>
			<hr/>
			<ul class="nav nav-tabs nav-justified">
				<li class="active"><a data-toggle="tab" href="#IndividualShareholders"><h5><i class="fa fa-user"></i>&nbsp;Individual
							Share
							Holders</h5></a></li>
				<li><a data-toggle="tab" href="#CompanyShareholders"><h5><i class="fa fa-users"></i>&nbsp;Company Share
							Holders</h5></a>
				</li>
				<li><a data-toggle="tab" href="#nextofKins"><h5><i class="fa fa-user-md"></i>&nbsp;Next of
							Kins</h5></a>
				</li>
			</ul>
			<div class="tab-content">
				<div id="IndividualShareholders" class="tab-pane fade in active">
					<hr/>
					<div class="col-md-6 col-sm-12 col-xs-12">
						<div class="panel panel-primary text-center no-boder bg-color-green">
							<div class="panel-body">

								<a href="<?php echo base_url() . "shareholders/addShareholders" ?>"><i
										class="fa fa-user fa-10x bg-color-green"></i></a>

								<h3></h3>
							</div>
							<div class="panel-footer back-footer-green">
								<a class="white" href="<?php echo base_url() . "shareholders/addShareholders" ?>">Add
									Individual Shareholders</a>
							</div>
						</div>
					</div>
					<?php
					if (($this->session->userdata('role') == 'Admin') OR ($this->session->userdata('role') == 'Admin2')
						OR ($this->session->userdata('role') == 'Finance')
					) {
						?>
						<div class="col-md-6 col-sm-12 col-xs-12">
							<div class="panel panel-primary text-center no-boder bg-color-blue">
								<div class="panel-body">
									<a href="<?php echo base_url() . "shareholders/viewShareholders" ?>"><i
											class="fa fa-edit fa-10x bg-color-blue"></i></a>

									<h3></h3>
								</div>
								<div class="panel-footer back-footer-blue">
									<a class="white"
									   href="<?php echo base_url() . "shareholders/viewShareholders" ?>">View
										Individual Shareholders</a>
								</div>
							</div>
						</div>
					<?php } ?>
				</div>
				<div id="CompanyShareholders" class="tab-pane fade">
					<hr/>
					<div class="col-md-6 col-sm-12 col-xs-12">
						<div class="panel panel-primary text-center no-boder bg-color-brown">
							<div class="panel-body">
								<a href="<?php echo base_url() . "shareholders/addCompany" ?>"><i
										class="fa fa-users fa-10x bg-color-brown"></i></a>

								<h3></h3>
							</div>
							<div class="panel-footer back-footer-brown">
								<a class="white" href="<?php echo base_url() . "shareholders/addCompany" ?>">Add
									Company
									Shareholders</a>
							</div>
						</div>
					</div>
					<?php
					if (($this->session->userdata('role') == 'Admin') OR ($this->session->userdata('role') == 'Admin2')
						OR ($this->session->userdata('role') == 'Finance')
					) {
						?>
						<div class="col-md-6 col-sm-12 col-xs-12">
							<div class="panel panel-primary text-center no-boder bg-color-TribalTriumph">
								<div class="panel-body">
									<a href="<?php echo base_url() . "shareholders/viewCompanyShareholders" ?>"><i
											class="fa fa-edit fa-10x bg-color-TribalTriumph"></i></a>

									<h3></h3>
								</div>
								<div class="panel-footer back-footer-TribalTriumph">
									<a class="white"
									   href="<?php echo base_url() . "shareholders/viewCompanyShareholders" ?>">View
										Company Shareholders</a>
								</div>
							</div>
						</div>
					<?php } ?>
				</div>
				<div id="nextofKins" class="tab-pane fade in">
					<hr/>
					<div class="col-md-6 col-sm-12 col-xs-12">
						<div class="panel panel-primary text-center no-boder bg-color-green">
							<div class="panel-body">

								<a href='<?php echo base_url() . 'shareholders/addNextofkin' ?>'><i
										class="fa fa-user fa-10x bg-color-green"></i></a>

								<h3></h3>
							</div>
							<div class="panel-footer back-footer-green">
								<a class="white" href='<?php echo base_url() . 'shareholders/addNextofkin' ?>'>Add
									Next Of Kins</a>
							</div>
						</div>
					</div>
					<?php
					if (($this->session->userdata('role') == 'Admin') OR ($this->session->userdata('role') == 'Admin2')
						OR ($this->session->userdata('role') == 'Finance')
					) {
						?>
						<div class="col-md-6 col-sm-12 col-xs-12">
							<div class="panel panel-primary text-center no-boder bg-color-blue">
								<div class="panel-body">
									<a href='<?php echo base_url() . 'shareholders/viewNextofkin' ?>'><i
											class="fa fa-edit fa-10x bg-color-blue"></i></a>

									<h3></h3>
								</div>
								<div class="panel-footer back-footer-blue">
									<a class="white"
									   href='<?php echo base_url() . 'shareholders/viewNextofkin' ?>'>View
										Next of Kins</a>
								</div>
							</div>
						</div>
					<?php } ?>
				</div>
			</div>
		</div>
	</div>



