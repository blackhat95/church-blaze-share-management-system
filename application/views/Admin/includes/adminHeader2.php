<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<meta charset="utf-8"/>
	<meta name="viewport" content="width=device-width, initial-scale=1.0"/>
	<link rel="shortcut icon" href="<?php echo base_url( '/assets/img/favicon.ico' ) ?>" type="image/x-icon">
	<link rel="icon" href="<?php echo base_url( '/assets/img/favicon.ico' ) ?>" type="image/x-icon">
	<title>{CBSS} | <?php echo $title; ?></title>
	<!-- Bootstrap Styles-->
	<link href="<?php echo base_url(); ?>assets/css/bootstrap.css" rel="stylesheet"/>
	<link href="<?php echo base_url(); ?>assets/css/bootstrap-datepicker.css" rel="stylesheet"/>
	<link href="<?php echo base_url(); ?>assets/css/login.css" rel="stylesheet"/>
	<!-- FontAwesome Styles-->
	<link href="<?php echo base_url(); ?>assets/css/font-awesome.css" rel="stylesheet"/>
	<!-- Morris Chart Styles-->
	<link href="<?php echo base_url(); ?>assets/js/morris/morris-0.4.3.min.css" rel="stylesheet"/>
	<!-- Custom Styles-->
	<link href="<?php echo base_url(); ?>assets/css/custom-styles.css" rel="stylesheet"/>
	<!--<link rel="stylesheet" type="text/css" href="//cdn.datatables.net/1.10.7/css/jquery.dataTables.css">-->
	<!--<link href="<?php /*echo base_url(); */ ?>assets/js/dataTables/dataTables.bootstrap.css" rel="stylesheet"/>-->
	<link href="<?php echo base_url(); ?>assets/js/dataTables/jquery.dataTables.min.css" rel="stylesheet"/>
	<link href="<?php echo base_url(); ?>assets/js/dataTables/jquery.dataTables_themroller.css" rel="stylesheet"/>
	<link href="<?php echo base_url(); ?>assets/css/style.css" rel="stylesheet"/>
	<!-- Google Fonts-->
	<link href='http://fonts.googleapis.com/css?family=Open+Sans' rel='stylesheet' type='text/css'/>
	<link href='http://fonts.googleapis.com/css?family=Pacifico' rel='stylesheet' type='text/css'>
	<link href='http://fonts.googleapis.com/css?family=Milonga' rel='stylesheet' type='text/css'>
	<!-- jQuery Js -->
	<script src="<?php echo base_url(); ?>assets/js/jquery.min.js"></script>
	<!-- Bootstrap Js -->
	<script src="<?php echo base_url(); ?>assets/js/bootstrap.min.js"></script>
	<!--<script type="text/javascript" charset="utf8" src="//cdn.datatables.net/1.10.7/js/jquery.dataTables.js"></script>-->

	<script src="<?php echo base_url(); ?>assets/js/bootstrap-datepicker.js" type="text/javascript"></script>
	<!-- JS Scripts-->
	<!-- Metis Menu Js -->
	<script src="<?php echo base_url(); ?>assets/js/jquery.metisMenu.js"></script>
	<!-- Morris Chart Js -->
	<script src="<?php echo base_url(); ?>assets/js/morris/raphael-2.1.0.min.js"></script>
	<script src="<?php echo base_url(); ?>assets/js/morris/morris.js"></script>
	<!-- Custom Js -->
	<script src="<?php echo base_url(); ?>assets/js/custom-scripts.js"></script>
	<script src="<?php echo base_url(); ?>assets/js/html5shiv.js" type="text/javascript"></script>
	<script src="<?php echo base_url(); ?>assets/js/respond.js" type="text/javascript"></script>
	<script type="text/javascript" src="<?php echo base_url( '/assets/js/behaviour/general.js' ) ?>"></script>

	<script type="text/javascript" src="<?php echo base_url( '/assets/js/jquery.gritter/js/jquery.gritter.js' ) ?>"></script>

	<script type="text/javascript"
	        src="<?php echo base_url( '/assets/js/jquery.nanoscroller/jquery.nanoscroller.js' ) ?>"></script>
	<script type="text/javascript" src="<?php echo base_url( '/assets/js/behaviour/general.js' ) ?>"></script>
	<script src="<?php echo base_url( '/assets/js/jquery.ui/jquery-ui.js' ) ?>" type="text/javascript"></script>
	<script type="text/javascript"
	        src="<?php echo base_url( '/assets/js/jquery.sparkline/jquery.sparkline.min.js' ) ?>"></script>
	<script type="text/javascript"
	        src="<?php echo base_url( '/assets/js/jquery.easypiechart/jquery.easy-pie-chart.js' ) ?>"></script>
	<script type="text/javascript" src="<?php echo base_url( '/assets/js/jquery.nestable/jquery.nestable.js' ) ?>"></script>
	<script type="text/javascript"
	        src="<?php echo base_url( '/assets/js/bootstrap.switch/bootstrap-switch.min.js' ) ?>"></script>
	<script type="text/javascript"
	        src="<?php echo base_url( '/assets/js/bootstrap.datetimepicker/js/bootstrap-datetimepicker.min.js' ) ?>"></script>
	<script src="<?php echo base_url( '/assets/js/jquery.select2/select2.min.js' ) ?>" type="text/javascript"></script>
	<script src="<?php echo base_url( '/assets/js/skycons/skycons.js' ) ?>" type="text/javascript"></script>
	<script src="<?php echo base_url( '/assets/js/bootstrap.slider/js/bootstrap-slider.js' ) ?>"
	        type="text/javascript"></script>
	<script src="<?php echo base_url( '/assets/js/intro.js/intro.js' ) ?>" type="text/javascript"></script>

	<script src="<?php echo base_url( '/assets/js/behaviour/voice-commands.js' ) ?>"></script>
	<script type="text/javascript" src="<?php echo base_url( '/assets/js/jquery.flot/jquery.flot.js' ) ?>"></script>
	<script type="text/javascript" src="<?php echo base_url( '/assets/js/jquery.flot/jquery.flot.pie.js' ) ?>"></script>
	<script type="text/javascript" src="<?php echo base_url( '/assets/js/jquery.flot/jquery.flot.resize.js' ) ?>"></script>
	<script type="text/javascript" src="<?php echo base_url( '/assets/js/jquery.flot/jquery.flot.labels.js' ) ?>"></script>
	<!--<script>
		$(document).ready(function () {
			$('#sysusers').dataTable();
		});
	</script>
	<script>
		$(document).ready(function () {
			$('#stafftable').dataTable();
		});
	</script>-->
	<script type="text/javascript">
		window.setTimeout(function () {
			$("#error").fadeTo(500, 0).slideUp(500, function () {
				$(this).remove();
			});
		}, 3000);
	</script>
	<script type="text/javascript">
		window.setTimeout(function () {
			$("#success").fadeTo(500, 0).slideUp(500, function () {
				$(this).remove();
			});
		}, 3000);
	</script>
	<script type="text/javascript">
		window.setTimeout(function () {
			$("#info").fadeTo(500, 0).slideUp(500, function () {
				$(this).remove();
			});
		}, 3000);
	</script>
</head>
<script>
	$('#confirm-delete').on('show.bs.modal', function (e) {
		$(this).find('.btn-ok').attr('href', $(e.relatedTarget).data('href'));

		$('.debug-url').html('Delete URL: <strong>' + $(this).find('.btn-ok').attr('href') + '</strong>');
	});
</script>
<body>
<div id="wrapper">
	<nav class="navbar navbar-default top-navbar no-print" role="navigation">
		<div class="navbar-header">
			<button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".sidebar-collapse">
				<span class="sr-only">Toggle navigation</span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
			</button>
			<a class="navbar-brand" href="<?php echo base_url() . "home" ?>">Cb Shares</a>
		</div>

		<ul class="nav navbar-top-links navbar-right">
			<!-- /.dropdown -->
			<li class="dropdown">
				<a class="dropdown-toggle text-white" data-toggle="dropdown" href="#" aria-expanded="false">
					<i class="fa fa-user fa-fw"></i>&nbsp;<?php echo $this->session->userdata( 'name' ) ?>&nbsp;&nbsp;<i
						class="fa fa-caret-down"></i>
				</a>
				<ul class="dropdown-menu dropdown-user">
					<li><a href="<?php echo base_url() . "main/userProfile" ?>"><i class="fa fa-user fa-fw"></i>
							User Profile</a>
					</li>
					<?php
					if ( $this->session->userdata( 'role' ) == 'Admin' ) {
						?>
						<li><a href="<?php echo base_url() . "settings/settingsHome" ?>"><i
									class="fa fa-gears fa-fw"></i> Settings</a>
						</li>
					<?php } ?>
					<li class="divider"></li>
					<li><a href="<?php echo base_url() . "systemUsers/logout" ?>"><i
								class="fa fa-sign-out fa-fw"></i> Logout</a>
					</li>
				</ul>
				<!-- /.dropdown-user -->
			</li>
			<!-- /.dropdown -->
		</ul>
	</nav>
	<!--/. NAV TOP  -->
